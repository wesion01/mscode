# MSCode微服务平台框架的后端微服务分布式版本

MSCode微服务平台是基于 Spring Cloud、Spring Boot、Activiti7 工作流和阿里巴巴组件，提供所有源码和详尽文档的企业级快速开发平台。
#### 1. 企业级微服务分布式
基于最新的 Spring Cloud 2020.0.3Ilford、Spring Boot 2.5.4、Activiti7 工作流和 Spring Cloud Alibaba 阿里巴巴组件实现企业级微服务架构，支持分布式事务，保障业务高性能、高并发、高可用。
#### 2. 所有源码全部提供
提供所有源码，易用稳定可扩展。二次开发各种企业项目，例如办公(OA)、电商、金融、财务、教育、医疗、信息管理(MIS)、客户关系(CRM)和企业资源计划(ERP)等业务系统。根据业务场景和系统状况，提供定制化的架构设计。
#### 3. 多平台快速开发
提供后端的微服务分布式版本和单体式版本，前端的 Vue 版本和 React 版本，跨多端的移动版，使用代码生成器和表单设计器快速开发。

#### 在线演示
请访问 <a href="https://www.mscodecloud.com" target="_blank">MSCode微服务平台框架官网</a> 查看 Vue 版本和 React 版本的在线演示。

#### 软件架构
前后端分离项目，这是后端的微服务分布式版本。统一为前端的 Vue 版本、 React 版本和跨多端的移动版提供数据接口。
<br>后端的单体式版本在我们仓库：https://gitee.com/mscodecloud/mscodeall<br>
前端的 Vue 版本在我们仓库：https://gitee.com/mscodecloud/mscodevue<br>
前端的 React 版本在我们仓库：https://gitee.com/mscodecloud/mscodereact<br>
![Image text](https://gitee.com/mscodecloud/mscode/raw/master/images/mscode-architecture.png)

#### 交流反馈
长期项目，严格标准提问题提需求。访问 <a href="https://www.mscodecloud.com" target="_blank">MSCode微服务平台框架官网</a> 的技术社区发帖或提 Issues，我们快速解答。请给 Star 让项目走的更远。
<br>官方QQ4群：259643738  官方微信：15011899123

#### 本地运行
1. 将工程源码以 Maven 方式导入 IDEA 或 Eclipse
2. 运行网关服务 MSCodeGatewayApplication、安全认证授权服务 MSCodeAuthApplication、账户管理服务 MSCodeAccountApplication、系统管理服务 MSCodeAdminApplication、文件管理服务 MSCodeFileApplication、消息通知服务 MSCodeNotificationApplication

#### 运行说明
1. SQL 脚本文件在本仓库的 https://gitee.com/mscodecloud/mscode/tree/master/dbsql (开源 MySQL 数据库版本，还有 Oracle、SQLServer、PostgreSQL 和 DB2 数据库版本)
2. 阿里巴巴配置中心、服务注册&发现 Nacos 的 yml 文件在本仓库的 https://gitee.com/mscodecloud/mscode/tree/master/nacos
3. Redis 的配置请登录 Nacos 界面，在 application.yml 修改
4. 数据库配置请登录 Nacos 界面，在各个 yml 修改
5. 更详细文档请看 <a href="https://www.mscodecloud.com" target="_blank">MSCode微服务平台框架官网</a> 的开发文档

#### 部分系统截图
#### Vue版本
<img src="https://gitee.com/mscodecloud/mscode/raw/master/images/vue/verticalminivue.png" width="400" />
<img src="https://gitee.com/mscodecloud/mscode/raw/master/images/vue/usermanagevue.png" width="400" />
<img src="https://gitee.com/mscodecloud/mscode/raw/master/images/vue/designvue.png" width="400" />
<img src="https://gitee.com/mscodecloud/mscode/raw/master/images/vue/activitivue.png" width="400" />
<img src="https://gitee.com/mscodecloud/mscode/raw/master/images/vue/mscode-highlighted.png" width="400" />
<img src="https://gitee.com/mscodecloud/mscode/raw/master/images/vue/mscode-customformvue.png" width="400" />
<img src="https://gitee.com/mscodecloud/mscode/raw/master/images/vue/mscode-rolevue.png" width="400" />
<img src="https://gitee.com/mscodecloud/mscode/raw/master/images/vue/mscode-urlvue.png" width="400" />
<img src="https://gitee.com/mscodecloud/mscode/raw/master/images/vue/mscode-datapermissionvue.png" width="400" />
<img src="https://gitee.com/mscodecloud/mscode/raw/master/images/vue/mscode-tenantvue.png" width="400" />
<img src="https://gitee.com/mscodecloud/mscode/raw/master/images/vue/mscode-notificationvue.png" width="400" />
<img src="https://gitee.com/mscodecloud/mscode/raw/master/images/vue/mscode-chartvue.png" width="400" />
<img src="https://gitee.com/mscodecloud/mscode/raw/master/images/vue/mscode-codegeneratorvue.png" width="400" />
<img src="https://gitee.com/mscodecloud/mscode/raw/master/images/vue/mscode-loginvue.png" width="400" />

#### React版本

<img src="https://gitee.com/mscodecloud/mscode/raw/master/images/react/verticalmini.png" width="400" />
<img src="https://gitee.com/mscodecloud/mscode/raw/master/images/react/horizontalmini.png" width="400" />
<img src="https://gitee.com/mscodecloud/mscode/raw/master/images/react/design.png" width="400" />
<img src="https://gitee.com/mscodecloud/mscode/raw/master/images/react/activiti.png" width="400" />
<img src="https://gitee.com/mscodecloud/mscode/raw/master/images/react/mscode-highlighted.png" width="400" />
<img src="https://gitee.com/mscodecloud/mscode/raw/master/images/react/mscode-customform.png" width="400" />
<img src="https://gitee.com/mscodecloud/mscode/raw/master/images/react/mscode-role.png" width="400" />
<img src="https://gitee.com/mscodecloud/mscode/raw/master/images/react/mscode-url.png" width="400" />
<img src="https://gitee.com/mscodecloud/mscode/raw/master/images/react/mscode-datapermission.png" width="400" />
<img src="https://gitee.com/mscodecloud/mscode/raw/master/images/react/mscode-tenant.png" width="400" />
<img src="https://gitee.com/mscodecloud/mscode/raw/master/images/react/mscode-notification.png" width="400" />
<img src="https://gitee.com/mscodecloud/mscode/raw/master/images/react/chart.png" width="400" />
<img src="https://gitee.com/mscodecloud/mscode/raw/master/images/react/mscode-codegenerator.png" width="400" />
<img src="https://gitee.com/mscodecloud/mscode/raw/master/images/react/login.png" width="400" />
<img src="https://gitee.com/mscodecloud/mscode/raw/master/images/mscode-kibana.png" />
<img src="https://gitee.com/mscodecloud/mscode/raw/master/images/mscode-nacos.png" />
<img src="https://gitee.com/mscodecloud/mscode/raw/master/images/mscode-sentinel.png" />

#### 技术架构
1. 基于 Spring Cloud 2020.0.3 Ilford、Spring Boot 2.5.4 和 Spring Cloud Alibaba 阿里巴巴组件实现企业级微服务架构。<br>
2. Activiti7 实现业务流程，支持新的 BPMN 2.0 标准，对 Web 设计更友好，适应 Activiti 未来发展。(商业授权提供)<br>
3. 代码生成器(模板管理、正向生成、反向生成)动态智能生成前端后端各层次代码和 SQL，提高开发效率。(商业授权提供)<br>
4. 支持 MySQL、Oracle、SQL Server、PostgreSQL 和 DB2 分布式数据库。<br>
5. Mybatis + Mybatis-Plus 作为持久层框架，读写性能好，更适合 SQL 的优化。<br>
6. MongoDB 作为高性能数据存储，实现统计服务。(商业授权提供)<br>
7. Redis 作为分布式缓存，实现高性能数据读取。<br>
8. 结合 Redis 实现轻量级分布式定时任务，易扩展。(商业授权提供)<br>
9. RabbitMQ 作为消息队列，实现数据集成、系统解耦、异步处理、事件驱动和流量削峰。(商业授权提供)<br>
10. 基于 Twitter Snowflake 算法优化而成分布式ID，更适合分库分表。<br>
11. Spring Security + OAuth2 实现认证授权和安全保护。<br>
12. Spring Cloud Gateway 实现微服务网关，Ribbon 实现负载均衡，Feign 实现服务调用，Bus 实现消息总线，Stream 实现事件驱动，Sleuth 实现链路跟踪，Turbine 实现集群聚合监控。<br>
13. 阿里巴巴 Seata 1.4.2 作为高性能分布式事务，对业务代码零侵入。(商业授权提供)<br>
14. 阿里巴巴 Sentinel 1.8.1 实现流量控制，阿里巴巴 Sentinel Dashboard 查看实时监控和机器列表，配置簇点链路、流控规则、降级规则、热点规则、系统规则、授权规则和集群流控。(商业授权提供)<br>
15. 阿里巴巴 Nacos 2021.1 实现配置中心、服务注册&发现，阿里巴巴 Nacos Dashboard 实现配置管理、服务管理、集群管理、权限控制、命名空间。<br>
16. ELK7.6.2 实现日志集中分析平台，Elasticsearch 作为分布式大数据搜索分析引擎，Logstash 作为分布式实时数据采集日志组件，Kibana 作为分布式数据可视化日志组件。(商业授权提供)<br>
17. Swagger 实现接口文档和协同开发。<br>
18. 阿里巴巴数据库连接池 Druid 1.2.6 和 Druid Monitor 监控。<br>

#### 正版保障
MSCode微服务平台的软件著作权证书号：软著登字第4614321号 软著登字第7145259号 软著登字第7371791号
<br>我们除了提供框架平台，也接单各种信息化项目。
