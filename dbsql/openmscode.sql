/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1_3306
 Source Server Type    : MySQL
 Source Server Version : 50728
 Source Host           : 127.0.0.1:3306
 Source Schema         : openmscode

 Target Server Type    : MySQL
 Target Server Version : 50728
 File Encoding         : 65001

 Date: 03/08/2021 09:10:33
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

CREATE DATABASE openmscode;
USE openmscode;

-- ----------------------------
-- Table structure for mscode_file
-- ----------------------------
DROP TABLE IF EXISTS `mscode_file`;
CREATE TABLE `mscode_file`  (
  `id` bigint(20) NOT NULL COMMENT '文件主键ID',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件参数名',
  `original_filename` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文件名称',
  `file_size` bigint(20) NULL DEFAULT NULL COMMENT '文件大小，单位：字节',
  `content_type` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '数据类型',
  `file_type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件类型',
  `type` tinyint(4) NULL DEFAULT 1 COMMENT '1:文件夹，2:文件',
  `url` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '下载URL',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '文件字符串内容',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '上级ID',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `tenant_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '10001' COMMENT '租户编码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文件表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mscode_file
-- ----------------------------
INSERT INTO `mscode_file` VALUES (681335583645552640, 'file', '我的头像.png', 15814, 'image/png', 'image', 2, '/static/upload/20200224110421638.png', NULL, 0, '2020-02-24 11:04:22', '2021-01-11 13:43:14', '10001');
INSERT INTO `mscode_file` VALUES (798065279526948864, NULL, '文档集', NULL, NULL, NULL, 1, NULL, NULL, 0, '2021-01-11 13:46:30', NULL, '10001');
INSERT INTO `mscode_file` VALUES (798075650501431296, 'file', 'Spring Boot and Spring Cloud.pdf', 88940, 'application/pdf', 'document', 2, '/static/upload/20210111142742606.pdf', 'Copyright ©2003-2021 Accelebrate, Inc. Some outlines may contain content from our courseware partners; such \r\ncontent is protected by these partners’ copyrights. All trademarks are owned by their respective owners. \r\n \r\n \r\nSpring Boot and Spring Cloud \r\nCourse Number: SPRG-184 \r\nDuration: 5 Days \r\nOverview \r\nRapid advancements in cloud-based software delivery and virtualization have caused many \r\ndevelopers to question the wisdom of a “Big Server” approach to deploying web applications. \r\nSpring Boot is a technology stack that builds on the popular Spring Framework to allow the \r\ndeployment of Spring-based applications as stand-alone jar files that host their own web \r\nservers.  This approach works nicely with deployment automation and rapid scaling. \r\nCloud-based, highly-distributed applications face additional challenges in supporting a \r\ndynamic environment, especially around configuration, service discovery, service resilience, \r\nand monitoring. Spring Cloud embraces and extends the popular suite of open source cloud \r\ntools published by Netflix (Eureka, Hystrix, Ribbon, etc.). \r\nThis Spring Boot and Spring Cloud training introduces Spring Boot, Spring Cloud, and the \r\nNetflix OSS suite as a way to deploy highly resilient and scalable RESTful services and web \r\napplications. \r\nPrerequisites \r\nAll attendees must be comfortable programming in Java and have a basic understanding of \r\nSpring Dependency Injection using the Spring @Autowired annotation or the javax @Inject \r\nannotation. Spring Java Configuration experience is helpful but not required. \r\nMaterials \r\nAll Spring training attendees receive courseware covering the topics in the course. \r\nSoftware Needed on Each Student PC \r\n 64-bit Windows, Mac, or Linux environment with at least 8GB RAM \r\n Java 1.8 or later \r\n Eclipse or IntelliJ (we may be able to support other IDEs upon request) \r\n Related lab files that Accelebrate provides \r\nObjectives \r\n Use Spring Boot to build standalone web applications and RESTful services \r\n Understand the Configuration techniques that Spring Boot Provides \r\n Build Spring boot based Microservices for JSON and XML data exchange \r\n Monitor services using the Prometheus Actuator \r\n Understand the major components of Netflix OSS \r\n Register services with a Eureka Service \r\n Implement “client” load balancing with Ribbon to Eureka managed Services \r\n Create fault-tolerant services with Hystrix callbacks \r\nCopyright ©2003-2021 Accelebrate, Inc. Some outlines may contain content from our courseware partners; such \r\ncontent is protected by these partners’ copyrights. All trademarks are owned by their respective owners. \r\n Filter requests to your Microservices using Zuul \r\n Create producer and Consumer services using Kafka and Zookeeper \r\n Define Feign clients to your services \r\nOutline \r\n Introduction  \r\no What is Spring Boot? \r\no Spring starter Maven Dependencies \r\no Understanding @SpringBootApplication \r\no Example of Spring MVC-based RESTful Web Service \r\no Project Structure \r\no Externalized Configuration application.properties and YAML \r\no @ConfigurationPropetrties  \r\n Type Safe Properties \r\n JSR303 Validation of Properties \r\n Spring Environment Object \r\n @Value vs @ConfigurationProperties \r\no Logging \r\n Actuators  \r\no Exposing Information about your services \r\no Customize Health and Info Endpoints \r\no Custom Metrics and Custom Actuator \r\no Prometheus Monitoring \r\n Building Web Applications  \r\no Controllers and ModelAttributes \r\no Template Views \r\no Using Embedded and External Databases \r\no Spring Boot Initializers and Command Line Runners \r\no Active Profiles \r\no Form Submissions \r\n REST Services  \r\no What is REST? \r\no Restful Controllers \r\no JSON and XML Data Exchange \r\no Content Negotiation \r\no Angular JS Accessing your Services \r\n Java Clients using RestTemplate  \r\no ResponseEntity \r\no Headers \r\no Status codes \r\no RequestEntity \r\no Posting JSON to a Service \r\n Persistence with JPA Repositories  \r\no JPA, EntityManagers, Entity Classes, Annotation mappings \r\no Spring JPA Data, CrudRepository, PagingAndSortingRepository \r\no Spring Data Rest, Exposing Databases as Endpoints \r\no HATEOAS JSON \r\no @Projections and Excerpts  \r\n Restrict the data sent back to the client \r\n Introduction to Microservices  \r\no What are Microservices? \r\no Decentralized Governance, Scalability, Fault Tolerance \r\nCopyright ©2003-2021 Accelebrate, Inc. Some outlines may contain content from our courseware partners; such \r\ncontent is protected by these partners’ copyrights. All trademarks are owned by their respective owners. \r\no Cloud Computing \r\no Spring Cloud \r\no Service and Client Discovery \r\no Netflix OSS \r\n Core Microservice Patterns using Spring Cloud and Netflix OSS  \r\no Where are my Services?  \r\n Using Service Discovery \r\n Eureka Servers and Clients \r\no Scale Services  \r\n Load Balancing with Ribbon using Service Discovery \r\n A LoadBalanced RestTemplate \r\n Load Balancing with Ribbon without Service Discovery \r\no Circuit Breaker, when services fail  \r\n Hystrix \r\n Callbacks \r\no Gateway/Edge Services – Providing an API  \r\n Zuul services \r\n Filtering the Request and Response  \r\no Create Feign Clients to your Services \r\no Event Driven Services or Sagas  \r\n Keeping Services synchronized with each other \r\n Creating Message Queues with Zookeeper and Kafka \r\n Docker Images and Containers  \r\no Orchestration and Workflow \r\n Conclusion \r\n \r\n', 798065279526948864, '2021-01-11 14:27:43', '2021-01-18 11:25:24', '10001');
INSERT INTO `mscode_file` VALUES (798076789640843264, 'file', 'Spring Boot and Spring Cloud.txt', 53, 'text/plain', 'other', 2, '/static/upload/20210111143214199.txt', 'MSCode微服务平台\r\nSpring Boot and Spring Cloud\r\n', 0, '2021-01-11 14:32:14', '2021-01-18 11:27:56', '10001');
INSERT INTO `mscode_file` VALUES (798076828920500224, 'file', 'Spring Boot and Spring Cloud.docx', 36190, 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'document', 2, '/static/upload/20210111143223562.docx', '\n\nSpring Boot and Spring Cloud\nCourse Number: SPRG-184\nDuration: 5 Days\nOverview\nRapid advancements in cloud-based software delivery and virtualization have caused many developers to question the wisdom of a “Big Server” approach to deploying web applications. Spring Boot is a technology stack that builds on the popular Spring Framework to allow the deployment of Spring-based applications as stand-alone jar files that host their own web servers.  This approach works nicely with deployment automation and rapid scaling.\nCloud-based, highly-distributed applications face additional challenges in supporting a dynamic environment, especially around configuration, service discovery, service resilience, and monitoring. Spring Cloud embraces and extends the popular suite of open source cloud tools published by Netflix (Eureka, Hystrix, Ribbon, etc.).\nThis Spring Boot and Spring Cloud training introduces Spring Boot, Spring Cloud, and the Netflix OSS suite as a way to deploy highly resilient and scalable RESTful services and web applications.\nPrerequisites\nAll attendees must be comfortable programming in Java and have a basic understanding of Spring Dependency Injection using the Spring @Autowired annotation or the javax @Inject annotation. Spring Java Configuration experience is helpful but not required.\nMaterials\nAll Spring training attendees receive courseware covering the topics in the course.\nSoftware Needed on Each Student PC\n64-bit Windows, Mac, or Linux environment with at least 8GB RAM\nJava 1.8 or later\nEclipse or IntelliJ (we may be able to support other IDEs upon request)\nRelated lab files that Accelebrate provides\nObjectives\nUse Spring Boot to build standalone web applications and RESTful services\nUnderstand the Configuration techniques that Spring Boot Provides\nBuild Spring boot based Microservices for JSON and XML data exchange\nMonitor services using the Prometheus Actuator\nUnderstand the major components of Netflix OSS\nRegister services with a Eureka Service\nImplement “client” load balancing with Ribbon to Eureka managed Services\nCreate fault-tolerant services with Hystrix callbacks\nFilter requests to your Microservices using Zuul\nCreate producer and Consumer services using Kafka and Zookeeper\nDefine Feign clients to your services\nOutline\nIntroduction \nWhat is Spring Boot?\nSpring starter Maven Dependencies\nUnderstanding @SpringBootApplication\nExample of Spring MVC-based RESTful Web Service\nProject Structure\nExternalized Configuration application.properties and YAML\n@ConfigurationPropetrties \nType Safe Properties\nJSR303 Validation of Properties\nSpring Environment Object\n@Value vs @ConfigurationProperties\nLogging\nActuators \nExposing Information about your services\nCustomize Health and Info Endpoints\nCustom Metrics and Custom Actuator\nPrometheus Monitoring\nBuilding Web Applications \nControllers and ModelAttributes\nTemplate Views\nUsing Embedded and External Databases\nSpring Boot Initializers and Command Line Runners\nActive Profiles\nForm Submissions\nREST Services \nWhat is REST?\nRestful Controllers\nJSON and XML Data Exchange\nContent Negotiation\nAngular JS Accessing your Services\nJava Clients using RestTemplate \nResponseEntity\nHeaders\nStatus codes\nRequestEntity\nPosting JSON to a Service\nPersistence with JPA Repositories \nJPA, EntityManagers, Entity Classes, Annotation mappings\nSpring JPA Data, CrudRepository, PagingAndSortingRepository\nSpring Data Rest, Exposing Databases as Endpoints\nHATEOAS JSON\n@Projections and Excerpts \nRestrict the data sent back to the client\nIntroduction to Microservices \nWhat are Microservices?\nDecentralized Governance, Scalability, Fault Tolerance\nCloud Computing\nSpring Cloud\nService and Client Discovery\nNetflix OSS\nCore Microservice Patterns using Spring Cloud and Netflix OSS \nWhere are my Services? \nUsing Service Discovery\nEureka Servers and Clients\nScale Services \nLoad Balancing with Ribbon using Service Discovery\nA LoadBalanced RestTemplate\nLoad Balancing with Ribbon without Service Discovery\nCircuit Breaker, when services fail \nHystrix\nCallbacks\nGateway/Edge Services – Providing an API \nZuul services\nFiltering the Request and Response \nCreate Feign Clients to your Services\nEvent Driven Services or Sagas \nKeeping Services synchronized with each other\nCreating Message Queues with Zookeeper and Kafka\nDocker Images and Containers \nOrchestration and Workflow\nConclusion\n\nCopyright ©2003-2021 Accelebrate, Inc. Some outlines may contain content from our courseware partners; such content is protected by these partners’ copyrights. All trademarks are owned by their respective owners.\n', 0, '2021-01-11 14:32:24', '2021-01-18 11:27:23', '10001');
INSERT INTO `mscode_file` VALUES (798076858771361792, 'file', 'Spring Boot and Spring Cloud.pdf', 88940, 'application/pdf', 'document', 2, '/static/upload/20210111143230678.pdf', 'Copyright ©2003-2021 Accelebrate, Inc. Some outlines may contain content from our courseware partners; such \r\ncontent is protected by these partners’ copyrights. All trademarks are owned by their respective owners. \r\n \r\n \r\nSpring Boot and Spring Cloud \r\nCourse Number: SPRG-184 \r\nDuration: 5 Days \r\nOverview \r\nRapid advancements in cloud-based software delivery and virtualization have caused many \r\ndevelopers to question the wisdom of a “Big Server” approach to deploying web applications. \r\nSpring Boot is a technology stack that builds on the popular Spring Framework to allow the \r\ndeployment of Spring-based applications as stand-alone jar files that host their own web \r\nservers.  This approach works nicely with deployment automation and rapid scaling. \r\nCloud-based, highly-distributed applications face additional challenges in supporting a \r\ndynamic environment, especially around configuration, service discovery, service resilience, \r\nand monitoring. Spring Cloud embraces and extends the popular suite of open source cloud \r\ntools published by Netflix (Eureka, Hystrix, Ribbon, etc.). \r\nThis Spring Boot and Spring Cloud training introduces Spring Boot, Spring Cloud, and the \r\nNetflix OSS suite as a way to deploy highly resilient and scalable RESTful services and web \r\napplications. \r\nPrerequisites \r\nAll attendees must be comfortable programming in Java and have a basic understanding of \r\nSpring Dependency Injection using the Spring @Autowired annotation or the javax @Inject \r\nannotation. Spring Java Configuration experience is helpful but not required. \r\nMaterials \r\nAll Spring training attendees receive courseware covering the topics in the course. \r\nSoftware Needed on Each Student PC \r\n 64-bit Windows, Mac, or Linux environment with at least 8GB RAM \r\n Java 1.8 or later \r\n Eclipse or IntelliJ (we may be able to support other IDEs upon request) \r\n Related lab files that Accelebrate provides \r\nObjectives \r\n Use Spring Boot to build standalone web applications and RESTful services \r\n Understand the Configuration techniques that Spring Boot Provides \r\n Build Spring boot based Microservices for JSON and XML data exchange \r\n Monitor services using the Prometheus Actuator \r\n Understand the major components of Netflix OSS \r\n Register services with a Eureka Service \r\n Implement “client” load balancing with Ribbon to Eureka managed Services \r\n Create fault-tolerant services with Hystrix callbacks \r\nCopyright ©2003-2021 Accelebrate, Inc. Some outlines may contain content from our courseware partners; such \r\ncontent is protected by these partners’ copyrights. All trademarks are owned by their respective owners. \r\n Filter requests to your Microservices using Zuul \r\n Create producer and Consumer services using Kafka and Zookeeper \r\n Define Feign clients to your services \r\nOutline \r\n Introduction  \r\no What is Spring Boot? \r\no Spring starter Maven Dependencies \r\no Understanding @SpringBootApplication \r\no Example of Spring MVC-based RESTful Web Service \r\no Project Structure \r\no Externalized Configuration application.properties and YAML \r\no @ConfigurationPropetrties  \r\n Type Safe Properties \r\n JSR303 Validation of Properties \r\n Spring Environment Object \r\n @Value vs @ConfigurationProperties \r\no Logging \r\n Actuators  \r\no Exposing Information about your services \r\no Customize Health and Info Endpoints \r\no Custom Metrics and Custom Actuator \r\no Prometheus Monitoring \r\n Building Web Applications  \r\no Controllers and ModelAttributes \r\no Template Views \r\no Using Embedded and External Databases \r\no Spring Boot Initializers and Command Line Runners \r\no Active Profiles \r\no Form Submissions \r\n REST Services  \r\no What is REST? \r\no Restful Controllers \r\no JSON and XML Data Exchange \r\no Content Negotiation \r\no Angular JS Accessing your Services \r\n Java Clients using RestTemplate  \r\no ResponseEntity \r\no Headers \r\no Status codes \r\no RequestEntity \r\no Posting JSON to a Service \r\n Persistence with JPA Repositories  \r\no JPA, EntityManagers, Entity Classes, Annotation mappings \r\no Spring JPA Data, CrudRepository, PagingAndSortingRepository \r\no Spring Data Rest, Exposing Databases as Endpoints \r\no HATEOAS JSON \r\no @Projections and Excerpts  \r\n Restrict the data sent back to the client \r\n Introduction to Microservices  \r\no What are Microservices? \r\no Decentralized Governance, Scalability, Fault Tolerance \r\nCopyright ©2003-2021 Accelebrate, Inc. Some outlines may contain content from our courseware partners; such \r\ncontent is protected by these partners’ copyrights. All trademarks are owned by their respective owners. \r\no Cloud Computing \r\no Spring Cloud \r\no Service and Client Discovery \r\no Netflix OSS \r\n Core Microservice Patterns using Spring Cloud and Netflix OSS  \r\no Where are my Services?  \r\n Using Service Discovery \r\n Eureka Servers and Clients \r\no Scale Services  \r\n Load Balancing with Ribbon using Service Discovery \r\n A LoadBalanced RestTemplate \r\n Load Balancing with Ribbon without Service Discovery \r\no Circuit Breaker, when services fail  \r\n Hystrix \r\n Callbacks \r\no Gateway/Edge Services – Providing an API  \r\n Zuul services \r\n Filtering the Request and Response  \r\no Create Feign Clients to your Services \r\no Event Driven Services or Sagas  \r\n Keeping Services synchronized with each other \r\n Creating Message Queues with Zookeeper and Kafka \r\n Docker Images and Containers  \r\no Orchestration and Workflow \r\n Conclusion \r\n \r\n', 0, '2021-01-11 14:32:31', '2021-01-18 11:25:19', '10001');
INSERT INTO `mscode_file` VALUES (800587637328367616, 'file', '用户管理.xlsx', 12491, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'other', 2, '/static/upload/20210118124926874.xlsx', '用户名昵称手机号角色所属机构状态rdd张部1.5688888888E10管理角色开发三组正常hrd人事总监1.9999999999E10管理角色行政部正常personnel人事经理1.5666666667E10行政角色行政部正常manager李总1.3333333339E10管理角色行政部正常attendance人事专员1.2111111113E10行政角色行政部正常test王雪红1.5011112222E10测试角色测试一组正常admin超级管理员1.2222222229E10开发角色,超级管理员开发三组正常', 0, '2021-01-11 14:32:15', '2021-01-18 12:50:40', '10001');
INSERT INTO `mscode_file` VALUES (801659644946075648, 'file', '20200221103500027.jpg', 13034, 'image/jpeg', NULL, 2, '/static/upload/20210121114913352.jpg', NULL, NULL, '2021-01-21 11:49:14', NULL, '10001');
INSERT INTO `mscode_file` VALUES (801660787122163712, 'file', '20200214115811030.png', 22241, 'image/png', NULL, 2, '/static/upload/20210121115345785.png', NULL, NULL, '2021-01-21 11:53:46', NULL, '10001');
INSERT INTO `mscode_file` VALUES (801660973856772096, 'file', 'MSCode微服务平台专业版.docx', 14785, 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', NULL, 2, '/static/upload/20210121115429387.docx', 'MSCode微服务平台专业版 3900元\n\n1、永久商业授权；不限制域名数、项目数、库表数、用户数；提供增值税电子普通发票或增值税专用发票，签订商业授权合同或发送商业授权书。\n2、一对一金牌服务：无限次即时响应的电话、QQ、微信、邮件和远程协助。\n3、根据企业项目需求进行专业定制培训。\n4、提供所有源码和详尽文档，包含Spring Cloud Hoxton.SR8、Spring Boot 2.3.3和Spring Cloud Alibaba阿里巴巴组件的微服务分布式版本，包含Spring Boot 2.3.3和Spring Cloud Alibaba阿里巴巴组件的单体式版本，免费持续升级。\n5、基于Vue的Web端，响应式布局，针对不同屏幕大小设计，适配电脑、平板和手机。\n6、基于React的Web端，响应式布局，针对不同屏幕大小设计，适配电脑、平板和手机。\n7、国际化方案支持简体中文、繁体中文和英文，8种主题皮肤满足多样化的品牌诉求。\n8、代码生成器智能生成前后端各层次基础代码，提高开发效率。\n9、图表报表：分析页、监控页，支持饼状图、柱状图、仪表盘、雷达图、标签云、水波图、迷你柱状图、迷你区域图、迷你进度条、带有时间轴的折线图、图表卡片、图表字段。\n10、权限管理：角色管理(授权菜单、授权按钮)、接口权限、数据权限，在线配置即时生效。\n11、系统管理：用户管理、菜单管理、机构管理、字典管理、区域管理、参数管理、应用管理。\n12、基础功能：账户密码登录、手机号登录、验证码、注册、找回密码、发送邮件、个人设置、我的消息、消息通知、短信服务、上传下载、租户管理、SSO单点登录。\n13、支持MySQL、Oracle、SQL Server、PostgreSQL和DB2分布式数据库。\n14、完整的SaaS多租户架构方案。\n15、ELK7.6.2日志集中分析平台，Elasticsearch分布式大数据搜索分析引擎，Logstash分布式实时数据采集日志组件，Kibana分布式数据可视化日志组件。\n16、Spring Security + OAuth2认证授权和安全保护。\n17、Spring Cloud Gateway微服务网关，Ribbon负载均衡， Feign服务调用，Bus消息总线，Stream事件驱动，Sleuth链路跟踪，Turbine集群聚合监控。\n18、阿里巴巴Seata高性能分布式事务，对业务代码零侵入。\n19、阿里巴巴Sentinel流量控制，阿里巴巴Sentinel Dashboard查看实时监控和机器列表，配置簇点链路、流控规则、降级规则、热点规则、系统规则、授权规则和集群流控。\n20、阿里巴巴Nacos配置中心、服务注册&发现，阿里巴巴Nacos Dashboard配置管理、服务管理、集群管理、权限控制、命名空间。\n21、基于Twitter Snowflake算法优化而成分布式ID，更适合分库分表。\n22、RabbitMQ消息队列，实现数据集成、系统解耦、异步处理、事件驱动和流量削峰。\n23、Redis分布式缓存，实现高性能数据读取，实现轻量级分布式定时任务。\n24、MongoDB高性能数据存储，实现统计服务。\n25、Swagger实现接口文档和协同开发。\n26、Web端适配电脑、平板和手机，另外提供多端统一开发移动版：一处代码，10端运行，支持H5、Android、iOS、QQ小程序、360小程序、快应用、微信小程序、百度小程序、支付宝小程序、字节跳动小程序。\n', NULL, '2021-01-21 11:54:31', NULL, '10001');

-- ----------------------------
-- Table structure for mscode_file_sysuser
-- ----------------------------
DROP TABLE IF EXISTS `mscode_file_sysuser`;
CREATE TABLE `mscode_file_sysuser`  (
  `id` bigint(20) NOT NULL COMMENT '文件与系统用户关联主键ID',
  `file_id` bigint(20) NOT NULL COMMENT '文件主键ID',
  `sysuser_id` bigint(20) NOT NULL COMMENT '系统用户主键ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文件与系统用户关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mscode_file_sysuser
-- ----------------------------
INSERT INTO `mscode_file_sysuser` VALUES (681335583775576064, 681335583645552640, 653917974152592329);
INSERT INTO `mscode_file_sysuser` VALUES (798065279526948865, 798065279526948864, 653917974152592329);
INSERT INTO `mscode_file_sysuser` VALUES (798075650501431297, 798075650501431296, 653917974152592329);
INSERT INTO `mscode_file_sysuser` VALUES (798076789640843265, 798076789640843264, 653917974152592329);
INSERT INTO `mscode_file_sysuser` VALUES (798076828920500225, 798076828920500224, 653917974152592329);
INSERT INTO `mscode_file_sysuser` VALUES (798076858771361793, 798076858771361792, 653917974152592329);
INSERT INTO `mscode_file_sysuser` VALUES (800587637328367617, 800587637328367616, 653917974152592329);
INSERT INTO `mscode_file_sysuser` VALUES (801659644950269952, 801659644946075648, 653917974152592329);
INSERT INTO `mscode_file_sysuser` VALUES (801660787122163713, 801660787122163712, 653917974152592329);
INSERT INTO `mscode_file_sysuser` VALUES (801660973856772097, 801660973856772096, 653917974152592329);

-- ----------------------------
-- Table structure for mscode_notification
-- ----------------------------
DROP TABLE IF EXISTS `mscode_notification`;
CREATE TABLE `mscode_notification`  (
  `id` bigint(20) NOT NULL COMMENT '消息通知主键ID',
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '标题',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '内容',
  `type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '类型',
  `priority` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '优先级',
  `publisher` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '发布者',
  `start_time` timestamp(0) NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` timestamp(0) NULL DEFAULT NULL COMMENT '结束时间',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `tenant_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '10001' COMMENT '租户编码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '消息通知表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mscode_notification
-- ----------------------------
INSERT INTO `mscode_notification` VALUES (99, '通知公告标题测试', '<p>通知公告内容测试</p><p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAyAAAALxCAMAAACTuzJkAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAHaUExURW20PWmyN2ixNmuzOYrDY2qyOGuzOv///2yzPGixNWyzOmawM2exNGqyOWOvLmmyNmKuLWSvMGWwMmCtKWexNWewNGGuK2GtK2WvMfP58G60PnC1QWy0PGSvMXi5SnS3RXq6TojBYP3+/fn8+F+sKHy7UG+1P/X58XW4Rtzt0JvMe/7+/o3EaMLgr/b689/u1Or05M/nwP3+/P7//mawMmuyObfaoJfJdfn79q/WlX28UsjjtuTw2+Dv1pjKeObx3XK2Q73dqOnz4vH37qPQhvD37IS/XNrszr7eqazUkdfqynm6TJHGbZ7Nf6bRiuz05YzDZoXAXbXZnfL47+726Xa4SYC9Vt3u0prLevv8+ZXIc1yrJNHnwobBX4nCYmawNKXRiKrTj9nszNLow8njuMThsqjSjLrbo12sJpPHcbDWl7LXmcvkuujy4Y7FaqDOgnK2Qu3156LPhH68U/f79ZDGbLvcpq3Uk8fitcDeq4G+V//+/4O/Ws7mvrPYm7jaoefy4J3MfYrCZMzlvOHv2NPpxePv2dbqyfv9++/268Xhs1qqItPoxe/26WOvMMHfrcPhsJLHb2myOH+9Va7VlNXpx4K+WGiyNmWvMmqyN1epH2SwMGexNmyyPLF+3SEAADt1SURBVHja7N3pW9PYAoDxJH1Sk6bFtlBgLAW8yqIyjPqAKFBGFBBFNrkgoLIoiteNpWx3LqCIPiADbqMz+t9eoE1FWWRpIUnf3ycX9MNJ36bn5CQVDgHYlMAQAAQCEAhAIACBAAQCEAhAIACBAAQCgEAAAgEIBCAQgEAAAgEIBCAQgEAAAgFAIACBAAQCEAhAIACBAAQCEAhAIAAIBCAQgEAAAgEIBCAQgEAAAgEIBCAQAAQCEAhAIACBAAQCEAhAIACBAAQCgEAAAgEIBCAQgEAAAgEIBCAQgEAAAgFAIACBAAQCEAhAIACBAAQCEAhAIAAIBCAQgEAAAgEIBCAQgEAAAgEIBCAQAAQCEAhAIACBAAQCEAhAIACBAAQCEAhDABAIQCAAgQAEAhAIQCAAgQAEAhAIAAIBCAQgEIBAAAIBCAQgEIBAAAIBCAQAgQAEAhAIQCAAgQAEAhAIQCDYT6dOnWIQCASr5jvyhoYabwaDwXu3761amL4xXba4+svbr5f/4ubS0FBhx3mGikASSeHSq7uzC8/P9QxXFBQ05be21rmT01a5PR6PO/zL5EB+ZmvWs4KCk1UD/c/fz14LDhEKgVjbyNC999PDFaMv23IGhaS33iKfqkquZYqwnlK8/BeqqmpFRUlv7YPZbV3XK3pu3L82dJaBJBDLqZ7qr39ZmpOVa3dqKZIo2+wbRbEZxWGTRSlFUxV/fnbozkzl3C8MKYFYxNJsz6RDlZatdOHYSRjrQ7G7lktZptraxicaGVwCMbHzLU+neo/eepjqlZZPGMuE2Fj+nxx22+GA+6G7qbfsdMsxhppAzKalfK7yZGlaWroUqyw24lDTU92lA7Wvf89jyAnELI696vtUUlrk1sR4xhE9n4ia+5+2iunZ8nmGnkAMb+hJz+gfuUnOj/sRR/RMInZ7Bic/9PZxIiEQI2vsL6gblJyiXdh3ik30if6cpkpm7gRiTK8a8jVJkm2KcFCWI5EkLbtqiINBIMaadbQszuQ+9MoO5eDq0Fe4HKL3llJ1t4UZCYEYw/zTqeHMVM0lGEaGzZtW2nDmNBsfCeTAPZ2oqnF7ZUUwFkUMOI8+6HvKASKQg7QwUKN4RaPVEVnakopya07e4yARyAF50/tXrirZjZnH6mnELmkXJx9d5lARyP6brQmoooHriC5sqYHmPg4XgezrqtWlx+nJ0sGvWW1zXUtKPvK4nLtJCGSfNE60a0l2wUxcSXLFEy6PEMg+CF7oStfMlccKu6Z1PQ9y+Agkvl7Xh+K7QTeOn7VUX2jgGoeQQOKn79mgKpozj/DFETXr6BSHkUDi4n9n/JrkMm8eq4m4JG/ObY4lgcRcda0/WVbMnUd4UetwWvZECweUQGLpxVgoSc4QrEER3c33X3BQCSRWGp9PJomKYB2K5Pn7HKu+BBITlx/fcUpWyiOciPNoP9cOCWTvHufYLJdHOBGldCweA3aJQBLIVK4mOqzYx8pWRrHIH4fNvl/nCSRBHPtcaomVq81XtMS05msxfj3Ppi4QSEI4NVeiqtatI9KIzzFwN6bDdtTzjEASQXlDfsAuWJ/dkzUewyehfM5Kyc4jEMvLm277IguJQfY0n+uI1cANqMWDTwjE6m7XyIcVIVEokqtpLjYDFwyJirOeQKztzajb+PcKxvjeXPdoTCbrvUmKIN1pJBALG/n1lqYkUh7hBS3vl4W9Pya+PCQKgs3fRyCWNb/Y5XYIicjlHf2819EbS195Z/E0EIhVDT0QNEVITIo3f/zfexq96hxx5T+Suk4TiDW9+1uShcQl+/7Y080iY+7VNxeHukggVnTsg5KiCIlM8fmH9/AlocWRdxfP+FkCsZ4nfs0uJDil2Ju96xXfsuTI24vY1kIgVnOp3Vr3fOx6OUt62/Bmd0M4GP14eusKgVjMbE4SeYQ50mp2NRMZ80VHMPCJQCyluuGrShlRzuLHO3+g71K2GL0pWc4kECu51hSwkcXaayLO67/tdBAfFX+7fqQc/51ArOOcX+Lj1Q97fA9n3d/ZIJ5uXjuIgasEYhUjBW4XfaybrMvuZzva4nvjuwusLoFALOLEV69CHxstZ3kyb25/GF+FL6JHJ/raCQKxgsIxn0gMG8tQ/ZUj2x3I8dQfdtBfIBALeNGuuihh00JEV/02N2cF/d+PoyI2E4j5zbWl2OlgCzapeXsflToDP66D5Z8gELN7p7B69bOZiKps5wbau25Fn7no/9B1gUBMbjiZ1attrGZ5en8+lJlS5Kcd+p2Yiu8/BGJqjV3sLdneJRFv589u73iuL/HKoWy9EDH0G4GYefoR0uhjm3uzvjRf2XIsL2VHnnyvaJ9q/ZFZnfLPGIGYV20dfWz/Y5Y3Z8unJVbp26DlzCsj2R8j/8o7M0IgZtUgSLzud0A98mjzwVy8KOsz+plDh67rCx/i5AsCMal2jasfOyNLm26u+qWzWz+BXFz+KFamB6J4zhCIKRVOpjt4ye/wU5bd07nJU4HeBfQiUkpWfu/ULy15qzoIxISCk91MP3axNcvdvOFHpqe5+n2ELmU1oQ/OyO/Fv1oIxHw+t3npY1c8Lzf6lvV2j/73qeF5ylyy3pT7BIGYzkKdxkt9l7wX59aN55T+oAZBzA9/ojoV/YyV3kMgZjORwa21u5fSWvbDeM7ni9HzxWzkz07qb0FyK4GYzAWFze17mYiIuf/9fkDrnfoJRGsq1Jd9j+s/fnyJQEyl1ycz/9hTIR/lyrUD2nfEpi/xOqITjsv5+rQ98IBATKTjU4qNPvZYiMt79dvi7ek/nNEl3m+bdzuGvfq0JJtAzCNvQOby4N7Z3dHTQkdVtA+1ec13gpTpV0YcgSUCMYuzJUncHBULjmghfYL+jmMT1j4D5VWdvvckpZZAzKLEw8erGJ1DtHAhl+v0FUFF/bB2qEdmiiJ/LjURiFn6YPdurGTY1dV7qAqK9CF1Sd9fM6/UH5Iv11UTiBkc6/ziyOClHbuNWQ1nD/VHLxEqyRPfD/fNUGTPuz33VwIxw/y83aPQRywLcY8H0/Qdn4q384fxLhyNzN4V5wyBmKCPk8w/Yj1Tl1od+pg6Q+s2JT7SrzdJXUsEYnid9BH7k0j0kpKcO7VuxK+0Rta3irOmCMTorrrpI46ldD/41/oxD+mPOdGmCcTgPqlc/4hjH947eRsMeo8aeVPydbYQiKH1drO/JI59iJnlG416UHXoC71BAjGyfpmnw8XzomHKJs87OaJvWEwuIxADe3+R/e3xlLzZPVFV+oZFrf48gRjWYi6P94mn9JebfSH6zYeCWS+mJ1Ag5VncPxhPUt3mX0WoRL71UUm6SyAGdT7LyYs4jmRxi/lF9OEmb6cJxJjy2nzMz+PI1r3VZvY5fauWmEMgxjx/lDjpI567TZwNWw1/tUP/Zu0/LxOIAc03cIEwrldAiq5v+XDq8xW+yE+6awnEgGqjK/GIh+6an3x9YW1a5CfVlwRiPFNHuADyf/bO/C2J7Y/jzMwzw2wgq+CCSCHhEmlfRVJC0tRcMNHMhXJLc8kl0xRN/ZJ6k9Su2fVmff/ar9WZ+1h5C5RhBvi8f/R51Hnmc15zzmc9YgrD635jgWJrFfJVSABEdnLB/QbiVpiwzb8zwcIcirHT/CgAIjdVwfWcYvJBYuHf22AaDTehmXkARGbqNAAfYiZAFIcxGGHbT6RkoDcDAFkhIYAlZgIEn47FCpEmdMxNsTvT0x+QsTwIYIl5wNKsxHYzTh9KRLF0FwAiI3kLDbCKRZS+5VVshnDTLHJCqgEQ+ehKJ1SYiLp/lMVqid4etJPjwXoARDYqgRZ0EUVxL6Mxm2INxRIJ/zYAIheNh4APEfnAB67FbosOIdiuuQWAyMUBMUIASzyx+MDbOIxxgKPOdD6VRjekNSAu2wlsIOL5H58G4msPrEKZELLwNgAiCz09gSvQxZOpcynehC0iy7IBgMhBtTTckSPe/jEZiMZpj/ca9LuWkSgAIr3eFEIJlnj5c3P8nrZrUpjR+7gbAJFcnj418CGS8kn8uDR+k/yFmg4oQx0AIrnckAERj4/crv4LmGTEjP5A9iEAInmEFwMHRCxhlXsXskmzMB7L6ANApFaQg4UsFh/Eo4vZpCEPfbQIBQAisTahBEus8BXHXLRe3dOHvlq0/Q0AIql280gARJzyEn7w4oN7VgW/UDsNgEgp1xxMwRIpvMv7LjHYqlnoXcPXABAp1QVNhOKI5Fpcl7BLvQ21FVJ/AiASqrgHpvyIIpxcvpRhosI9CLTxDwBEMl2dAw9dFHHWy3ZyLBuF2SbDAIhkmoEUoRjRK1az5r2saQ7Qhbc0tgaASOahh8ABEYEPEiu5emnbvN1C8+PIgggAIpGqdbCcEy+jPyHDSOZReJFy7AEg0mjMBGN+Er9/qD8+Soh17uV+297pk7sAiCSKvnQ6J/UatZljSGiXSpBUXHWC6m9vFKDPFz93FQCRBJBIXV1zb/jOSklZm3nSpOUgpX5Z5ePktCtR9llEEXjs4Q4AIqVKo/2epUjF9OsefShba1ARFIByMbGf/I8SZxcfLjghtQCITNSw/azT4aA/Mx8IgCRu7wNjjkoTaIwwhjIh6hUARE6aHe+wLfr5IQxiwPGIMlonEmqHd8JdeCky/CeD7klXKr21LdUfWTUDG0ms3gf5eS7R991YkZdO1uwAIPLT0puZ4zlazcM+EgsfjOJWwj/zr4WekKEKAESe7nujd7WaM8HIk9+6H9mLxYmfzzOWjf68ehMAkW8w+Fo4aMo+PWsBJf9GB83oX1wR4dV79tF/4EqWABBZK2xTkLgKEDm/9IpZuyHOaxca08medwCIzBW5ZXNyWYDIz3xwPSMukV664IQoQrcBENnLU/F0HVz2H4VpBlpFe+V7ghPypxsASYng72o7pQVEzqTO1YVul3jve7YS1cgZygCQ1FB3810dzCkVYruYOeAV9W0XoXIsggRAkqqF8vrd6+Nn9X779Gc3Y/rdlZAaYwESmjA9fyLyqbbkn+lY9wEQkVMa0X5PQ8WdkZL1Rft+yG63mzTfK/v0Z6H9fev61t3pR639nugvaOlqI/EML5CnSeyD+NmJQ42QCZkAQMTS1e7620/cHba/QpMmtZnDMfpXInFGpzXpQ+y6b+V6s3fBdW7+q78riDEZvInkE4z/dRLmru/qaKEcCwARQ+96N5aHp2rMerUOx+Ja0AQ2ZNGYFMGSkdpd7zl+aLf7MZexiOQz1EBSrn6qW0NOiCoXAEk4HE+65udqWI3O8IG+8DEb5yxqa1N1x+Htaz/+/fo7TZ/I/EzkQ2W3hXOSExOp5lG4TPcWAEmglg6OO9d7MB2TgLmJdBXGa3OLgr7lH7NVC+5cbeYRQmkcE0lbrStaZAR+DABJlHImOvNySQOWyNIQisRx1mEd/mFa+dLd/zEZ1X9IE2ampTF5tlxFx1gafwqAJCJW5Sl++jmkNqioxNcW0ixB6uya4PJ3fnvDoAqnMmUboUnG4WtMpkGbi1BPCFYEgFxW0Qd/DzsndYSoy5XF1ftlyztneh/GgiSWnxl48AUD3uTadGEQzbAm28oBkEspMjMStOuSMeiKZjT0UVevR/jPjZtt2gzIiqh0PS9ak25WH0oVEv4KAOQSbsd24DGlTV5jE2UwFfmWZ4V/3+urxNPdNdfmjxSXJt+yx2gWE009A0AuKu/IY6sWT3IRIckRiz6h1GIp3KZN344qmqYslvlIVArbjvkJFMYqAUAuptpBBYkTEiwbFsMVhcItGAtHJixNCWFJNTMvlQcQKUSnZmarGwC5QDbQbTJJ1w5LU5hGU133rd20wmog0nD3IAwq53FUOgt/xL9FQMiaXgAkXs9jdJjUSN2ewep0Lw++XsaX065It/EONGnwB6WdmHAkzFdUjAMgcck1NqXQyuGbzfKTZXtfy7GXi4zpFM6iMW1hYKZfWjO7hfmKnzYBkDjUMDGAc3I50tA8Njhd/yWc1Z4+PbmsUV3T1dsvtaGbMfTN4QIeACRW3XQ3sbJaiqyBLBo5NaDrmSkdSk9omtLqpjbkkJtzCeFJfKscAIlR96wqXG7rkMUw5+Z/lMrRUKpXwdN0lcFU+fR+qTyM7RBm/7TdAEBikeeQNZFybHmlSc3J6lVl9GFWKk/QokmGcJRtyGfdVRvRk9m3AZAYdtxHQRlfnEboF8duKjsUqXoVD00YmLayZ7JqAL+uQQ+n3SwFQH7nexwM6AyyXnxZ9r4/POE8PBXLFwne7O+cHpXZuvPa0ePJfQCpDADZmc/Tyf3bTHOOwE5vAZdyxVZMtmJqc3dBduvulTChF2tqAEB+6Xy0FPGqFDimqPiC4/qt1Jme9WVWxZA+a2s1Is9iDic6UrPa+wDIL3RQgKVItRONZRWFX1hS4mFpSoXrTJO+sZwrcl14viH0rPonAMi/aulxCEudelka0221y/52KprADFiuP3jcLOuFN6FGzyvz4VhSAuJaZlPsUE/jPQVyLjuhVXgl73/Y+WKsUSlzHejRMxt9AMj5+nuKIRWpJkq2fFA4pzYUDAY2ZyLKFFCjUOCmeg6AnHu6anGaM3zQZwL3DZ3arggG3GOj9VeUqaHuGjQ9jp68CYD8rLq1kw+wtC8XpDoVcbptmELPbcMTxXUPXMoU0tIRL+TSvQDIzy7acx62j4txQVGEiiTxIZ06Wx9SDAYmNm72R5Upp1ebWsFL/y8A8oMiWxoSbhqIPWZLUSRJYjhu4BlC5XDkWZ0964GV8Z1ZZQorLFw0xbcDIN9/O2oLzIDHL7ztqqoqEj8VPzTEWSxa7BSJwqLCJputvb3v+NZMRW+jR5n6ahXKJz60ASBnlTNSmQUUnBV7uj/wPM+ZzRa1RmPKdTqtRTabrdPX53t93NLifj++N9rb/CDnmjKd5K0RculqAOSMeo+y4T7Ar0WEGMbptBqN3m4sKOypnjoKtKys3AuH9560tu7eiMzOlrs8S/3KtFW5DRfq3OoAkH80U8Nn7PEKBZ4wznJKxX7u4tqLeffhHe9tb319/bXu7qVoNKrMHLkCwk1sxg0ARNA0gWUkHzRLqUh8SKvRT2Y9LBl5v1sezSwcztGKBb0bpgMAQR+NLT2VeXxQJGbAKYfT2TR/Z/eBEoS0jAo/6ZMyAOSrZgssGReh/XJNT+F65//ZO/O3ppUujifpmzBp0tqVvVAsO7VceAp4EeoCqGwiYAFRUZDLouy4AYr4Iojb5VURl7/2ZZelbUqatOnp+f4+M8+TmU/mnJkz50x2LHeeRSaOqCx7L38NXY2A7LgfzsRyP/S0YMzf7B06d62tBHE4qYH9KiFs1hUEZGtHnWISiA/Obvr5rfX6Sk0nkhBEbftVQioKixCQhRskQdxzQojBm8v3zJVOe5CCEEpz83s/E+oyAuLzViQGHjraYqUmr5UsIAFSahX2q4Q8SnRAXE5TImwfhLXppvIq23Dth6Wh/WR8QlWCA3Jr05IAfOhFMatvoxsXfriaS9+LqeDdnoQGpKj9B3w+WAffsvFuDZd9+Hp1cM7bfiWRAbmWZ4fOB6GthfXLdQm93E//b6jZrzPFOgcSGJDaLB40H4QQm7V6fdyV2NvB9Oqpm3zO2391S1XXf09UQP6qAH39QfSscKevFu2ldRnZG6/u53Il28nRGscTEZAlK+jrD8IY/DMNiEdSSY6MRk///DsJZ3DkZr3+XJ5ggBRl0JDx4Pl7i4jHjqM5KaNR/9GfJ6FN3smBhkQCpMkJ2L4ijHCvA/HY1cQLGY2enbAuOMu/T4dXEwaQgTy4/nkmbdy8fgXJ2JWHl/MpyuiTqW2IyHfNNCUGIBf9UPkghDMV/lOHYOzrnFWWg0pzAc/Mvf6JlQQApBxseDsxWFIeIxWHVC3HR09Ko7kgITs8nVMKHRDXlACTj8wKJnsIr8wPa4V5JqsdwwV9acbfeVlaDhmQ2y+FTJjbhz3fh/ceR9X6+62sdoWhspczd6qaPGAB8bQCvf9gzTlj+Hj2qMZfmuQ1PMjPG/hXZKF9ZUABKd/4qgO5faTmd2A0+4njWqFdXsP+1NCfm3NcKn4HEpBzLAsRD4PxC1pXJ1Q3OtIor+VBmamgMgiFNzzwAPnEQOSDE23r+FDwpJqEEZkXF2PJ4TxC83+ABsi4HmCACaF17U+QhgDHlcXGZpmpKVatYRwaEnYkhme+agDiKrTB40Mv+ocRhkCqcVh6ZKab7xwJ7+OL3o33cAC5XfUT3gGWTnQPIAsB9cBheia37cdwrVvTvVdrUAB5fAZe+vav3kcYlhhYBSKbKTt2Sh/uWSfh82euwADkQoYB2gZCbMwAJkcMokUTc3NabuNvYTurhP2RNwYBkNU8aBHuhDNn4fYRVHad44HsmJDZU5zmEFrwxT8gaRMOaHyw9APEIKj6bZz+hezWQ5bTzITemPE23gFptEIzr+jsy4hBUL33/6Jn5R/C1ltONxuM/vX5uAbkggNYdWciZJUhBsG1odOLVfJj0zrMpw2F431LcQxIXT60G0Lz/CpSEFzjmwxHDctvP2Y6dTiDZfNV/ALyBViKUZLc/hkpCLUDiIT1R1C84IPp9HNizx+OV0AeMaBuQAiX/AaDS0Jp6SVDmG8RdDCdK+fUROyJT0A+FYIysIiOqceHgyH12koRujISQEZk/bhS+y7GISCdbgso+8rAdiACoT0QJ0txYiTvY24ly7LJicPZFH+AVDKgTrA4YyUiIHFIa4q0AmddoUHe7NzPWo83QEozQL0B4VJw/5BQg7jlcpquR9JFWp7cRcOww3EGyD0BlH+OfEjq5taMk9yIgtQ6nXIByWTZ/rgC5HoyoBNeojMiH1KqtWyZ1KI7oj4K9ivdyjpDeeOJH0A6UzhIfFS8RgCkVE1nUiT5YUR9uIrt8mdJbx3tjBtARiFlUeT457j+JU2G7QqDbHNaRJ2c99kjmajk3s44AWQsE9IGYpzB+0EptW1uv6s2bpyNJSBU8ujFuADkcw6kMp3Gp3g/KKnH24kBucx3kfVye9Ib2b/MfHU1HgCprICzgRDhWxqufymtZm9HTYgTEeZSWDh1OO/x2eKv1mgfkFonnDyjhEnHqh/SznXPdlgq+fkswn4WKs2RzpfYtaR1QNbqTXD4oLNKcf1L+5w7M35mvibmgGxZWe73GgekKB1OHl42fR2Xv6Q86eyOeTOTFHtAKMqk5mmvEoC4vWD40DP9mL5EWg92curqLkX8dKmkQwFAqOQvT7QMSJkRjoFlb3Hh8pdU907mM/IjJ/KuBk1KTJvJp2VAsuG8AmFZDy5/aZ+zfSezE2EUyGaxkqLIxs/3axeQQTgeuv4jFjcI6wpk512DTkjSCiCES13ULCDNYDYQ4hjG1S+td/7dGTc+1gwg25GLcxoF5DKBckdIhJbzuPwlVdCyWxaKS/VoBxCKoi991yQgaX1gMo0yeZjhJwwN87sPRx0zSVoChBKbx7UIyBwFZQPhCBpYYWg14+uuv2au0RYglGVeg4A0tEAJcyfiBIYohqGcvYejFp9LY4AQo097gLwQgCRqIGz2Eq5+aW3snVlytDL1hBQEhOL4v7UGSOd/RDAnWJiiOgx15+r3DjR6PJoDhGKnxjQGSJMdSqYf4R6ufmnVze6FbXOUQpVnFQWEErveagsQ930gfOh047j8JXV2co+PLYetQIuAEKG3QEuALIHJZJLyCJd/GB5n/l6SHi5fqZhnZQGhOHuHlgBphZJrlNnEJO7Supi3t4FkMpsl2gSEMvDdGgIkF4gHojdfwOUvqSct3j2DQW8fTNIoIBT9SzuAbACpBkLu+wpw/Uuq0bo/3T/ykjQLCPE+1Qoga1DCFNlLWGVNWssHfBDrJ+0CQnEV6xoB5AOQKBNCv8FXhJJqow6y6DJdynX7PUX56VQ0i0MEgPQCCVM0TBXh+peSy30QU0SS7yrX75xJ+R8eM+nSAiC11TAsLML34vqX1NCf4i9Ci3IvwBcUeZN+TBX6ZS0AsmiHsYHoTW24/qW0nlmxP9s6haKwdgGpVAEQwjinYw/I9E0bDBfd4sP1L6UB/4E5TYQZl8YBoYhpKPaArOTDSIbFeacRACkHvf1PcSQ665qSXasCCEVyi2INSMnfRhgbSOokAiD1mx81HTJfZpTsurwxVZ3QiG+xBuRWFwwXXW/GRCaSDvqhvDW0X9HMxed71CncR1LmYgxIE5BkP0IVlgKRctBtf+67iPhY0b4jrQ8STJls863YAjKUCoIPYr6LBIRWWTp7aAPJULbz8z12lSZW9MUUkNsUDBfd3tKACIRUrdP2hw9iXVG2d88Er1b8kP9TLAG5lgvDA6ErEYGQ+nzzsKkg9Cncvfwy0JJbiL3nSQwB8cEoik7PX0QGQsnV4zjkaxoopas5pVWrBciWiVMWQ0BSQcQpEr4YGQip4jOHJpqkKp41pEHFvJyCIs+C5QFSkwLiDIvLGEQGQqnDfHj98i8Vz4w0naveQiLG5ZgBMvQTAiCEdi4gBCE0d4QPjjmn+Ai3RlScX2Y2ZoDkgbglJOJzhCCEHuoPOwjEVnxW8SHKUtSc4NwXMQJkdQrEIS8xYhhWCL0rZI5cvWWpEHOgSIGp4Ee9hTECZJiAyNZAO5GC4HrbfuRBHBE2VBikUtWIPpLyITaA+HgQPrq5AzEIqrauo+996NlyFUZ5oGriqEy6ay0WgBTkMCBuQT6+Rw6CKsd7hA/yu0aNUarUze3MpS/HApC7fgMEPhhMxxtc88JRPoz1qgyjckw4Ed2xAGTRBsLCMl1HDoLIlXMs5ZnYpU7xFNUiTfav06cGow9IOYyMo8SMYSZBtFZ8zMlkqYfqjPRb7Um2T65FHZBpNw/CwrpahygE3j+GyNFjfML0l6syUueI2rPMZnVHHZABGC6I43k5shBQN3THDB/xqkoXRm+tqtsJlsqoAzJmBBFnYlxGFAKqnjn2AzSkq5XbW4W0cScMhfn3UQakfMMMYQNhnfgYPaBaTccCbImgWl6L4X/V/xFaH0YZkIYJEC6I2IsuSCDNnAjUtnWpNpg7CrnV+P9GGZC/mlkIFtbPRoQhgH9+w3Y8ikgnqLfVzkYh6JVYp6MLSG0qBBeEo/6HOAQ4v6o4HoZKTIvqjcdH492d+Xl0ARlMgWBhsXkDyMNxLdyoOGEdOEZdqo2X9isagBj00QXkuQPELUgfpjM56X8wJw7wmaxS9ca7S0cjKpwkd0cTkLMTEJJWk/tVyMNxvTGd+KEbqH9UHPD/7J0LXxNJEsBnJs5Mz0wSJi8e4Q0moLxZElh5BQRRRBRXjpeAiIIIylOWFRFBPCK3rIurt/rzyx7c7d65ZwhJYKo7lakvMJV0/burq6qrKn6H8NaJMgMJSG0eijt6wLyj/59U1n/bLJNYO4z85HwayHVWzG8ABCTrHxg8LMm/ZyLxF3n7TPuWD2HM0Fh4oZADstinSXTGDUiVCwMgtv6/m0z8hY8D4dv7gI0YmkwNAzVAJ8IgICDTKMYeiL5iE4qvpL08LUJAaWTN0I82Q2XUhCv7cID0oqh1530mFF/J5VLl2/OD6Aa31ZsLAfX+kLL34ADpw/DclggdJhX/k5ufIzQZIJ4XBk+GeCkBpZxJZgccIAEMHX+IZ9bE4r9SY+Uj8MH37xj83YtgzohyaxEMkBEUte5ajcnFn1flvfxIToGYvWbwhztvgVW9WhZ2oQApceEApMkk44+81m2SFsltF6eKDP5y829g/TmJow0KkCoUo9dUpzl47U8/x/s5Yvb5XIPRX94IwXnrWksJECCNv6AA5BeTjP/IjBaIGAZXDOdDDgKWhYt51UCArH7BAIjNa6JxJCvLzojLaRnZN/zbxR2ACQPi6gICZMuTgwAQpdyE41C6S7XIfGgAc1OqQftzOh4DAdKCAhBrj0mHvF4RilyYLQkTAJ8f8EMmDJTtMhhA/paGIZGeadbyyus/+iNv4Sq/BRHCgG2OI3kaYABBUexu9nU/up6nRV5K4i6EGJsSnoB9eHf+Pgwg/TgASflE+ttC5zHPXYUQSEvWkjHYmiXHSxBA7uSj6KqYMZnifNwNeY5xcMT3MP3C7thhhzAJV0AA6cQBiOtCSuNRtmkVjuHDJqzB6ADd+0OygwByEwkgXanMx+K9jOMCSJL2EkiJeQ24usjVDgFId7bFBCTJZefcsVPuVe1eGEgLP/BOS+w1EIDU+CUTkOSWh8dkz4+M6MtwCZAW+yPAS060axCA3M02AUlu+eDlj+Vj/ClYv2Lwl9s5Qp8JiAnISXJ52245lg+r7wmYIgU89JqL/SYgJiAnyM98HTk2f+3OWwRTpEQBtyNbfjUAIOYdJImlJKhHSfMKXsDY9x4PXhQu+X8GAGSVqCYgSSrN9c4ou5uQPweoS70ADojKtQEA8lgiJiBJGr3yRXNrxOwHgLp0wl9BOCL2AgAyiwSQ1MukD5IoXg0Rx4OQyjRSuMoS67AJSKyip1pn3lcLmiUaHwHY8uaWOgpmpGw3GA8IEhcrxap5i2/bo/ZRtwDz0XqFp7Dowmin8YBM47ikp9Z7kAvPPFELhKTcIKxCa14asVDxUrPxgGzgCPM676UOHmXTBblRj32iL8FqFL5HZZC4eL3bTBTGKO6nKcNH64wl+sskYl8CbhJWTae/s2VhzQQkZnc0Vfi4mueJumI5qj7fAKzTnIPKRVZa2DMBifW/sqcGHtXlme+jGmOOpM2vAys1dIPOBA3iuGY8IDU4AFE/pQIeKxVeJ4m+WR/yAa7WTZXSm6LcD8YDMokDEOK8iZ+PnXr9pA4bkg7Ph7yaTis002s8IK2hAApAtC7seFTeLj2x962Fwvkhy/3vKa36eEul4YAgadpArBXI+XhTLvIn9cAU+eAQvGZV1AZojA82GA4IkrY/BKS5JkWZ5058cEFEhUq6tMdDa9XHt4oNB2SlVMQBCOo479wnLUBO5CMQpKFblofaLVYofGU4IPJ1FICgnnK7M2y3nezFCH4qfMgTAqEHyBPjASngUQAi5j1Bikf7bCg3BhNUQN9/fBU7oGhAIID0KBjGH3CWEM4pnuG2UUcsZ7wV9P3gV3KVk3ADgmOADqfmBDHycb9HdMfiwdT5KDUnXplRCG5Agk4UD0KIdRAfHp03Qu5Y9meSu7xDScWNBYqdOUEA2UMxxPNotHwWMjyKvhc1WyyLo3oKqynpWDyfSZAD8lHHAYjo60KFR+VGXrqFxLI2Uh29AY3dfpqtnUEAeevCAYgaeIcJj5pbeozRIfH3rVpqIYQbuRx2QORHOADhHFN4+JhsydFiXBbe++M6NT2reAk/IBkqDkCU8kUkeAwM93ti9VwE0kaPD3nYyeEH5DccmULOlo+jN1Znfcgjxnh8kHFvN0VVqzNICgAyqOEAhKRfRYBH1pLHY4vV7CRPAdXQ3TkPlwKAvHTgAITTtlaSnI6y/YkvDonEyodNOAjTVHcgU00FQCYzkABiCyX3q8Jw8+yCHselVyC9RVQVPhAIbUAAqnnhZ2cZJq4HyczH5Px1ZxyV1TnW/Da6fLz20x5v6e7pBAAkzFmQAJJYM2M25MJMf50Yx4aco+VRToyu9LhpJwjGLzZAAPJPJGEsjrjKkhSP7tEFazx4EFX3VVLW+d176u0+xgdXAABZX6rD4mPpm8lIR9FeQbpgiWc3JqJGvTRzcYz6AcJpgxAniNyoYwHkfX/S0RHeX/W6eBKXralK3kPqis8yUKKU+70MAcgAkmqso1Z7A8mFxw+7E/32eN88S8rYfeqa76gM3FwhOiseyhMcjU2OABFakgmPt6sz2XYx3t3Jot9joKamx8nAcvNBEEA6yxU0PlZBZ9Lg0dUxKmrxb8NCZuMQfeXvsvBIQvL/CgJIeCoTCyDk8+MkwWNtNJsX4k5EE9XeX8WC+n4WIp+WUCMIIHKFjuYSovTdSQI6WjsyHYKFxP2vE7Fua4iFH1DvYcFixEsDMIB052NJFXISYb1isaTpp+fpTokkYGFEu87Gr1vzMmEwfEIv8RMApJXOgCBDxFP+lmE6hpoah23pCZYw2ewzbATp9rcVJlwOoTALBpBiLBXvRzkCfo1ZPHbatp7bxxPNP1u5a2xUK4eneDbe2CkJvcVPABB51k3QEGLdrmSSjsV35cuqk0/0jyb68zeM/JINCxtpAeKuhwLkNZ5LyKEl3WWPjtqHt/I5LfEH3IS3X2QlgF3sYyQrQAI/QgFS/YJHAwhn83zHFh0Nc2MjuYJ4itK+gHthl5mfc+BgxN2QvBVQgMg9iHwszt7LDhydTe+209MVQk7x/xKrv76WmV+0ykwXHMvCfTBAHgdUPIBIn9rZsKX2j9OFUvppIz4W5cUeO8Q3eZiZaSmWVoIB0pWN5xLCEeUZA5bU/GvHmN8unNqc3MJUOzt8LPYJhB1AZDBA5HyRQ3SEiG2U7ajrZc8lryO+Rx6RYU9/3j3EDh/hGwozfOQoPYCAzOMYgvCHVQkvaG66kxcLsiX3GfQcJKr1fAdTjyQ3eXY8DeIMAgLyRkd0S+eIfYtOP5yyO5uj6XYrb1HP4O8kvPsWW80iN4jIjpkQ/QIgILJdQgQIp2qr4NaT1d695HPZBfVUIauvXGxluY0pPOTmUoGhbZQ8qoUE5KkbEyAcXzoAaTpDrRvBmVBG4onyb+9RmdksXc6P5IdlpiqSLJIMCciDDFSAcHV9YIVLnR+nb1wRdavt7LZXIij1rM1c/K7HztQKe+pBASmpQ+VjHV7hYJpkNa0O9l3ntbMdAyDpvkbmKsqWHGxdU/WfQAGRn1pxHSGqc9Zok1nfGCwIEUEQyVmaDpGc59vYK7jsVSS2Ap0jTbCAbGQQXEeIRTDwjlu2sjtV+shu5QPq2f5txJbGs9ggcvOzjS37sPmLYAG52S/iOkII32/MaOSs6o8fxpwu5+HBQc7aaCTPQv0Ag3ysCiJj+6enZR0WkPBULodMrL7dszaUy1WvZ2dKz+cKhtzY1Dp1eINBPOTpNOZ2Tz3R18eJAiI3ulVshGgFZzl/KbyzN3FwSbBbDTIW4q6bqWHytde0whwf5NMANCBPCgUO3RmSf1b+StPqxe3lbKdVNGoXIbzed7eERTzkVZE975u/tAgNiNxhRwcIp5BXpzeQ9nf/jlYpomSUH04k4XzBXoPMJh9EZC9+41xqAAdkN09EBwjhhbnTBKuKWoMHustxRtVVxykZEGy+KplR2VQY5IPoCbfvThyQ2nKFICSETCdUEFv0L/bO/KuJZIvjvdh9K+l0SLoT2XcSQEJcjhCHE9kji4DsCFEQRGWNCrKIgGwPQxSdM6JvZs6b//UF8MzojEqELHUz9f3VEFPd91PLt27darhUuj5E/GaTHAO36nNJptrOGVrxGJzmKeSDkxwD8QcEKknSLdMPn6Wt8UfLJ115Ohb6aSEnPHLEPrtbsvQ93i6glY/CexYqj9KNeNwJAMRdKyUhIJxsdV3/gZFjY6273VVitdri0HMSXs85aKX3Xqz0aiuVfSYx/QQJAATaTSQZCSHZeRHm7dQsN26tlAiaIT7bximpN+tqgF55XVY6A0Is2U4IIL3+pASEI4I4dGJZKW9zW25tBm/jK+LxEAgRldeOtSDFeEBTFa0dpuGuOyGAwDkTl5yEyJqx/Fvna8qC7vFGR6oenlRVyHGKCFVQSfUlmumAskWeVtOG8PcgMYC8sZAkJYSoE66xf1jnwYaN0sX2nNcWjY+xVfW5jLy1L3+9mGo8oLjRotIaDGLmqwQBAu8FLmllsrSXfmYXpXvfPHgYeGKxKHG1Joh4VVjZfBOkGw/wtlF8cyXvgEQBUm4myUuIUSmZ+3CcVvOh+eHQaubbNENFnNvLm8mFd7uU0wFlIYdGbyCQq6MJA2S/K4mHEI5I9j7PWKijcyWHO9wdj3cMyIrfseh1044H9DRyAsUdJSnyJQwQ6NY5LqkRETIPnSop3geMibFCKTK3714D+uV9L6g0TySkJ5A4QGpWBC65EZFlEv//kxdEzlMKKDT6O0/1PJucH08gIDBlIBxTdE1JhVS1vSxEQUdB00cLoTsCxIxriQRkY15gMR1NOrLtVY+p3i//XPUtGXbaO8jsRkgkIPBQYkNItKRazR9HQwNI6IAXzXcVlfZnKgtjiQXElymxyI4KHdoEt/ThVhALHnC9LQtBMp5psjixgECHxoaQM86riMi/9Rs7dp8BHgXvZRoQFA8k5ueQYEDALLIYP8scgBdEZ07lU8CkZ91Gs4qhYzTkX0o4IA8sbAg5/QxZy+hf6PaiogPS1/p1lWB462c5CRI1QIJ5zMg6HR0GMze8+fwSLjrAFwroWN44nzeQeEBg5lc2hJyCjvOGwPTl+hfI8HhW5+JHsLxvwrcDBYAUB67i95AUi+4Pq8iixWFjmNj138698/oAnZpXOQVPYX8xs4kGQOB+hoqYjSwiaX5n+7uBa273s/pQh6PILMWsag8hsqTpE57xniA+OqAyR7Zhuvfif6tABSCord4sSSiZ+2Id8GK6T4zNMEIkQcooeXwHIRtQtteeauZRFbIh572UALIzz2MlxKjljDb8o0GP+nk12vdbEF7hHVujYxjpAF/rpK7JBNdbtnUBJYDAOo+0RpYkBb46TZ3tyOCzokpHtmm1enqgACMdg2OVnVYFXRdILBepAaQwX0B5czqfsfSNTIQXy7lCFOmwrIyGvIUoB4/6xdt9VoxJ20obUAMIXJ/AOMeSuMVvl2HblVKiY6Vkn59/2HoriJKOwpnqfllDmZBqTGuiCBCoRljsXRRefhf6Cv6sQ4fIW/320Y1gGUo64EO1kxOwzp7N94I0ARLMRbefTtJOyEN4daabLoySTXLOr6fjZKPA/eqCriuqjNV94W9GpQJ+1ACBNxK2pEVl+ETf/9T3NRP+qtGxtVSDE469mtBtvkjBXJycCKMv6ALkSgcyn0M0NpzUpp6F05XrkBRrf/Wj6yjp6BkYX3I5UxWZwyzCf5wFugCB2Ru4KpGmdp/cpu3aih/+Xtmul1z4eQejZVXgnTloq+KtBjGLwy2jWAm0AQLLTzBlnPB5kRzB6PyhIYQQIqQqnuUNN0I6vHWP8+dzY3ivYjwlzA/SBwh4EN2HQFIfReTjRF4SixhFg+X3u8tufJuBg2PdWyVOI2+If3W8GL3diWglLEQVkEKHgOb5Sjd3ImpTpEfujZKJOId/xuXnlg0WNqytB+yvdatBiuW9ivFWWhvQCAhc1NAs7UY8kfmvo2mRdFiSQhxbzXiOzRYU+zZ27rzsOJfrP2+1yVySSZLddAICo1iqWZOR9cha1HpycVWjzZozhMSzcvtmm/4TqjuoduUYdLMmqFwSimS/A0oBKZh8i+MZylykFwO/PmnwMCuu5qav9FjFPUcqLm36QqVP03s+qSFuq5X0Hm/TnfK6h/c8bfl5uVZL9ohBSsYrWD9NsBae0QoIbFThSGuTHJEm6jjF75lWttSSqYt/7af4fJcvh1qmNl2BQGfXjSN15f1NK5/+IazhwJFcjw8OtbR4Zy2s7R3f/v5+b++VM+5n9O7Nbq/dnz5o8bjCv6Uqr8/JWTW7TZDEJD8hzedE8UhBtAGBbQFFORh+fiDCBvV/MyGLyKaJhWPO6lvLWzZXa3MznxzCJwo2m80gpBxL+pv4lD91+LmwTLx6KEnOOPpeZ+aRanOPlDd54Vie5fIT9ODgwu3wB9tvHv7d4VdwXEb4a4Xwb+F/kVRZzuL+BTLal4BiQKAbQz0xTthqOCsghKudS9+Y6Qjk+f1FuiVbESSpQpRlOTyw/GAkkmMZ5SOJx1I/8WRSPslyorKPP8gf/l1F+CuOf8y/q6QGsQWuUA1I4W0Mkyyhs+fMgOTmV+m6RTPwrKgLPcpScy8C1YDAfheC/UJ+NdI8wpVvT7EklaFB3QAS2QZwIgEBL0d/PWu+P1JbtopnUYdI2jmgHhBY1qnvWcWSyxE2JtXIog6Rg5XZiwAQqPTTTgixL0bolvpZ1OGRrH8ADIBASxrtVpY2F1m653MLCzs8Dq82DTgA2RuifaNWyK+PqCUBG4s7NAt0YagHCSDwNJ9ys9cobEfSjlspMgs8LHykVEX/iHOsAIHdecorZWmbkZz5W/qNOblYJEpvAA8gUKPTnfMj2yMo3Nrbz3YB0QwgejNgAgRai+iuN2a/G4HZYGN8YOHDsgC4AIGmP+jeY5soPzHz0smu8MXCh2kYsAECISPVhMjWEyZZl4YVFnlI+LDf8OEDpOAR1Ukn5L/vv+t6pFcrbIKFRKaqJsAHCJQtUn3emQid39kMKWvLZnwgEZ+5BhgBAWimuuq7UfpOUm+blfGBxeAVxwEpIPDST/UYwv9x/+u/u76fza+wSNZHAS0g8FyheSuBiP6F4q+kKE4ZTIwPJFKV2PERB0Cgzkn3ZpsmTu1+USOrcPZdXirLMEEzvyItgBoQGO9LoZoQNS3X09xaf1Qqxr038KDxps4OSeGZX9k7ADkgcN9poPoZE3UkLWfYs9nRMTfpqjKksd1BRHwYbvegBwTGMmkv5EBUwaRommYySGztgUjEPhnbSvrxAQT2s5gnxBSD8cPSGePIjRMgkO6yi+x9MkV59WiagyQBBHo2VTa1Z4qqJHkJkgYQCK7LzBtiiqJ4eQqSCBCA5QyBvVWmqPFR+3wwuQCBAadG2FqdKSr21S+/zsQjZuMKCDztsoiMEKYo8GEwx+c+r/gCAtBC2CFvprPbuzbHHiQlIFDu0BghTGe0r9S2YkhSQGDAZWU7IkxnWp6TlmuQtICAryWFuVlMp5eBCwUhiQEB2C6xMjeL6XSrc5Lt3IljrCYEEOhdsLKcQKbT8KHq/2/n3n+bKgM4Dtsz27ddu9kLk0EZbLJJNo3DBNGQqVwEMsMusmwxw93JRjZhJKIgcYlBwQ11kBgS/lwLJAYNEfeD9O36PH/D95NzzntO++1m2PWBhLBSLfhJEjvuI9l/6tUOtV6BhB/HM4mLCDvSUpj9LTRJIGHzeHeniwg7Ob3KjW6FpgkkhMH1ss8X+c+3V6VDt179SOsZSNjcuN6ecZ/Fyx3Ipvd++GloskBCeGugtEcivLSPzLHWB3VZaJ0DCWHoWouHdV5yd7Un+8mR0JyBhPBgtuwDRv4lj9Qbt2/Va50RBBImbv7xuM0OeLHM48nhrdDMgYTw88Lb7a2mwAskpfXzdZxmHIGEzaHRYt73Wfzj5iqb7u0ePBwEUnPk3d5cm0R4ro901/6HdZ5lPIGEMDhdTTyu89ezedK9+k4QyHPujXS3S4SneZS75/fVf5JxBRJC/8JUyXsReaTyU8v9MQwytkBqV5GVL4odEmlqqWLfylAcc4wvkBDOLF24VG5xptWkB1fZpLfv1NlYxhhjIDVH19OlxJlW82lNFyrnvo5oiZEGUnN/5lCS8zdzTXX1aCtkJ9fvRTXDeAMJYeiznkMdSeaA5TSFTK5j8s7KWGQjjDmQEPZtLPQkXYmvUJrg2KormR3+6nB0E4w7kJrTd5dnyu1Ofnd1HenO4jfLd0/HuL/oAwlh88zHx89VirlM1sHWbjy0aitXqvOLW5txrq8BAnl29Puw59j2XJJ2+rub6mhJJ/nia/OLEQ+vUQJ5+tC+dnuy2lJI0v4MpeEdyLal5tInD9459Xvco2ukQGrO/rQ63XOi2p7PpVudbjXqhSOTKnQmB89duLl0I/rFNVggT2z137o5Mt6XFDvnEu9JGqyND5L8djH7w/Tod3E+k++GQJ74aGxw6f7wlz3VSqXYlU9SDoKjfweYypeLeyvXxwcuXn40NNEwS2vQQJ4db7135ujVR98Pj8xMlX69UkulVCqVCzki0lUqbb9ZqVy6cvLEwPr9jatjEzcaa2SNHMjfTPxy/vLq2trCtdnXicbM2traxfcXP2/cXe2aQEAgIBAQCAgEBAICAYEAAgGBgEBAICAQEAgIBAQCAgEEAgIBgYBAQCAgEBAICAQEAgIBBAICAYGAQEAgIBAQCAgEBAICAQQCAgGBgEBAICAQEAgIBAQCCAQEAgIBgYBAQCAgEBAICAQEAggEBAICAYGAQEAgIBAQCAgEEAgIBAQCAgGBgEBAICAQEAgIBBAICAQEAgIBgYBAQCAgEBAIIBAQCAgEBAICAYGAQEAgIBAQCCAQEAgIBAQCAgGBgEBAICAQQCAgEBAICAQEAgIBgYBAQCAgEEAgIBAQCAgEBAICAYGAQEAgIBBAICAQEAgIBAQCAgGBgEBAIIBAQCAgEBAICAQEAgIBgYBAQCCAQEAgIBAQCAgEBAICAYGAQACBgEBAICAQEAgIBAQCAgGBgEAAgYBAQCAgEBAICAQEAgIBgQACAYGAQOD/8Cd1jFA82xl4XQAAAABJRU5ErkJggg==\"></p>', 'notification', 'high', 'admin', NULL, NULL, '2020-10-15 05:36:45', '2021-03-14 09:05:59', '10001');
INSERT INTO `mscode_notification` VALUES (100, '系统消息标题测试', '<p>系统消息内容测试</p>', 'message', 'medium', 'admin', NULL, NULL, '2019-10-15 05:38:00', '2019-10-27 16:45:36', '10001');
INSERT INTO `mscode_notification` VALUES (101, 'test系统消息标题测试', '<p>test系统消息内容测试</p>', 'message', 'low', 'test', NULL, NULL, '2019-10-15 05:39:05', '2019-10-29 16:52:26', '10001');
INSERT INTO `mscode_notification` VALUES (102, 'admin通知公告标题非时间测试', '<p>admin通知公告内容非时间测试</p>', 'notification', 'medium', 'admin', NULL, NULL, '2019-10-15 05:41:00', '2020-02-11 18:27:55', '10001');

-- ----------------------------
-- Table structure for mscode_notification_sysuser
-- ----------------------------
DROP TABLE IF EXISTS `mscode_notification_sysuser`;
CREATE TABLE `mscode_notification_sysuser`  (
  `id` bigint(20) NOT NULL COMMENT '消息通知与系统用户关联主键ID',
  `notification_id` bigint(20) NOT NULL COMMENT '消息通知主键ID',
  `sysuser_id` bigint(20) NOT NULL COMMENT '系统用户主键ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '消息通知与系统用户关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mscode_notification_sysuser
-- ----------------------------
INSERT INTO `mscode_notification_sysuser` VALUES (676736282084691968, 101, 653918847061381120);
INSERT INTO `mscode_notification_sysuser` VALUES (820253808747008000, 102, 653917974152592329);
INSERT INTO `mscode_notification_sysuser` VALUES (820253808847671296, 102, 653918847061381120);
INSERT INTO `mscode_notification_sysuser` VALUES (820253808931557376, 102, 656413611073654784);
INSERT INTO `mscode_notification_sysuser` VALUES (820253809015443456, 102, 656414601122336768);
INSERT INTO `mscode_notification_sysuser` VALUES (820253809111912448, 102, 689718972811497472);
INSERT INTO `mscode_notification_sysuser` VALUES (820253809212575744, 102, 689732941378932736);
INSERT INTO `mscode_notification_sysuser` VALUES (820253809300656128, 102, 689734818510327808);
INSERT INTO `mscode_notification_sysuser` VALUES (820253809439068160, 102, 820241798336532480);
INSERT INTO `mscode_notification_sysuser` VALUES (820253912212099072, 99, 653917974152592329);
INSERT INTO `mscode_notification_sysuser` VALUES (820253912304373760, 99, 653918847061381120);
INSERT INTO `mscode_notification_sysuser` VALUES (820253912388259840, 99, 656413611073654784);
INSERT INTO `mscode_notification_sysuser` VALUES (820253912480534528, 99, 656414601122336768);
INSERT INTO `mscode_notification_sysuser` VALUES (820253912593780736, 99, 689718972811497472);
INSERT INTO `mscode_notification_sysuser` VALUES (820253912681861120, 99, 689732941378932736);
INSERT INTO `mscode_notification_sysuser` VALUES (820253912761552896, 99, 689734818510327808);
INSERT INTO `mscode_notification_sysuser` VALUES (820253912845438976, 99, 820241798336532480);
INSERT INTO `mscode_notification_sysuser` VALUES (820253981749465088, 100, 653917974152592329);
INSERT INTO `mscode_notification_sysuser` VALUES (820253981854322688, 100, 653918847061381120);
INSERT INTO `mscode_notification_sysuser` VALUES (820253981938208768, 100, 656413611073654784);
INSERT INTO `mscode_notification_sysuser` VALUES (820253982030483456, 100, 656414601122336768);
INSERT INTO `mscode_notification_sysuser` VALUES (820253982122758144, 100, 689718972811497472);
INSERT INTO `mscode_notification_sysuser` VALUES (820253982215032832, 100, 689732941378932736);
INSERT INTO `mscode_notification_sysuser` VALUES (820253982307307520, 100, 689734818510327808);
INSERT INTO `mscode_notification_sysuser` VALUES (820253982407970816, 100, 820241798336532480);

-- ----------------------------
-- Table structure for mscode_sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `mscode_sys_dict`;
CREATE TABLE `mscode_sys_dict`  (
  `id` bigint(20) NOT NULL COMMENT '字典主键ID',
  `dict_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典名称',
  `dict_value` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典值',
  `dict_type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典类型',
  `dict_sequence` bigint(20) NULL DEFAULT NULL COMMENT '排序',
  `parent_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '上级字典ID',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `tenant_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '10001' COMMENT '租户编码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mscode_sys_dict
-- ----------------------------
INSERT INTO `mscode_sys_dict` VALUES (1, '性别', 'gender', 'sex', 1, 0, '2019-08-13 12:41:19', '2019-09-24 18:12:39', '10001');
INSERT INTO `mscode_sys_dict` VALUES (2, '男', 'male', 'sex', 1, 1, '2019-08-25 21:33:29', '2019-08-28 10:05:39', '10001');
INSERT INTO `mscode_sys_dict` VALUES (3, '女', 'female', 'sex', 2, 1, '2019-08-25 21:34:14', '2019-08-28 10:05:54', '10001');
INSERT INTO `mscode_sys_dict` VALUES (38, '机构', 'organization', 'org', 2, 0, '2019-08-28 17:39:10', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (39, '公司', 'company', 'org', 2, 38, '2019-08-28 17:39:50', '2021-04-18 17:06:00', '10001');
INSERT INTO `mscode_sys_dict` VALUES (40, '部门', 'department', 'org', 3, 38, '2019-08-28 17:40:32', '2021-04-18 17:06:08', '10001');
INSERT INTO `mscode_sys_dict` VALUES (41, '小组', 'team', 'org', 4, 38, '2019-08-28 17:42:00', '2021-04-18 17:06:14', '10001');
INSERT INTO `mscode_sys_dict` VALUES (45, '区域类型', 'regiontype', 'region', 4, 0, '2019-09-17 14:43:40', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (46, '省份', 'province', 'region', 1, 45, '2019-09-17 14:44:38', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (47, '地市', 'city', 'region', 2, 45, '2019-09-17 14:45:06', '2019-10-07 14:12:50', '10001');
INSERT INTO `mscode_sys_dict` VALUES (53, '消息通知类型', 'notificationtype', 'notification', 5, 0, '2019-10-07 15:20:03', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (54, '系统消息', 'message', 'notification', 1, 53, '2019-10-07 15:25:04', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (55, '通知公告', 'notification', 'notification', 2, 53, '2019-10-07 15:26:36', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (56, '消息通知优先级', 'notificationpriority', 'priority', 6, 0, '2019-10-07 15:29:43', '2019-10-07 15:32:40', '10001');
INSERT INTO `mscode_sys_dict` VALUES (57, '高', 'high', 'priority', 1, 56, '2019-10-07 15:32:14', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (58, '中', 'medium', 'priority', 2, 56, '2019-10-07 15:34:06', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (59, '低', 'low', 'priority', 3, 56, '2019-10-07 15:34:46', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (690409019042877440, '按钮类型', 'buttontype', 'button', 3, 0, '2020-03-20 11:59:00', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (690409184067768320, '查看', 'view', 'button', 1, 690409019042877440, '2020-03-20 11:59:39', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (690409389072764928, '新增', 'add', 'button', 2, 690409019042877440, '2020-03-20 12:00:28', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (690409525698023424, '编辑', 'update', 'button', 3, 690409019042877440, '2020-03-20 12:01:01', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (690409681893904384, '删除', 'delete', 'button', 4, 690409019042877440, '2020-03-20 12:01:38', '2020-03-20 12:01:59', '10001');
INSERT INTO `mscode_sys_dict` VALUES (690409931979280384, '导出', 'export', 'button', 5, 690409019042877440, '2020-03-20 12:02:38', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (690412734541910016, '授权', 'authorize', 'button', 6, 690409019042877440, '2020-03-20 12:13:46', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (690413233668280320, '上传', 'upload', 'button', 7, 690409019042877440, '2020-03-20 12:15:45', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (690433305002561536, '下载', 'download', 'button', 8, 690409019042877440, '2020-03-20 13:35:30', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (690436655534231552, '生成', 'generate', 'button', 9, 690409019042877440, '2020-03-20 13:48:49', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (690437022095429632, '预览', 'preview', 'button', 10, 690409019042877440, '2020-03-20 13:50:17', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (690440262862163968, '发起', 'start', 'button', 11, 690409019042877440, '2020-03-20 14:03:09', '2020-03-20 14:03:26', '10001');
INSERT INTO `mscode_sys_dict` VALUES (690440661547536384, '跟踪', 'track', 'button', 12, 690409019042877440, '2020-03-20 14:04:45', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (690440905299513344, '签收', 'assign', 'button', 13, 690409019042877440, '2020-03-20 14:05:42', '2020-03-20 14:05:52', '10001');
INSERT INTO `mscode_sys_dict` VALUES (690441553252372480, '通过', 'complete', 'button', 14, 690409019042877440, '2020-03-20 14:08:17', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (690441876436078592, '委派', 'delegate', 'button', 15, 690409019042877440, '2020-03-20 14:09:34', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (690442190606225408, '回退', 'regress', 'button', 16, 690409019042877440, '2020-03-20 14:10:49', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (690442344885309440, '驳回', 'reject', 'button', 17, 690409019042877440, '2020-03-20 14:11:26', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (690442513152397312, '终止', 'terminate', 'button', 18, 690409019042877440, '2020-03-20 14:12:06', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (690442893357666304, '流程设计', 'design', 'button', 19, 690409019042877440, '2020-03-20 14:13:36', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (690443118273024000, '部署', 'deploy', 'button', 20, 690409019042877440, '2020-03-20 14:14:30', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (690443434502574080, '挂起', 'suspend', 'button', 21, 690409019042877440, '2020-03-20 14:15:46', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (690443568074379264, '激活', 'activate', 'button', 22, 690409019042877440, '2020-03-20 14:16:17', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (690443896224141312, '路径', 'execution', 'button', 23, 690409019042877440, '2020-03-20 14:17:36', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (690444200416038912, '节点', 'activity', 'button', 24, 690409019042877440, '2020-03-20 14:18:48', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (690444357635330048, '详细', 'detail', 'button', 25, 690409019042877440, '2020-03-20 14:19:26', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (690444491202940928, '变量', 'variable', 'button', 26, 690409019042877440, '2020-03-20 14:19:57', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (693426304712101888, '详情', 'details', 'button', 27, 690409019042877440, '2020-03-28 19:48:34', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (798063426328252416, '文件类型', 'filetype', 'file', 7, 0, '2021-01-11 13:39:08', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (798063617890504704, '图片', 'image', 'file', 1, 798063426328252416, '2021-01-11 13:39:54', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (798063838640918528, '文档', 'document', 'file', 2, 798063426328252416, '2021-01-11 13:40:47', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (798063992991305728, '视频', 'video', 'file', 3, 798063426328252416, '2021-01-11 13:41:24', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (798064175741325312, '音频', 'audio', 'file', 4, 798063426328252416, '2021-01-11 13:42:07', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (798064281538449408, '其他', 'other', 'file', 5, 798063426328252416, '2021-01-11 13:42:32', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (833267511255814144, '集团', 'group', 'org', 1, 38, '2021-04-18 17:07:36', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (852038408955416576, '模板类型', 'templatetype', 'templatetype', 8, 0, '2021-06-09 12:16:29', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (852038799516422144, '带左侧树的表格', 'treeshape', 'templatetype', 3, 852038408955416576, '2021-06-09 12:18:02', '2021-07-21 07:01:29', '10001');
INSERT INTO `mscode_sys_dict` VALUES (852039073949732864, '模板项目', 'templateitem', 'templateitem', 9, 0, '2021-06-09 12:19:07', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (852039216908390400, 'MapperXML', 'mapperxml', 'templateitem', 1, 852039073949732864, '2021-06-09 12:19:41', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (852039364241707008, 'MapperJava', 'mapperjava', 'templateitem', 2, 852039073949732864, '2021-06-09 12:20:16', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (852039472828043264, 'ServiceImplJava', 'serviceimpljava', 'templateitem', 3, 852039073949732864, '2021-06-09 12:20:42', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (852039573709443072, 'ServiceJava', 'servicejava', 'templateitem', 4, 852039073949732864, '2021-06-09 12:21:06', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (852039677581381632, 'ControllerJava', 'controllerjava', 'templateitem', 5, 852039073949732864, '2021-06-09 12:21:31', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (852039774369140736, 'EntityJava', 'entityjava', 'templateitem', 6, 852039073949732864, '2021-06-09 12:21:54', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (852039884507369472, 'WebVue', 'webvue', 'templateitem', 8, 852039073949732864, '2021-06-09 12:22:20', '2021-07-20 22:35:25', '10001');
INSERT INTO `mscode_sys_dict` VALUES (852040027256311808, 'MobileVue', 'mobilevue', 'templateitem', 12, 852039073949732864, '2021-06-09 12:22:54', '2021-07-20 22:39:08', '10001');
INSERT INTO `mscode_sys_dict` VALUES (852040120323723264, 'WebReactJS', 'webreactjs', 'templateitem', 13, 852039073949732864, '2021-06-09 12:23:17', '2021-07-20 22:39:15', '10001');
INSERT INTO `mscode_sys_dict` VALUES (852040205212241920, 'WebReactModelJS', 'webreactmodeljs', 'templateitem', 14, 852039073949732864, '2021-06-09 12:23:37', '2021-07-20 22:39:26', '10001');
INSERT INTO `mscode_sys_dict` VALUES (852101371812827136, '公司请假工作流', 'workflow', 'templatetype', 5, 852038408955416576, '2021-06-09 16:26:40', '2021-07-21 07:01:42', '10001');
INSERT INTO `mscode_sys_dict` VALUES (852101661194637312, '财务申请工作流', 'finance', 'templatetype', 4, 852038408955416576, '2021-06-09 16:27:49', '2021-07-21 07:01:36', '10001');
INSERT INTO `mscode_sys_dict` VALUES (867049514002206720, 'WebVue主表', 'webvuemain', 'templateitem', 9, 852039073949732864, '2021-07-20 22:25:13', '2021-07-20 22:35:36', '10001');
INSERT INTO `mscode_sys_dict` VALUES (867049846048477184, 'WebVue子表', 'webvuesub', 'templateitem', 10, 852039073949732864, '2021-07-20 22:26:32', '2021-07-20 22:35:43', '10001');
INSERT INTO `mscode_sys_dict` VALUES (867051921826304000, 'VOJava', 'vojava', 'templateitem', 7, 852039073949732864, '2021-07-20 22:34:47', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (867052894573481984, 'WebVueAPI', 'webvueapi', 'templateitem', 11, 852039073949732864, '2021-07-20 22:38:39', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (867179219216945152, '带左侧树的主表', 'treemain', 'templatetype', 1, 852038408955416576, '2021-07-21 07:00:38', NULL, '10001');
INSERT INTO `mscode_sys_dict` VALUES (867179396682141696, '带左侧树的子表', 'treesub', 'templatetype', 2, 852038408955416576, '2021-07-21 07:01:20', NULL, '10001');

-- ----------------------------
-- Table structure for mscode_sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `mscode_sys_menu`;
CREATE TABLE `mscode_sys_menu`  (
  `id` bigint(20) NOT NULL COMMENT '菜单主键ID',
  `menu_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单编码',
  `menu_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单名称',
  `menu_icon` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
  `menu_path` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单路由',
  `menu_component` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单组件',
  `menu_sequence` bigint(20) NULL DEFAULT NULL COMMENT '排序',
  `menu_status` tinyint(4) NOT NULL COMMENT '菜单状态 0：不隐藏 1：隐藏',
  `parent_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '上级菜单ID',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `tenant_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '10001' COMMENT '租户编码',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_menu_route`(`menu_path`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mscode_sys_menu
-- ----------------------------
INSERT INTO `mscode_sys_menu` VALUES (663750580350930944, 'home', '首页', 'home', '/home', NULL, 1, 1, 0, '2020-01-06 22:27:52', '2020-06-29 09:16:39', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663854683370475520, 'workbench', '工作台', 'profile', '/home/workbench', './Home/Workbench', 1, 1, 663750580350930944, '2020-01-07 05:21:33', '2020-06-29 09:18:00', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663854974853632000, 'mynotification', '我的消息', 'message', '/home/mynotification', './Home/MyNotification', 2, 1, 663750580350930944, '2020-01-07 05:22:42', '2020-06-29 09:40:43', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663855492212641792, 'admin', '系统管理', 'desktop', '/admin', NULL, 3, 1, 0, '2020-01-07 05:24:45', '2020-06-29 09:17:56', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663855709905408000, 'sysuser', '用户管理', 'user', '/admin/sysuser', './Admin/SysUser', 1, 1, 663855492212641792, '2020-01-07 05:25:37', '2020-06-29 09:17:53', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663855944106954752, 'sysrole', '角色管理', 'idcard', '/authority/sysrole', './Admin/SysRole', 1, 1, 665496626324230144, '2020-01-07 05:26:33', '2020-06-29 09:17:51', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663856139884482560, 'sysparam', '参数管理', 'sliders', '/admin/sysparam', './Admin/SysParam', 7, 1, 663855492212641792, '2020-01-07 05:27:20', '2021-04-18 15:52:55', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663856370193715200, 'sysdict', '字典管理', 'book', '/admin/sysdict', './Admin/SysDict', 5, 1, 663855492212641792, '2020-01-07 05:28:15', '2021-04-18 15:52:25', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663856596409307136, 'sysorg', '组织机构', 'team', '/admin/sysorg', './Admin/SysOrg', 4, 1, 663855492212641792, '2020-01-07 05:29:09', '2021-06-18 18:25:55', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663856793264771072, 'sysmenu', '菜单管理', 'menu', '/admin/sysmenu', './Admin/SysMenu', 3, 1, 663855492212641792, '2020-01-07 05:29:56', '2021-04-18 15:51:53', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663857014669496320, 'sysregion', '区域管理', 'environment', '/admin/sysregion', './Admin/SysRegion', 6, 1, 663855492212641792, '2020-01-07 05:30:48', '2021-04-18 15:52:44', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663857253375725568, 'notification', '消息通知', 'notification', '/admin/notification', './Admin/Notification', 8, 1, 663855492212641792, '2020-01-07 05:31:45', '2021-04-18 15:53:07', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663861404352565248, 'tenant', '多租户', 'cloud-server', '/tenant', NULL, 4, 1, 0, '2020-01-07 05:48:15', '2020-06-29 09:17:37', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663861613581225984, 'systenant', '租户管理', 'cloud', '/tenant/systenant', './Admin/SysTenant', 1, 1, 663861404352565248, '2020-01-07 05:49:05', '2020-06-29 09:17:35', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663862096987344896, 'personaloa', '我的办公', 'schedule', '/personaloa', NULL, 5, 1, 0, '2020-01-07 05:51:00', '2020-06-29 09:17:33', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663862460172128256, 'startprocess', '发起流程', 'interaction', '/personaloa/startprocess', './PersonalOA/StartProcess', 1, 1, 663862096987344896, '2020-01-07 05:52:27', '2020-06-29 09:17:32', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663862916743090176, 'myprocess', '我的流程', 'calendar', '/personaloa/myprocess', './PersonalOA/MyProcess', 2, 1, 663862096987344896, '2020-01-07 05:54:15', '2020-06-29 09:17:30', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663863146888744960, 'candidatetask', '待签收任务', 'container', '/personaloa/candidatetask', './PersonalOA/CandidateTask', 3, 1, 663862096987344896, '2020-01-07 05:55:10', '2020-06-29 09:17:28', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663863400497336320, 'assigneetask', '待办任务', 'switcher', '/personaloa/assigneetask', './PersonalOA/AssigneeTask', 4, 1, 663862096987344896, '2020-01-07 05:56:11', '2020-06-29 09:17:27', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663863700486541312, 'finishedtask', '已办任务', 'project', '/personaloa/finishedtask', './PersonalOA/FinishedTask', 5, 1, 663862096987344896, '2020-01-07 05:57:22', '2020-06-29 09:17:25', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663864025209556992, 'activiti', '工作流管理', 'appstore', '/activiti', NULL, 6, 1, 0, '2020-01-07 05:58:40', '2020-06-29 09:17:22', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663864263244697600, 'model', '模型管理', 'hdd', '/activiti/model', './Activiti/ModelManage', 1, 1, 663864025209556992, '2020-01-07 05:59:37', '2020-06-29 09:17:43', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663864697992695808, 'processdesign', '流程设计', NULL, '/activiti/processdesign', './Activiti/ProcessDesign/ProcessDesign', 2, 0, 663864025209556992, '2020-01-07 06:01:20', '2020-06-29 09:15:20', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663864915588993024, 'deployment', '部署管理', 'deployment-unit', '/activiti/deployment', './Activiti/Deployment', 3, 1, 663864025209556992, '2020-01-07 06:02:12', '2020-06-29 09:18:43', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663865201925738496, 'processdefinition', '流程定义', 'control', '/activiti/processdefinition', './Activiti/ProcessDefinition', 4, 1, 663864025209556992, '2020-01-07 06:03:20', '2020-06-29 09:18:39', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663865503307452416, 'processinstance', '流程实例', 'windows', '/activiti/processinstance', './Activiti/ProcessInstance', 5, 1, 663864025209556992, '2020-01-07 06:04:32', '2020-06-29 09:18:38', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663865799052021760, 'execution', '流程实例执行路径', NULL, '/activiti/execution', './Activiti/Execution', 6, 0, 663864025209556992, '2020-01-07 06:05:43', '2020-06-29 09:15:53', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663866117546496000, 'task', '流程实例任务', 'solution', '/activiti/task', './Activiti/Task', 7, 1, 663864025209556992, '2020-01-07 06:06:59', '2020-06-29 09:18:36', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663866457184456704, 'historicprocessinstance', '流程发起历史', 'tablet', '/activiti/historicprocessinstance', './Activiti/HistoricProcessInstance', 8, 1, 663864025209556992, '2020-01-07 06:08:20', '2020-06-29 09:18:34', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663866797015355392, 'historicactivityinstance', '流程节点执行历史', NULL, '/activiti/historicactivityinstance', './Activiti/HistoricActivityInstance', 9, 0, 663864025209556992, '2020-01-07 06:09:41', '2020-06-29 09:15:48', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663867140080062464, 'historicdetail', '流程节点详细历史', NULL, '/activiti/historicdetail', './Activiti/HistoricDetail', 10, 0, 663864025209556992, '2020-01-07 06:11:02', '2020-06-29 09:15:28', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663867371509174272, 'historicvariableinstance', '流程实例变量历史', NULL, '/activiti/historicvariableinstance', './Activiti/HistoricVariableInstance', 11, 0, 663864025209556992, '2020-01-07 06:11:58', '2020-06-29 09:15:23', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663867636220088320, 'historictaskinstance', '流程任务历史', 'carry-out', '/activiti/historictaskinstance', './Activiti/HistoricTaskInstance', 12, 1, 663864025209556992, '2020-01-07 06:13:01', '2020-06-29 09:18:32', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663867981964955648, 'file', '文件管理', 'file', '/file', NULL, 7, 1, 0, '2020-01-07 06:14:23', '2020-06-29 09:18:31', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663868238819938304, 'uploaddownload', '上传下载', 'file-text', '/file/upload-download', './File/File', 1, 1, 663867981964955648, '2020-01-07 06:15:24', '2020-06-29 09:18:26', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663868640676204544, 'chart', '图表报表', 'dashboard', '/chart', NULL, 8, 1, 0, '2020-01-07 06:17:00', '2020-06-29 09:18:24', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663868873103560704, 'analysis', '分析页', 'bar-chart', '/chart/analysis', './Chart/Analysis', 1, 1, 663868640676204544, '2020-01-07 06:17:56', '2020-06-29 09:18:02', '10001');
INSERT INTO `mscode_sys_menu` VALUES (663869085146599424, 'monitor', '监控页', 'area-chart', '/chart/monitor', './Chart/Monitor', 2, 1, 663868640676204544, '2020-01-07 06:18:46', '2020-06-29 09:18:21', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665003871265280000, 'template', '模板实例', 'project', '/template', NULL, 14, 1, 0, '2020-01-10 09:28:00', '2020-06-29 09:18:19', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665004465291972608, 'form', '表单页', 'form', '/template/form', NULL, 1, 1, 665003871265280000, '2020-01-10 09:30:21', '2020-06-29 09:18:17', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665004888883122176, 'basicform', '基础表单', 'form', '/template/form/basic-form', './Template/Forms/BasicForm', 1, 1, 665004465291972608, '2020-01-10 09:32:02', '2020-06-29 09:18:15', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665005583355006976, 'stepform', '分步表单', 'form', '/template/form/step-form?current=1', './Template/Forms/StepForm', 2, 1, 665004465291972608, '2020-01-10 09:34:48', '2020-06-29 09:18:13', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665006035807162368, 'info', '填写转账信息', 'form', '/template/form/step-form', './Template/Forms/StepForm/Step1', 1, 1, 665005583355006976, '2020-01-10 09:36:36', '2020-06-29 09:18:09', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665006303672193024, 'confirm', '确认转账信息', NULL, '/template/form/step-form/confirm', './Template/Forms/StepForm/Step2', 2, 0, 665005583355006976, '2020-01-10 09:37:40', '2020-06-29 09:15:32', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665006482060136448, 'finish', '完成', NULL, '/template/form/step-form/finish', './Template/Forms/StepForm/Step3', 3, 0, 665005583355006976, '2020-01-10 09:38:22', '2020-06-29 09:15:35', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665006779776028672, 'advancedform', '高级表单', 'form', '/template/form/advanced-form', './Template/Forms/AdvancedForm', 3, 1, 665004465291972608, '2020-01-10 09:39:33', '2020-06-29 09:18:07', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665007146580496384, 'list', '列表页', 'table', '/template/list', NULL, 2, 1, 665003871265280000, '2020-01-10 09:41:01', '2020-06-29 09:18:05', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665007437736497152, 'searchtable', '查询表格', 'table', '/template/list/table-list', './Template/List/TableList', 1, 1, 665007146580496384, '2020-01-10 09:42:10', '2020-06-29 09:18:03', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665007802225709056, 'basiclist', '标准列表', 'table', '/template/list/basic-list', './Template/List/BasicList', 2, 1, 665007146580496384, '2020-01-10 09:43:37', '2020-06-29 09:18:23', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665009153424936960, 'cardlist', '卡片列表', 'table', '/template/list/card-list', './Template/List/CardList', 3, 1, 665007146580496384, '2020-01-10 09:48:59', '2020-06-29 09:16:31', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665009726589161472, 'searchlist', '搜索列表', 'table', '/template/list/search', './Template/List/List', 3, 1, 665007146580496384, '2020-01-10 09:51:16', '2020-06-29 09:16:29', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665010520889675776, 'articles', '搜索列表（文章）', 'table', '/template/list/search/articles', './Template/List/Articles', 1, 1, 665009726589161472, '2020-01-10 09:54:25', '2020-06-29 09:16:26', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665010657795952640, 'projects', '搜索列表（项目）', 'table', '/template/list/search/projects', './Template/List/Projects', 2, 1, 665009726589161472, '2020-01-10 09:54:58', '2020-06-29 09:16:23', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665010791703302144, 'applications', '搜索列表（应用）', 'table', '/template/list/search/applications', './Template/List/Applications', 3, 1, 665009726589161472, '2020-01-10 09:55:30', '2020-06-29 09:16:21', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665191788055023616, 'profile', '详情页', 'profile', '/template/profile', NULL, 3, 1, 665003871265280000, '2020-01-10 21:54:40', '2020-06-29 09:16:18', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665192197972742144, 'basic', '基础详情页', 'profile', '/template/profile/basic', './Template/Profile/BasicProfile', 1, 1, 665191788055023616, '2020-01-10 21:56:18', '2020-06-29 09:15:56', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665192716074143744, 'advanced', '高级详情页', 'profile', '/template/profile/advanced', './Template/Profile/AdvancedProfile', 2, 1, 665191788055023616, '2020-01-10 21:58:22', '2020-06-29 09:16:11', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665197219141832704, 'result', '结果页', 'check-circle-o', '/template/result', NULL, 4, 1, 665003871265280000, '2020-01-10 22:16:15', '2020-06-29 09:16:05', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665197381171990528, 'success', '成功页', 'check-circle-o', '/template/result/success', './Template/Result/Success', 1, 1, 665197219141832704, '2020-01-10 22:16:54', '2020-06-29 09:16:01', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665197527700000768, 'fail', '失败页', 'close-circle-o', '/template/result/fail', './Template/Result/Error', 2, 1, 665197219141832704, '2020-01-10 22:17:29', '2020-06-29 09:15:59', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665197875319721984, 'exception', '异常页', 'warning', '/exception', NULL, 12, 0, 0, '2020-01-10 22:18:52', '2020-06-29 09:15:04', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665198079427137536, 'not-permission', '403', 'warning', '/exception/403', './Exception/Exception403', 1, 0, 665197875319721984, '2020-01-10 22:19:40', '2020-06-29 09:15:12', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665198241801228288, 'not-find', '404', 'warning', '/exception/404', './Exception/Exception404', 2, 0, 665197875319721984, '2020-01-10 22:20:19', '2020-06-29 09:15:15', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665198396457799680, 'server-error', '500', 'warning', '/exception/500', './Exception/Exception500', 3, 0, 665197875319721984, '2020-01-10 22:20:56', '2020-06-29 09:14:56', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665198983094128640, 'account', '个人页', 'user', '/template/account', NULL, 6, 1, 665003871265280000, '2020-01-10 22:23:16', '2020-06-29 09:16:56', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665199223704571904, 'center', '个人中心', 'user', '/template/account/center', './Template/Account/Center/Center', 1, 1, 665198983094128640, '2020-01-10 22:24:13', '2020-06-29 09:17:16', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665199423219224576, 'editor', '图形编辑器', 'highlight', '/template/editor', NULL, 7, 1, 665003871265280000, '2020-01-10 22:25:01', '2020-06-29 09:16:16', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665199632426913792, 'flow', '流程编辑器', 'highlight', '/template/editor/flow', './Template/Editor/GGEditor/Flow', 1, 1, 665199423219224576, '2020-01-10 22:25:51', '2020-06-29 09:17:13', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665199774022422528, 'mind', '脑图编辑器', 'highlight', '/template/editor/mind', './Template/Editor/GGEditor/Mind', 2, 1, 665199423219224576, '2020-01-10 22:26:24', '2020-06-29 09:17:11', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665199936786583552, 'koni', '拓扑编辑器', 'highlight', '/template/editor/koni', './Template/Editor/GGEditor/Koni', 3, 1, 665199423219224576, '2020-01-10 22:27:03', '2020-06-29 09:17:09', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665496626324230144, 'authority', '权限管理', 'safety', '/authority', NULL, 2, 1, 0, '2020-01-11 18:06:00', '2020-06-29 09:17:07', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665564219978469376, 'monitors', '配置监控', 'line-chart', '/monitors', NULL, 11, 1, 0, '2020-01-11 22:34:36', '2020-06-29 09:17:06', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665564620060545024, 'sentinel', 'Sentinel流量监控', 'sliders', 'https://www.mscodecloud.com/sentinel/', NULL, 1, 1, 665564219978469376, '2020-01-11 22:36:11', '2020-10-02 17:22:41', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665730162285268992, 'nacos', 'Nacos配置注册', 'branches', 'https://www.mscodecloud.com/nacos/', NULL, 2, 1, 665564219978469376, '2020-01-12 09:34:02', '2020-06-29 09:17:00', '10001');
INSERT INTO `mscode_sys_menu` VALUES (665734849524846592, 'swagger', 'Swagger接口文档', 'box-plot', 'https://www.mscodecloud.com/swagger', NULL, 3, 1, 665564219978469376, '2020-01-12 09:52:39', '2020-10-01 21:55:27', '10001');
INSERT INTO `mscode_sys_menu` VALUES (666473278365683712, 'devtool', '研发工具', 'code-sandbox', '/devtool', NULL, 9, 1, 0, '2020-01-14 10:46:53', '2020-06-29 09:16:54', '10001');
INSERT INTO `mscode_sys_menu` VALUES (666473881804394496, 'codegenerator', '代码生成器', 'code', '/devtool/codegenerator', './DevTool/CodeGenerator', 1, 1, 666473278365683712, '2020-01-14 10:49:17', '2021-06-09 10:47:35', '10001');
INSERT INTO `mscode_sys_menu` VALUES (684185040317763584, 'sysurl', '接口权限', 'api', '/authority/sysurl', './Admin/SysUrl', 2, 1, 665496626324230144, '2020-03-03 07:47:08', '2020-06-29 09:16:51', '10001');
INSERT INTO `mscode_sys_menu` VALUES (687514791186976768, 'account', '个人页', 'setting', '/account', '', 13, 0, 0, '2020-03-12 12:18:19', '2020-06-29 09:15:25', '10001');
INSERT INTO `mscode_sys_menu` VALUES (687563817764704256, 'settings', '个人设置', 'user', '/account/settings', './Account/Settings/Info', 1, 1, 687514791186976768, '2020-03-12 15:33:08', '2020-06-29 09:16:48', '10001');
INSERT INTO `mscode_sys_menu` VALUES (690019670816116736, 'datapermission', '数据权限', 'gateway', '/authority/datapermission', './Admin/DataPermission', 3, 1, 665496626324230144, '2020-03-19 10:11:52', '2020-06-29 09:16:47', '10001');
INSERT INTO `mscode_sys_menu` VALUES (692361087815176192, 'groupchart', '分组报表', 'stock', '/chart/groupchart', './Chart/GroupChart', 3, 1, 663868640676204544, '2020-03-25 21:15:54', '2020-06-29 09:16:45', '10001');
INSERT INTO `mscode_sys_menu` VALUES (693434079391436800, 'myprocessdetails', '我的流程详情', NULL, '/personaloa/myprocessdetails', './PersonalOA/MyProcessDetails', 6, 0, 663862096987344896, '2020-03-28 20:19:27', '2020-06-29 09:15:38', '10001');
INSERT INTO `mscode_sys_menu` VALUES (693698880994725888, 'mytaskdetails', '我的任务详情', NULL, '/personaloa/mytaskdetails', './PersonalOA/MyTaskDetails', 7, 0, 663862096987344896, '2020-03-29 13:51:43', '2020-06-29 09:15:41', '10001');
INSERT INTO `mscode_sys_menu` VALUES (693762431952277504, 'historictaskdetails', '任务历史详情', NULL, '/activiti/historictaskdetails', './Activiti/HistoricTaskDetails', 14, 0, 663864025209556992, '2020-03-29 18:04:15', '2020-06-29 09:15:43', '10001');
INSERT INTO `mscode_sys_menu` VALUES (693762978637860864, 'taskdetails', '任务详情', NULL, '/activiti/taskdetails', './Activiti/TaskDetails', 13, 0, 663864025209556992, '2020-03-29 18:06:25', '2020-06-29 09:15:46', '10001');
INSERT INTO `mscode_sys_menu` VALUES (699081036910743552, 'appclient', '应用管理', 'fork', '/admin/appclient', './Admin/AppClient', 9, 1, 663855492212641792, '2020-04-13 10:18:27', '2021-04-18 15:53:20', '10001');
INSERT INTO `mscode_sys_menu` VALUES (705593293912068096, 'distributedlog', '分布式日志', 'cluster', '/distributedlog', NULL, 10, 1, 0, '2020-05-01 09:35:53', '2020-06-29 09:16:41', '10001');
INSERT INTO `mscode_sys_menu` VALUES (705595098737856512, 'kibana', 'ELK分布式日志', 'pull-request', 'https://www.mscodecloud.com/kibana/app/kibana#/discover?_g=(refreshInterval:(pause:!t,value:0),time:(from:now-150m,to:now))&_a=(columns:!(_source),index:\'71a76aa0-f071-11eb-a38f-8b27d9c2c453\',interval:auto,query:(language:kuery,query:\'\'),sort:!(!(\'@timestamp\',desc)))', NULL, 1, 1, 705593293912068096, '2020-05-01 09:43:03', '2021-07-29 21:34:51', '10001');
INSERT INTO `mscode_sys_menu` VALUES (765381334624817152, 'formgenerator', '表单设计器', 'codepen', '/devtool/formgenerator', './DevTool/FormGenerator', 2, 1, 666473278365683712, '2020-10-13 09:12:11', '2020-10-23 10:35:56', '10001');
INSERT INTO `mscode_sys_menu` VALUES (820207211942170624, 'onlinesysuser', '在线用户', 'el-icon-chat-line-round', '/admin/onlinesysuser', NULL, 10, 1, 663855492212641792, '2021-03-13 16:10:41', '2021-04-18 15:53:30', '10001');
INSERT INTO `mscode_sys_menu` VALUES (833249453736906752, 'syspost', '岗位管理', 'el-icon-bank-card', '/admin/syspost', './Admin/SysPost', 2, 1, 663855492212641792, '2021-04-18 15:55:50', NULL, '10001');
INSERT INTO `mscode_sys_menu` VALUES (852017180513325056, 'generatortemplate', '模板管理', 'code', '/devtool/codegenerator/generatortemplate', NULL, 1, 1, 666473881804394496, '2021-06-09 10:52:07', NULL, '10001');
INSERT INTO `mscode_sys_menu` VALUES (852017515856318464, 'forwardgenerator', '正向生成', 'code', '/devtool/codegenerator/forwardgenerator', NULL, 2, 1, 666473881804394496, '2021-06-09 10:53:27', '2021-06-09 10:53:36', '10001');
INSERT INTO `mscode_sys_menu` VALUES (852017916051640320, 'reversegenerator', '反向生成', 'code', '/devtool/codegenerator/reversegenerator', NULL, 3, 1, 666473881804394496, '2021-06-09 10:55:03', NULL, '10001');
INSERT INTO `mscode_sys_menu` VALUES (871025442801766400, 'formdesign', '表单设计', 'form', '/devtool/formgenerator/formdesign', NULL, 1, 1, 765381334624817152, '2021-07-31 21:44:09', NULL, '10001');
INSERT INTO `mscode_sys_menu` VALUES (871025918976905216, 'generatorform', '表单管理', 'skill', '/devtool/formgenerator/generatorform', NULL, 2, 1, 765381334624817152, '2021-07-31 21:46:02', NULL, '10001');
INSERT INTO `mscode_sys_menu` VALUES (871704733705228288, 'druid', 'Druid连接池监控', 'database', 'https://www.mscodecloud.com/druid', NULL, 4, 1, 665564219978469376, '2021-08-02 18:43:25', '2021-08-02 19:55:30', '10001');

-- ----------------------------
-- Table structure for mscode_sys_menu_button
-- ----------------------------
DROP TABLE IF EXISTS `mscode_sys_menu_button`;
CREATE TABLE `mscode_sys_menu_button`  (
  `id` bigint(20) NOT NULL COMMENT '菜单与按钮关联主键ID',
  `menu_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单编码',
  `menu_button` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '按钮',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单与按钮关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mscode_sys_menu_button
-- ----------------------------
INSERT INTO `mscode_sys_menu_button` VALUES (690886417148465152, 'sysurl', 'add');
INSERT INTO `mscode_sys_menu_button` VALUES (690886417240739840, 'sysurl', 'update');
INSERT INTO `mscode_sys_menu_button` VALUES (690886417341403136, 'sysurl', 'delete');
INSERT INTO `mscode_sys_menu_button` VALUES (690886417442066432, 'sysurl', 'export');
INSERT INTO `mscode_sys_menu_button` VALUES (690886417559506944, 'sysurl', 'authorize');
INSERT INTO `mscode_sys_menu_button` VALUES (690886572505485312, 'datapermission', 'authorize');
INSERT INTO `mscode_sys_menu_button` VALUES (690886745692491776, 'sysuser', 'add');
INSERT INTO `mscode_sys_menu_button` VALUES (690886745797349376, 'sysuser', 'update');
INSERT INTO `mscode_sys_menu_button` VALUES (690886745927372800, 'sysuser', 'delete');
INSERT INTO `mscode_sys_menu_button` VALUES (690886746032230400, 'sysuser', 'export');
INSERT INTO `mscode_sys_menu_button` VALUES (690887942629412864, 'systenant', 'add');
INSERT INTO `mscode_sys_menu_button` VALUES (690887942730076160, 'systenant', 'update');
INSERT INTO `mscode_sys_menu_button` VALUES (690887942839128064, 'systenant', 'delete');
INSERT INTO `mscode_sys_menu_button` VALUES (690887942939791360, 'systenant', 'export');
INSERT INTO `mscode_sys_menu_button` VALUES (690888369387261952, 'startprocess', 'preview');
INSERT INTO `mscode_sys_menu_button` VALUES (690888369483730944, 'startprocess', 'start');
INSERT INTO `mscode_sys_menu_button` VALUES (690889637128228864, 'model', 'add');
INSERT INTO `mscode_sys_menu_button` VALUES (690889637233086464, 'model', 'update');
INSERT INTO `mscode_sys_menu_button` VALUES (690889637346332672, 'model', 'delete');
INSERT INTO `mscode_sys_menu_button` VALUES (690889637451190272, 'model', 'design');
INSERT INTO `mscode_sys_menu_button` VALUES (690889637551853568, 'model', 'deploy');
INSERT INTO `mscode_sys_menu_button` VALUES (690889807416971264, 'deployment', 'delete');
INSERT INTO `mscode_sys_menu_button` VALUES (690889807521828864, 'deployment', 'download');
INSERT INTO `mscode_sys_menu_button` VALUES (690889807626686464, 'deployment', 'preview');
INSERT INTO `mscode_sys_menu_button` VALUES (690890090238889984, 'processdefinition', 'download');
INSERT INTO `mscode_sys_menu_button` VALUES (690890090343747584, 'processdefinition', 'preview');
INSERT INTO `mscode_sys_menu_button` VALUES (690890090465382400, 'processdefinition', 'suspend');
INSERT INTO `mscode_sys_menu_button` VALUES (690890090574434304, 'processdefinition', 'activate');
INSERT INTO `mscode_sys_menu_button` VALUES (690890400390893568, 'processinstance', 'delete');
INSERT INTO `mscode_sys_menu_button` VALUES (690890400499945472, 'processinstance', 'track');
INSERT INTO `mscode_sys_menu_button` VALUES (690890400608997376, 'processinstance', 'suspend');
INSERT INTO `mscode_sys_menu_button` VALUES (690890400718049280, 'processinstance', 'activate');
INSERT INTO `mscode_sys_menu_button` VALUES (690890400818712576, 'processinstance', 'execution');
INSERT INTO `mscode_sys_menu_button` VALUES (690890958535315456, 'historicprocessinstance', 'delete');
INSERT INTO `mscode_sys_menu_button` VALUES (690890958635978752, 'historicprocessinstance', 'activity');
INSERT INTO `mscode_sys_menu_button` VALUES (690890958757613568, 'historicprocessinstance', 'detail');
INSERT INTO `mscode_sys_menu_button` VALUES (690890958862471168, 'historicprocessinstance', 'variable');
INSERT INTO `mscode_sys_menu_button` VALUES (690891368851492864, 'uploaddownload', 'delete');
INSERT INTO `mscode_sys_menu_button` VALUES (690891368952156160, 'uploaddownload', 'upload');
INSERT INTO `mscode_sys_menu_button` VALUES (690891369061208064, 'uploaddownload', 'download');
INSERT INTO `mscode_sys_menu_button` VALUES (693344019832819712, 'generator', 'add');
INSERT INTO `mscode_sys_menu_button` VALUES (693344019958648832, 'generator', 'update');
INSERT INTO `mscode_sys_menu_button` VALUES (693344020092866560, 'generator', 'delete');
INSERT INTO `mscode_sys_menu_button` VALUES (693344020206112768, 'generator', 'export');
INSERT INTO `mscode_sys_menu_button` VALUES (693344020336136192, 'generator', 'generate');
INSERT INTO `mscode_sys_menu_button` VALUES (693790288543928320, 'historictaskinstance', 'delete');
INSERT INTO `mscode_sys_menu_button` VALUES (693790288632008704, 'historictaskinstance', 'details');
INSERT INTO `mscode_sys_menu_button` VALUES (693790517292879872, 'task', 'track');
INSERT INTO `mscode_sys_menu_button` VALUES (693790517385154560, 'task', 'execution');
INSERT INTO `mscode_sys_menu_button` VALUES (693790517464846336, 'task', 'details');
INSERT INTO `mscode_sys_menu_button` VALUES (693791070404136960, 'myprocess', 'track');
INSERT INTO `mscode_sys_menu_button` VALUES (693791070496411648, 'myprocess', 'details');
INSERT INTO `mscode_sys_menu_button` VALUES (693791130684674048, 'candidatetask', 'track');
INSERT INTO `mscode_sys_menu_button` VALUES (693791130781143040, 'candidatetask', 'assign');
INSERT INTO `mscode_sys_menu_button` VALUES (693791130898583552, 'candidatetask', 'details');
INSERT INTO `mscode_sys_menu_button` VALUES (693791160086745088, 'assigneetask', 'track');
INSERT INTO `mscode_sys_menu_button` VALUES (693791160174825472, 'assigneetask', 'complete');
INSERT INTO `mscode_sys_menu_button` VALUES (693791160271294464, 'assigneetask', 'delegate');
INSERT INTO `mscode_sys_menu_button` VALUES (693791160413900800, 'assigneetask', 'regress');
INSERT INTO `mscode_sys_menu_button` VALUES (693791160518758400, 'assigneetask', 'reject');
INSERT INTO `mscode_sys_menu_button` VALUES (693791160606838784, 'assigneetask', 'terminate');
INSERT INTO `mscode_sys_menu_button` VALUES (693791160711696384, 'assigneetask', 'details');
INSERT INTO `mscode_sys_menu_button` VALUES (693791186443751424, 'finishedtask', 'track');
INSERT INTO `mscode_sys_menu_button` VALUES (693791186577969152, 'finishedtask', 'details');
INSERT INTO `mscode_sys_menu_button` VALUES (726975396599746560, 'mynotification', 'view');
INSERT INTO `mscode_sys_menu_button` VALUES (833248459753967616, 'sysmenu', 'add');
INSERT INTO `mscode_sys_menu_button` VALUES (833248459879796736, 'sysmenu', 'update');
INSERT INTO `mscode_sys_menu_button` VALUES (833248460009820160, 'sysmenu', 'delete');
INSERT INTO `mscode_sys_menu_button` VALUES (833248460131454976, 'sysmenu', 'export');
INSERT INTO `mscode_sys_menu_button` VALUES (833248535347908608, 'sysorg', 'add');
INSERT INTO `mscode_sys_menu_button` VALUES (833248535465349120, 'sysorg', 'update');
INSERT INTO `mscode_sys_menu_button` VALUES (833248535561818112, 'sysorg', 'delete');
INSERT INTO `mscode_sys_menu_button` VALUES (833248535654092800, 'sysorg', 'export');
INSERT INTO `mscode_sys_menu_button` VALUES (833248592793096192, 'sysdict', 'add');
INSERT INTO `mscode_sys_menu_button` VALUES (833248592927313920, 'sysdict', 'update');
INSERT INTO `mscode_sys_menu_button` VALUES (833248594072358912, 'sysdict', 'delete');
INSERT INTO `mscode_sys_menu_button` VALUES (833248594198188032, 'sysdict', 'export');
INSERT INTO `mscode_sys_menu_button` VALUES (833248671448879104, 'sysregion', 'add');
INSERT INTO `mscode_sys_menu_button` VALUES (833248671633428480, 'sysregion', 'update');
INSERT INTO `mscode_sys_menu_button` VALUES (833248671713120256, 'sysregion', 'delete');
INSERT INTO `mscode_sys_menu_button` VALUES (833248671805394944, 'sysregion', 'export');
INSERT INTO `mscode_sys_menu_button` VALUES (833248719997947904, 'sysparam', 'add');
INSERT INTO `mscode_sys_menu_button` VALUES (833248720325103616, 'sysparam', 'update');
INSERT INTO `mscode_sys_menu_button` VALUES (833248720618704896, 'sysparam', 'delete');
INSERT INTO `mscode_sys_menu_button` VALUES (833248720941666304, 'sysparam', 'export');
INSERT INTO `mscode_sys_menu_button` VALUES (833248770174406656, 'notification', 'add');
INSERT INTO `mscode_sys_menu_button` VALUES (833248770279264256, 'notification', 'update');
INSERT INTO `mscode_sys_menu_button` VALUES (833248770405093376, 'notification', 'delete');
INSERT INTO `mscode_sys_menu_button` VALUES (833248821479133184, 'appclient', 'add');
INSERT INTO `mscode_sys_menu_button` VALUES (833248821571407872, 'appclient', 'update');
INSERT INTO `mscode_sys_menu_button` VALUES (833248821672071168, 'appclient', 'delete');
INSERT INTO `mscode_sys_menu_button` VALUES (833248821764345856, 'appclient', 'export');
INSERT INTO `mscode_sys_menu_button` VALUES (833249453833375744, 'syspost', 'add');
INSERT INTO `mscode_sys_menu_button` VALUES (833249453934039040, 'syspost', 'update');
INSERT INTO `mscode_sys_menu_button` VALUES (833249454022119424, 'syspost', 'delete');
INSERT INTO `mscode_sys_menu_button` VALUES (833249454135365632, 'syspost', 'export');
INSERT INTO `mscode_sys_menu_button` VALUES (852017180634959872, 'generatortemplate', 'add');
INSERT INTO `mscode_sys_menu_button` VALUES (852017180744011776, 'generatortemplate', 'update');
INSERT INTO `mscode_sys_menu_button` VALUES (852017180857257984, 'generatortemplate', 'delete');
INSERT INTO `mscode_sys_menu_button` VALUES (852017181025030144, 'generatortemplate', 'export');
INSERT INTO `mscode_sys_menu_button` VALUES (852017551306575872, 'forwardgenerator', 'add');
INSERT INTO `mscode_sys_menu_button` VALUES (852017551394656256, 'forwardgenerator', 'update');
INSERT INTO `mscode_sys_menu_button` VALUES (852017551499513856, 'forwardgenerator', 'delete');
INSERT INTO `mscode_sys_menu_button` VALUES (852017551583399936, 'forwardgenerator', 'generate');
INSERT INTO `mscode_sys_menu_button` VALUES (852017551671480320, 'forwardgenerator', 'export');
INSERT INTO `mscode_sys_menu_button` VALUES (852017916441710592, 'reversegenerator', 'generate');
INSERT INTO `mscode_sys_menu_button` VALUES (864680803607306240, 'sysrole', 'add');
INSERT INTO `mscode_sys_menu_button` VALUES (864680803615694848, 'sysrole', 'update');
INSERT INTO `mscode_sys_menu_button` VALUES (864680803619889152, 'sysrole', 'delete');
INSERT INTO `mscode_sys_menu_button` VALUES (864680803624083456, 'sysrole', 'export');
INSERT INTO `mscode_sys_menu_button` VALUES (864680803628277760, 'sysrole', 'authorize');
INSERT INTO `mscode_sys_menu_button` VALUES (864680803628277761, 'sysrole', 'upload');
INSERT INTO `mscode_sys_menu_button` VALUES (871025442965344256, 'formdesign', 'download');
INSERT INTO `mscode_sys_menu_button` VALUES (871025919119511552, 'generatorform', 'update');
INSERT INTO `mscode_sys_menu_button` VALUES (871025919224369152, 'generatorform', 'delete');
INSERT INTO `mscode_sys_menu_button` VALUES (871025919329226752, 'generatorform', 'export');

-- ----------------------------
-- Table structure for mscode_sys_org
-- ----------------------------
DROP TABLE IF EXISTS `mscode_sys_org`;
CREATE TABLE `mscode_sys_org`  (
  `id` bigint(20) NOT NULL COMMENT '机构主键ID',
  `org_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '机构名称',
  `org_type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '机构类型',
  `org_description` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '机构描述',
  `org_sequence` bigint(20) NULL DEFAULT NULL COMMENT '排序',
  `parent_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '上级机构ID',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `tenant_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '10001' COMMENT '租户编码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '机构表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mscode_sys_org
-- ----------------------------
INSERT INTO `mscode_sys_org` VALUES (1, '国星科技', 'company', '广东国星科技有限公司', 1, 0, '2019-08-27 15:29:11', '2021-04-18 17:02:58', '10001');
INSERT INTO `mscode_sys_org` VALUES (2, '研发部', 'department', '这是研发部门', 1, 1, '2019-08-30 22:30:51', '2019-08-30 22:34:32', '10001');
INSERT INTO `mscode_sys_org` VALUES (666664834108411904, '开发三组', 'team', '开发三组研发框架', 1, 2, '2020-01-14 23:28:03', '2020-03-18 10:36:57', '10001');
INSERT INTO `mscode_sys_org` VALUES (666665278205513728, '行政部', 'department', '行政部门服务员工', 2, 1, '2020-01-14 23:29:49', NULL, '10001');
INSERT INTO `mscode_sys_org` VALUES (666858728934789120, '测试一组', 'team', '测试一组测试框架', 2, 2, '2020-01-15 12:18:32', NULL, '10001');
INSERT INTO `mscode_sys_org` VALUES (713570638321078272, '电信集团', 'group', '电信集团是专业的电信服务提供商', 2, 0, '2020-05-23 09:55:01', '2021-04-18 17:08:26', '10001');
INSERT INTO `mscode_sys_org` VALUES (785354103659614208, '电信广州公司', 'company', '这是电信集团的广州公司', 1, 713570638321078272, '2020-12-07 11:56:50', '2021-04-18 17:09:52', '10001');
INSERT INTO `mscode_sys_org` VALUES (833268259523842048, '研发部', 'department', '这是电信广州公司的研发部', 1, 785354103659614208, '2021-04-18 17:10:34', NULL, '10001');
INSERT INTO `mscode_sys_org` VALUES (833268606342451200, '研发一组', 'team', '这是研发部的研发一组', 1, 833268259523842048, '2021-04-18 17:11:57', NULL, '10001');

-- ----------------------------
-- Table structure for mscode_sys_param
-- ----------------------------
DROP TABLE IF EXISTS `mscode_sys_param`;
CREATE TABLE `mscode_sys_param`  (
  `id` bigint(20) NOT NULL COMMENT '参数主键ID',
  `param_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '参数名称',
  `param_key` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '参数键名',
  `param_value` varchar(2048) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '参数键值',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `tenant_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '10001' COMMENT '租户编码',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_param_name`(`param_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '参数表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mscode_sys_param
-- ----------------------------
INSERT INTO `mscode_sys_param` VALUES (1, '账户初始密码', 'account.initPassword', '123456', '2019-09-23 16:54:15', '2020-01-15 12:20:32', '10001');

-- ----------------------------
-- Table structure for mscode_sys_post
-- ----------------------------
DROP TABLE IF EXISTS `mscode_sys_post`;
CREATE TABLE `mscode_sys_post`  (
  `id` bigint(20) NOT NULL COMMENT '岗位主键ID',
  `post_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位名称',
  `post_sequence` bigint(20) NULL DEFAULT NULL COMMENT '岗位排序',
  `parent_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '上级岗位ID',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `tenant_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '10001' COMMENT '租户编码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '岗位表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mscode_sys_post
-- ----------------------------
INSERT INTO `mscode_sys_post` VALUES (833277616042987520, 'CTS', '国星科技公司部门岗位', 1, 0, '2021-04-18 17:47:45', NULL, '10001');
INSERT INTO `mscode_sys_post` VALUES (833278195871961088, 'CTS-OFFICE', '办公室', 1, 833277616042987520, '2021-04-18 17:50:03', NULL, '10001');
INSERT INTO `mscode_sys_post` VALUES (833278590266560512, 'CTS-OFFICE-01', '办公室主任', 1, 833278195871961088, '2021-04-18 17:51:37', NULL, '10001');
INSERT INTO `mscode_sys_post` VALUES (833278710273986560, 'CTS-OFFICE-02', '办公室副主任', 2, 833278195871961088, '2021-04-18 17:52:06', NULL, '10001');
INSERT INTO `mscode_sys_post` VALUES (833278992160575488, 'CTS-OFFICE-03', '办公室文秘岗', 3, 833278195871961088, '2021-04-18 17:53:13', NULL, '10001');
INSERT INTO `mscode_sys_post` VALUES (833279223824568320, 'CTS-OFFICE-04', '办公室文书印信岗', 4, 833278195871961088, '2021-04-18 17:54:08', NULL, '10001');
INSERT INTO `mscode_sys_post` VALUES (833279376849555456, 'CTS-OFFICE-05', '办公室企业管理岗', 5, 833278195871961088, '2021-04-18 17:54:45', NULL, '10001');
INSERT INTO `mscode_sys_post` VALUES (833280471881666560, 'CTS-PROJECT', '工程部', 2, 833277616042987520, '2021-04-18 17:59:06', NULL, '10001');
INSERT INTO `mscode_sys_post` VALUES (833280609723273216, 'CTS-PROJECT-01', '工程部经理', 1, 833280471881666560, '2021-04-18 17:59:39', NULL, '10001');
INSERT INTO `mscode_sys_post` VALUES (833280703373692928, 'CTS-PROJECT-02', '工程部副经理', 2, 833280471881666560, '2021-04-18 18:00:01', '2021-04-18 18:16:10', '10001');
INSERT INTO `mscode_sys_post` VALUES (833280992529010688, 'CTS-PROJECT-03', '工程部项目管理标准化岗', 3, 833280471881666560, '2021-04-18 18:01:10', NULL, '10001');
INSERT INTO `mscode_sys_post` VALUES (833281296662188032, 'CTS-PROJECT-04', '工程部计划与统计管理岗', 4, 833280471881666560, '2021-04-18 18:02:22', NULL, '10001');
INSERT INTO `mscode_sys_post` VALUES (833281691782402048, 'CTS-RD', '研发部', 3, 833277616042987520, '2021-04-18 18:03:57', NULL, '10001');
INSERT INTO `mscode_sys_post` VALUES (833281885571829760, 'CTS-RD-01', '研发部经理', 1, 833281691782402048, '2021-04-18 18:04:43', NULL, '10001');
INSERT INTO `mscode_sys_post` VALUES (833282018061504512, 'CTS-RD-02', '研发部副经理', 2, 833281691782402048, '2021-04-18 18:05:14', NULL, '10001');
INSERT INTO `mscode_sys_post` VALUES (833282277089136640, 'CTS-RD-03', '研发部科技成果管理岗', 3, 833281691782402048, '2021-04-18 18:06:16', NULL, '10001');
INSERT INTO `mscode_sys_post` VALUES (833282774260961280, 'CTS-RD-04', '研发部开发岗', 4, 833281691782402048, '2021-04-18 18:08:15', NULL, '10001');
INSERT INTO `mscode_sys_post` VALUES (833282883937816576, 'CTS-RD-05', '研发部测试岗', 5, 833281691782402048, '2021-04-18 18:08:41', NULL, '10001');
INSERT INTO `mscode_sys_post` VALUES (833283127295528960, 'CTS-RD-06', '研发部架构设计岗', 6, 833281691782402048, '2021-04-18 18:09:39', NULL, '10001');
INSERT INTO `mscode_sys_post` VALUES (833283411786780672, 'CTS-RD-07', '研发部技术标准管理岗', 7, 833281691782402048, '2021-04-18 18:10:47', NULL, '10001');
INSERT INTO `mscode_sys_post` VALUES (833283765555351552, 'CTS-RD-08', '研发部技术方案管理岗', 8, 833281691782402048, '2021-04-18 18:12:11', NULL, '10001');

-- ----------------------------
-- Table structure for mscode_sys_region
-- ----------------------------
DROP TABLE IF EXISTS `mscode_sys_region`;
CREATE TABLE `mscode_sys_region`  (
  `id` bigint(20) NOT NULL COMMENT '区域主键ID',
  `region_name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '区域名称',
  `region_code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '区域代码',
  `region_type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '区域类型',
  `parent_region_code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '上级区域代码',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `tenant_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '10001' COMMENT '租户编码',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_region_code`(`region_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '区域表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mscode_sys_region
-- ----------------------------
INSERT INTO `mscode_sys_region` VALUES (1, '北京市', '110000', 'province', '0', '2019-02-28 17:16:58', '2019-09-16 22:04:01', '10001');
INSERT INTO `mscode_sys_region` VALUES (2, '天津市', '120000', 'province', '0', '2019-02-28 17:16:58', '2019-09-24 18:13:48', '10001');
INSERT INTO `mscode_sys_region` VALUES (3, '河北省', '130000', 'province', '0', '2019-02-28 17:16:58', '2019-09-16 22:04:01', '10001');
INSERT INTO `mscode_sys_region` VALUES (4, '山西省', '140000', 'province', '0', '2019-02-28 17:16:58', '2019-09-16 22:04:01', '10001');
INSERT INTO `mscode_sys_region` VALUES (5, '内蒙古自治区', '150000', 'province', '0', '2019-02-28 17:16:58', '2019-09-16 22:04:01', '10001');
INSERT INTO `mscode_sys_region` VALUES (6, '辽宁省', '210000', 'province', '0', '2019-02-28 17:16:58', '2019-09-16 22:04:01', '10001');
INSERT INTO `mscode_sys_region` VALUES (7, '吉林省', '220000', 'province', '0', '2019-02-28 17:16:58', '2019-09-16 22:04:01', '10001');
INSERT INTO `mscode_sys_region` VALUES (8, '黑龙江省', '230000', 'province', '0', '2019-02-28 17:16:58', '2019-09-16 22:04:01', '10001');
INSERT INTO `mscode_sys_region` VALUES (9, '上海市', '310000', 'province', '0', '2019-02-28 17:16:58', '2019-09-16 22:04:01', '10001');
INSERT INTO `mscode_sys_region` VALUES (10, '江苏省', '320000', 'province', '0', '2019-02-28 17:16:58', '2019-09-16 22:04:01', '10001');
INSERT INTO `mscode_sys_region` VALUES (11, '浙江省', '330000', 'province', '0', '2019-02-28 17:16:58', '2019-09-16 22:04:01', '10001');
INSERT INTO `mscode_sys_region` VALUES (12, '安徽省', '340000', 'province', '0', '2019-02-28 17:16:58', '2019-09-16 22:04:01', '10001');
INSERT INTO `mscode_sys_region` VALUES (13, '福建省', '350000', 'province', '0', '2019-02-28 17:16:58', '2019-09-16 22:04:01', '10001');
INSERT INTO `mscode_sys_region` VALUES (14, '江西省', '360000', 'province', '0', '2019-02-28 17:16:58', '2019-09-16 22:04:01', '10001');
INSERT INTO `mscode_sys_region` VALUES (15, '山东省', '370000', 'province', '0', '2019-02-28 17:16:58', '2019-09-16 22:04:01', '10001');
INSERT INTO `mscode_sys_region` VALUES (16, '河南省', '410000', 'province', '0', '2019-02-28 17:16:58', '2019-09-16 22:04:01', '10001');
INSERT INTO `mscode_sys_region` VALUES (17, '湖北省', '420000', 'province', '0', '2019-02-28 17:16:58', '2019-09-16 22:04:01', '10001');
INSERT INTO `mscode_sys_region` VALUES (18, '湖南省', '430000', 'province', '0', '2019-02-28 17:16:58', '2019-09-16 22:04:01', '10001');
INSERT INTO `mscode_sys_region` VALUES (19, '广东省', '440000', 'province', '0', '2019-02-28 17:16:58', '2019-09-16 22:04:01', '10001');
INSERT INTO `mscode_sys_region` VALUES (20, '广西壮族自治区', '450000', 'province', '0', '2019-02-28 17:16:58', '2019-09-16 22:04:01', '10001');
INSERT INTO `mscode_sys_region` VALUES (21, '海南省', '460000', 'province', '0', '2019-02-28 17:16:58', '2019-09-16 22:04:01', '10001');
INSERT INTO `mscode_sys_region` VALUES (22, '重庆市', '500000', 'province', '0', '2019-02-28 17:16:58', '2019-09-16 22:04:01', '10001');
INSERT INTO `mscode_sys_region` VALUES (23, '四川省', '510000', 'province', '0', '2019-02-28 17:16:58', '2019-09-16 22:04:01', '10001');
INSERT INTO `mscode_sys_region` VALUES (24, '贵州省', '520000', 'province', '0', '2019-02-28 17:16:58', '2019-09-16 22:04:01', '10001');
INSERT INTO `mscode_sys_region` VALUES (25, '云南省', '530000', 'province', '0', '2019-02-28 17:16:58', '2019-09-16 22:04:01', '10001');
INSERT INTO `mscode_sys_region` VALUES (26, '西藏自治区', '540000', 'province', '0', '2019-02-28 17:16:58', '2019-09-16 22:04:01', '10001');
INSERT INTO `mscode_sys_region` VALUES (27, '陕西省', '610000', 'province', '0', '2019-02-28 17:16:58', '2019-09-16 22:04:01', '10001');
INSERT INTO `mscode_sys_region` VALUES (28, '甘肃省', '620000', 'province', '0', '2019-02-28 17:16:58', '2019-09-16 22:04:01', '10001');
INSERT INTO `mscode_sys_region` VALUES (29, '青海省', '630000', 'province', '0', '2019-02-28 17:16:58', '2019-09-16 22:04:01', '10001');
INSERT INTO `mscode_sys_region` VALUES (30, '宁夏回族自治区', '640000', 'province', '0', '2019-02-28 17:16:58', '2019-09-16 22:04:01', '10001');
INSERT INTO `mscode_sys_region` VALUES (31, '新疆维吾尔自治区', '650000', 'province', '0', '2019-02-28 17:16:58', '2019-09-16 22:04:01', '10001');
INSERT INTO `mscode_sys_region` VALUES (32, '台湾省', '710000', 'province', '0', '2019-02-28 17:16:58', '2019-09-16 22:04:01', '10001');
INSERT INTO `mscode_sys_region` VALUES (33, '香港特别行政区', '810000', 'province', '0', '2019-02-28 17:16:58', '2019-09-16 22:04:01', '10001');
INSERT INTO `mscode_sys_region` VALUES (34, '澳门特别行政区', '820000', 'province', '0', '2019-02-28 17:16:58', '2019-09-16 22:04:01', '10001');
INSERT INTO `mscode_sys_region` VALUES (35, '北京市', '110100', 'city', '110000', '2019-02-28 17:16:58', '2019-09-24 14:02:08', '10001');
INSERT INTO `mscode_sys_region` VALUES (36, '天津市', '120100', 'city', '120000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (37, '石家庄市', '130100', 'city', '130000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (38, '唐山市', '130200', 'city', '130000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (39, '秦皇岛市', '130300', 'city', '130000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (40, '邯郸市', '130400', 'city', '130000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (41, '邢台市', '130500', 'city', '130000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (42, '保定市', '130600', 'city', '130000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (43, '张家口市', '130700', 'city', '130000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (44, '承德市', '130800', 'city', '130000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (45, '沧州市', '130900', 'city', '130000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (46, '廊坊市', '131000', 'city', '130000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (47, '衡水市', '131100', 'city', '130000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (48, '太原市', '140100', 'city', '140000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (49, '大同市', '140200', 'city', '140000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (50, '阳泉市', '140300', 'city', '140000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (51, '长治市', '140400', 'city', '140000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (52, '晋城市', '140500', 'city', '140000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (53, '朔州市', '140600', 'city', '140000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (54, '晋中市', '140700', 'city', '140000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (55, '运城市', '140800', 'city', '140000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (56, '忻州市', '140900', 'city', '140000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (57, '临汾市', '141000', 'city', '140000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (58, '吕梁市', '141100', 'city', '140000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (59, '呼和浩特市', '150100', 'city', '150000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (60, '包头市', '150200', 'city', '150000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (61, '乌海市', '150300', 'city', '150000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (62, '赤峰市', '150400', 'city', '150000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (63, '通辽市', '150500', 'city', '150000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (64, '鄂尔多斯市', '150600', 'city', '150000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (65, '呼伦贝尔市', '150700', 'city', '150000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (66, '巴彦淖尔市', '150800', 'city', '150000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (67, '乌兰察布市', '150900', 'city', '150000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (68, '兴安盟', '152200', 'city', '150000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (69, '锡林郭勒盟', '152500', 'city', '150000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (70, '阿拉善盟', '152900', 'city', '150000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (71, '沈阳市', '210100', 'city', '210000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (72, '大连市', '210200', 'city', '210000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (73, '鞍山市', '210300', 'city', '210000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (74, '抚顺市', '210400', 'city', '210000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (75, '本溪市', '210500', 'city', '210000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (76, '丹东市', '210600', 'city', '210000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (77, '锦州市', '210700', 'city', '210000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (78, '营口市', '210800', 'city', '210000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (79, '阜新市', '210900', 'city', '210000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (80, '辽阳市', '211000', 'city', '210000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (81, '盘锦市', '211100', 'city', '210000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (82, '铁岭市', '211200', 'city', '210000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (83, '朝阳市', '211300', 'city', '210000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (84, '葫芦岛市', '211400', 'city', '210000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (85, '长春市', '220100', 'city', '220000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (86, '吉林市', '220200', 'city', '220000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (87, '四平市', '220300', 'city', '220000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (88, '辽源市', '220400', 'city', '220000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (89, '通化市', '220500', 'city', '220000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (90, '白山市', '220600', 'city', '220000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (91, '松原市', '220700', 'city', '220000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (92, '白城市', '220800', 'city', '220000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (93, '延边朝鲜族自治州', '222400', 'city', '220000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (94, '哈尔滨市', '230100', 'city', '230000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (95, '齐齐哈尔市', '230200', 'city', '230000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (96, '鸡西市', '230300', 'city', '230000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (97, '鹤岗市', '230400', 'city', '230000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (98, '双鸭山市', '230500', 'city', '230000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (99, '大庆市', '230600', 'city', '230000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (100, '伊春市', '230700', 'city', '230000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (101, '佳木斯市', '230800', 'city', '230000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (102, '七台河市', '230900', 'city', '230000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (103, '牡丹江市', '231000', 'city', '230000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (104, '黑河市', '231100', 'city', '230000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (105, '绥化市', '231200', 'city', '230000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (106, '大兴安岭地区', '232700', 'city', '230000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (107, '上海市', '310100', 'city', '310000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (108, '南京市', '320100', 'city', '320000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (109, '无锡市', '320200', 'city', '320000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (110, '徐州市', '320300', 'city', '320000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (111, '常州市', '320400', 'city', '320000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (112, '苏州市', '320500', 'city', '320000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (113, '南通市', '320600', 'city', '320000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (114, '连云港市', '320700', 'city', '320000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (115, '淮安市', '320800', 'city', '320000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (116, '盐城市', '320900', 'city', '320000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (117, '扬州市', '321000', 'city', '320000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (118, '镇江市', '321100', 'city', '320000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (119, '泰州市', '321200', 'city', '320000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (120, '宿迁市', '321300', 'city', '320000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (121, '杭州市', '330100', 'city', '330000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (122, '宁波市', '330200', 'city', '330000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (123, '温州市', '330300', 'city', '330000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (124, '嘉兴市', '330400', 'city', '330000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (125, '湖州市', '330500', 'city', '330000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (126, '绍兴市', '330600', 'city', '330000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (127, '金华市', '330700', 'city', '330000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (128, '衢州市', '330800', 'city', '330000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (129, '舟山市', '330900', 'city', '330000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (130, '台州市', '331000', 'city', '330000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (131, '丽水市', '331100', 'city', '330000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (132, '合肥市', '340100', 'city', '340000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (133, '芜湖市', '340200', 'city', '340000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (134, '蚌埠市', '340300', 'city', '340000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (135, '淮南市', '340400', 'city', '340000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (136, '马鞍山市', '340500', 'city', '340000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (137, '淮北市', '340600', 'city', '340000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (138, '铜陵市', '340700', 'city', '340000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (139, '安庆市', '340800', 'city', '340000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (140, '黄山市', '341000', 'city', '340000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (141, '滁州市', '341100', 'city', '340000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (142, '阜阳市', '341200', 'city', '340000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (143, '宿州市', '341300', 'city', '340000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (144, '六安市', '341500', 'city', '340000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (145, '亳州市', '341600', 'city', '340000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (146, '池州市', '341700', 'city', '340000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (147, '宣城市', '341800', 'city', '340000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (148, '福州市', '350100', 'city', '350000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (149, '厦门市', '350200', 'city', '350000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (150, '莆田市', '350300', 'city', '350000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (151, '三明市', '350400', 'city', '350000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (152, '泉州市', '350500', 'city', '350000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (153, '漳州市', '350600', 'city', '350000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (154, '南平市', '350700', 'city', '350000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (155, '龙岩市', '350800', 'city', '350000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (156, '宁德市', '350900', 'city', '350000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (157, '南昌市', '360100', 'city', '360000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (158, '景德镇市', '360200', 'city', '360000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (159, '萍乡市', '360300', 'city', '360000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (160, '九江市', '360400', 'city', '360000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (161, '新余市', '360500', 'city', '360000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (162, '鹰潭市', '360600', 'city', '360000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (163, '赣州市', '360700', 'city', '360000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (164, '吉安市', '360800', 'city', '360000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (165, '宜春市', '360900', 'city', '360000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (166, '抚州市', '361000', 'city', '360000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (167, '上饶市', '361100', 'city', '360000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (168, '济南市', '370100', 'city', '370000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (169, '青岛市', '370200', 'city', '370000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (170, '淄博市', '370300', 'city', '370000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (171, '枣庄市', '370400', 'city', '370000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (172, '东营市', '370500', 'city', '370000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (173, '烟台市', '370600', 'city', '370000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (174, '潍坊市', '370700', 'city', '370000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (175, '济宁市', '370800', 'city', '370000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (176, '泰安市', '370900', 'city', '370000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (177, '威海市', '371000', 'city', '370000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (178, '日照市', '371100', 'city', '370000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (179, '莱芜市', '371200', 'city', '370000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (180, '临沂市', '371300', 'city', '370000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (181, '德州市', '371400', 'city', '370000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (182, '聊城市', '371500', 'city', '370000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (183, '滨州市', '371600', 'city', '370000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (184, '菏泽市', '371700', 'city', '370000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (185, '郑州市', '410100', 'city', '410000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (186, '开封市', '410200', 'city', '410000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (187, '洛阳市', '410300', 'city', '410000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (188, '平顶山市', '410400', 'city', '410000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (189, '安阳市', '410500', 'city', '410000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (190, '鹤壁市', '410600', 'city', '410000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (191, '新乡市', '410700', 'city', '410000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (192, '焦作市', '410800', 'city', '410000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (193, '济源市', '410881', 'city', '410000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (194, '濮阳市', '410900', 'city', '410000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (195, '许昌市', '411000', 'city', '410000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (196, '漯河市', '411100', 'city', '410000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (197, '三门峡市', '411200', 'city', '410000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (198, '南阳市', '411300', 'city', '410000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (199, '商丘市', '411400', 'city', '410000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (200, '信阳市', '411500', 'city', '410000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (201, '周口市', '411600', 'city', '410000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (202, '驻马店市', '411700', 'city', '410000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (203, '武汉市', '420100', 'city', '420000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (204, '黄石市', '420200', 'city', '420000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (205, '十堰市', '420300', 'city', '420000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (206, '宜昌市', '420500', 'city', '420000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (207, '襄阳市', '420600', 'city', '420000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (208, '鄂州市', '420700', 'city', '420000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (209, '荆门市', '420800', 'city', '420000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (210, '孝感市', '420900', 'city', '420000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (211, '荆州市', '421000', 'city', '420000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (212, '黄冈市', '421100', 'city', '420000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (213, '咸宁市', '421200', 'city', '420000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (214, '随州市', '421300', 'city', '420000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (215, '恩施土家族苗族自治州', '422800', 'city', '420000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (216, '仙桃市', '429004', 'city', '420000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (217, '潜江市', '429005', 'city', '420000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (218, '天门市', '429006', 'city', '420000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (219, '神农架林区', '429021', 'city', '420000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (220, '长沙市', '430100', 'city', '430000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (221, '株洲市', '430200', 'city', '430000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (222, '湘潭市', '430300', 'city', '430000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (223, '衡阳市', '430400', 'city', '430000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (224, '邵阳市', '430500', 'city', '430000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (225, '岳阳市', '430600', 'city', '430000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (226, '常德市', '430700', 'city', '430000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (227, '张家界市', '430800', 'city', '430000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (228, '益阳市', '430900', 'city', '430000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (229, '郴州市', '431000', 'city', '430000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (230, '永州市', '431100', 'city', '430000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (231, '怀化市', '431200', 'city', '430000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (232, '娄底市', '431300', 'city', '430000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (233, '湘西土家族苗族自治州', '433100', 'city', '430000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (234, '广州市', '440100', 'city', '440000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (235, '韶关市', '440200', 'city', '440000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (236, '深圳市', '440300', 'city', '440000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (237, '珠海市', '440400', 'city', '440000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (238, '汕头市', '440500', 'city', '440000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (239, '佛山市', '440600', 'city', '440000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (240, '江门市', '440700', 'city', '440000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (241, '湛江市', '440800', 'city', '440000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (242, '茂名市', '440900', 'city', '440000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (243, '肇庆市', '441200', 'city', '440000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (244, '惠州市', '441300', 'city', '440000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (245, '梅州市', '441400', 'city', '440000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (246, '汕尾市', '441500', 'city', '440000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (247, '河源市', '441600', 'city', '440000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (248, '阳江市', '441700', 'city', '440000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (249, '清远市', '441800', 'city', '440000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (250, '东莞市', '441900', 'city', '440000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (251, '中山市', '442000', 'city', '440000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (252, '东沙群岛', '442101', 'city', '440000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (253, '潮州市', '445100', 'city', '440000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (254, '揭阳市', '445200', 'city', '440000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (255, '云浮市', '445300', 'city', '440000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (256, '南宁市', '450100', 'city', '450000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (257, '柳州市', '450200', 'city', '450000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (258, '桂林市', '450300', 'city', '450000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (259, '梧州市', '450400', 'city', '450000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (260, '北海市', '450500', 'city', '450000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (261, '防城港市', '450600', 'city', '450000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (262, '钦州市', '450700', 'city', '450000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (263, '贵港市', '450800', 'city', '450000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (264, '玉林市', '450900', 'city', '450000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (265, '百色市', '451000', 'city', '450000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (266, '贺州市', '451100', 'city', '450000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (267, '河池市', '451200', 'city', '450000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (268, '来宾市', '451300', 'city', '450000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (269, '崇左市', '451400', 'city', '450000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (270, '海口市', '460100', 'city', '460000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (271, '三亚市', '460200', 'city', '460000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (272, '三沙市', '460300', 'city', '460000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (273, '五指山市', '469001', 'city', '460000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (274, '琼海市', '469002', 'city', '460000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (275, '儋州市', '469003', 'city', '460000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (276, '文昌市', '469005', 'city', '460000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (277, '万宁市', '469006', 'city', '460000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (278, '东方市', '469007', 'city', '460000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (279, '定安县', '469025', 'city', '460000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (280, '屯昌县', '469026', 'city', '460000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (281, '澄迈县', '469027', 'city', '460000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (282, '临高县', '469028', 'city', '460000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (283, '白沙黎族自治县', '469030', 'city', '460000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (284, '昌江黎族自治县', '469031', 'city', '460000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (285, '乐东黎族自治县', '469033', 'city', '460000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (286, '陵水黎族自治县', '469034', 'city', '460000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (287, '保亭黎族苗族自治县', '469035', 'city', '460000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (288, '琼中黎族苗族自治县', '469036', 'city', '460000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (289, '重庆市', '500100', 'city', '500000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (290, '成都市', '510100', 'city', '510000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (291, '自贡市', '510300', 'city', '510000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (292, '攀枝花市', '510400', 'city', '510000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (293, '泸州市', '510500', 'city', '510000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (294, '德阳市', '510600', 'city', '510000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (295, '绵阳市', '510700', 'city', '510000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (296, '广元市', '510800', 'city', '510000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (297, '遂宁市', '510900', 'city', '510000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (298, '内江市', '511000', 'city', '510000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (299, '乐山市', '511100', 'city', '510000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (300, '南充市', '511300', 'city', '510000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (301, '眉山市', '511400', 'city', '510000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (302, '宜宾市', '511500', 'city', '510000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (303, '广安市', '511600', 'city', '510000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (304, '达州市', '511700', 'city', '510000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (305, '雅安市', '511800', 'city', '510000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (306, '巴中市', '511900', 'city', '510000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (307, '资阳市', '512000', 'city', '510000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (308, '阿坝藏族羌族自治州', '513200', 'city', '510000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (309, '甘孜藏族自治州', '513300', 'city', '510000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (310, '凉山彝族自治州', '513400', 'city', '510000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (311, '贵阳市', '520100', 'city', '520000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (312, '六盘水市', '520200', 'city', '520000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (313, '遵义市', '520300', 'city', '520000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (314, '安顺市', '520400', 'city', '520000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (315, '铜仁市', '522200', 'city', '520000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (316, '黔西南布依族苗族自治州', '522300', 'city', '520000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (317, '毕节市', '522400', 'city', '520000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (318, '黔东南苗族侗族自治州', '522600', 'city', '520000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (319, '黔南布依族苗族自治州', '522700', 'city', '520000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (320, '昆明市', '530100', 'city', '530000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (321, '曲靖市', '530300', 'city', '530000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (322, '玉溪市', '530400', 'city', '530000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (323, '保山市', '530500', 'city', '530000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (324, '昭通市', '530600', 'city', '530000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (325, '丽江市', '530700', 'city', '530000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (326, '普洱市', '530800', 'city', '530000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (327, '临沧市', '530900', 'city', '530000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (328, '楚雄彝族自治州', '532300', 'city', '530000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (329, '红河哈尼族彝族自治州', '532500', 'city', '530000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (330, '文山壮族苗族自治州', '532600', 'city', '530000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (331, '西双版纳傣族自治州', '532800', 'city', '530000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (332, '大理白族自治州', '532900', 'city', '530000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (333, '德宏傣族景颇族自治州', '533100', 'city', '530000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (334, '怒江傈僳族自治州', '533300', 'city', '530000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (335, '迪庆藏族自治州', '533400', 'city', '530000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (336, '拉萨市', '540100', 'city', '540000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (337, '昌都地区', '542100', 'city', '540000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (338, '山南地区', '542200', 'city', '540000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (339, '日喀则地区', '542300', 'city', '540000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (340, '那曲地区', '542400', 'city', '540000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (341, '阿里地区', '542500', 'city', '540000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (342, '林芝地区', '542600', 'city', '540000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (343, '西安市', '610100', 'city', '610000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (344, '铜川市', '610200', 'city', '610000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (345, '宝鸡市', '610300', 'city', '610000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (346, '咸阳市', '610400', 'city', '610000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (347, '渭南市', '610500', 'city', '610000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (348, '延安市', '610600', 'city', '610000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (349, '汉中市', '610700', 'city', '610000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (350, '榆林市', '610800', 'city', '610000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (351, '安康市', '610900', 'city', '610000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (352, '商洛市', '611000', 'city', '610000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (353, '兰州市', '620100', 'city', '620000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (354, '嘉峪关市', '620200', 'city', '620000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (355, '金昌市', '620300', 'city', '620000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (356, '白银市', '620400', 'city', '620000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (357, '天水市', '620500', 'city', '620000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (358, '武威市', '620600', 'city', '620000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (359, '张掖市', '620700', 'city', '620000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (360, '平凉市', '620800', 'city', '620000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (361, '酒泉市', '620900', 'city', '620000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (362, '庆阳市', '621000', 'city', '620000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (363, '定西市', '621100', 'city', '620000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (364, '陇南市', '621200', 'city', '620000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (365, '临夏回族自治州', '622900', 'city', '620000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (366, '甘南藏族自治州', '623000', 'city', '620000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (367, '西宁市', '630100', 'city', '630000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (368, '海东市', '632100', 'city', '630000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (369, '海北藏族自治州', '632200', 'city', '630000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (370, '黄南藏族自治州', '632300', 'city', '630000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (371, '海南藏族自治州', '632500', 'city', '630000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (372, '果洛藏族自治州', '632600', 'city', '630000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (373, '玉树藏族自治州', '632700', 'city', '630000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (374, '海西蒙古族藏族自治州', '632800', 'city', '630000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (375, '银川市', '640100', 'city', '640000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (376, '石嘴山市', '640200', 'city', '640000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (377, '吴忠市', '640300', 'city', '640000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (378, '固原市', '640400', 'city', '640000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (379, '中卫市', '640500', 'city', '640000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (380, '乌鲁木齐市', '650100', 'city', '650000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (381, '克拉玛依市', '650200', 'city', '650000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (382, '吐鲁番地区', '652100', 'city', '650000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (383, '哈密地区', '652200', 'city', '650000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (384, '昌吉回族自治州', '652300', 'city', '650000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (385, '博尔塔拉蒙古自治州', '652700', 'city', '650000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (386, '巴音郭楞蒙古自治州', '652800', 'city', '650000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (387, '阿克苏地区', '652900', 'city', '650000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (388, '克孜勒苏柯尔克孜自治州', '653000', 'city', '650000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (389, '喀什地区', '653100', 'city', '650000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (390, '和田地区', '653200', 'city', '650000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (391, '伊犁哈萨克自治州', '654000', 'city', '650000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (392, '塔城地区', '654200', 'city', '650000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (393, '阿勒泰地区', '654300', 'city', '650000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (394, '石河子市', '659001', 'city', '650000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (395, '阿拉尔市', '659002', 'city', '650000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (396, '图木舒克市', '659003', 'city', '650000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (397, '五家渠市', '659004', 'city', '650000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (398, '台北市', '710100', 'city', '710000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (399, '高雄市', '710200', 'city', '710000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (400, '台南市', '710300', 'city', '710000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (401, '台中市', '710400', 'city', '710000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (402, '金门县', '710500', 'city', '710000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (403, '南投县', '710600', 'city', '710000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (404, '基隆市', '710700', 'city', '710000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (405, '新竹市', '710800', 'city', '710000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (406, '嘉义市', '710900', 'city', '710000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (407, '新北市', '711100', 'city', '710000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (408, '宜兰县', '711200', 'city', '710000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (409, '新竹县', '711300', 'city', '710000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (410, '桃园县', '711400', 'city', '710000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (411, '苗栗县', '711500', 'city', '710000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (412, '彰化县', '711700', 'city', '710000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (413, '嘉义县', '711900', 'city', '710000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (414, '云林县', '712100', 'city', '710000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (415, '屏东县', '712400', 'city', '710000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (416, '台东县', '712500', 'city', '710000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (417, '花莲县', '712600', 'city', '710000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (418, '澎湖县', '712700', 'city', '710000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (419, '连江县', '712800', 'city', '710000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (420, '香港岛', '810100', 'city', '810000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (421, '九龙', '810200', 'city', '810000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (422, '新界', '810300', 'city', '810000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (423, '澳门半岛', '820100', 'city', '820000', '2019-02-28 17:16:58', NULL, '10001');
INSERT INTO `mscode_sys_region` VALUES (424, '离岛', '820200', 'city', '820000', '2019-02-28 17:16:58', NULL, '10001');

-- ----------------------------
-- Table structure for mscode_sys_role
-- ----------------------------
DROP TABLE IF EXISTS `mscode_sys_role`;
CREATE TABLE `mscode_sys_role`  (
  `id` bigint(20) NOT NULL COMMENT '角色主键ID',
  `role_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色编码',
  `role_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  `role_description` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色描述',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `tenant_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '10001' COMMENT '租户编码',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_role_code`(`role_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mscode_sys_role
-- ----------------------------
INSERT INTO `mscode_sys_role` VALUES (665499597804064768, 'ROLE_ADMIN', '超级管理员', '超级管理员拥有所有权限', '2020-01-11 18:17:48', NULL, '10001');
INSERT INTO `mscode_sys_role` VALUES (666858336536678400, 'ROLE_DEV', '开发角色', '开发人员属于开发角色', '2020-01-15 12:16:58', '2020-03-18 10:14:39', '10001');
INSERT INTO `mscode_sys_role` VALUES (666858434746306560, 'ROLE_TEST', '测试角色', '测试人员属于测试角色', '2020-01-15 12:17:22', '2020-03-18 10:15:04', '10001');
INSERT INTO `mscode_sys_role` VALUES (685031431038488576, 'ROLE_ANONYMOUS', '匿名角色', '无需登录就可以拥有的权限，比如注册', '2020-01-05 15:50:22', '2020-03-16 00:22:32', '10001');
INSERT INTO `mscode_sys_role` VALUES (689659913718648832, 'ROLE_MANAGER', '管理角色', '管理人员属于管理角色', '2020-03-18 10:22:21', NULL, '10001');
INSERT INTO `mscode_sys_role` VALUES (689660323321794560, 'ROLE_AGENT', '行政角色', '行政人员属于行政角色', '2020-03-18 10:23:58', '2021-03-08 10:50:07', '10001');
INSERT INTO `mscode_sys_role` VALUES (820240126587621376, 'ROLE_REACT_ADMIN', '管理员', '这是React管理员', '2020-01-06 18:21:28', '2021-03-13 20:02:42', '10001');

-- ----------------------------
-- Table structure for mscode_sys_role_data
-- ----------------------------
DROP TABLE IF EXISTS `mscode_sys_role_data`;
CREATE TABLE `mscode_sys_role_data`  (
  `id` bigint(20) NOT NULL COMMENT '角色与过滤数据关联主键ID',
  `role_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色编码',
  `menu_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单编码',
  `data_field` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '过滤数据',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色与过滤数据关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for mscode_sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `mscode_sys_role_menu`;
CREATE TABLE `mscode_sys_role_menu`  (
  `id` bigint(20) NOT NULL COMMENT '角色与菜单关联主键ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色主键ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单主键ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色与菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mscode_sys_role_menu
-- ----------------------------
INSERT INTO `mscode_sys_role_menu` VALUES (693699450014978048, 689659913718648832, 663854683370475520);
INSERT INTO `mscode_sys_role_menu` VALUES (693699450145001472, 689659913718648832, 687514791186976768);
INSERT INTO `mscode_sys_role_menu` VALUES (693699450254053376, 689659913718648832, 663854974853632000);
INSERT INTO `mscode_sys_role_menu` VALUES (693699450379882496, 689659913718648832, 693434079391436800);
INSERT INTO `mscode_sys_role_menu` VALUES (693699450497323008, 689659913718648832, 687563817764704256);
INSERT INTO `mscode_sys_role_menu` VALUES (693699450614763520, 689659913718648832, 665197875319721984);
INSERT INTO `mscode_sys_role_menu` VALUES (693699450715426816, 689659913718648832, 663863400497336320);
INSERT INTO `mscode_sys_role_menu` VALUES (693699450824478720, 689659913718648832, 663750580350930944);
INSERT INTO `mscode_sys_role_menu` VALUES (693699450929336320, 689659913718648832, 693698880994725888);
INSERT INTO `mscode_sys_role_menu` VALUES (693699451046776832, 689659913718648832, 663862460172128256);
INSERT INTO `mscode_sys_role_menu` VALUES (693699451164217344, 689659913718648832, 663863146888744960);
INSERT INTO `mscode_sys_role_menu` VALUES (693699451302629376, 689659913718648832, 663863700486541312);
INSERT INTO `mscode_sys_role_menu` VALUES (693699451403292672, 689659913718648832, 665198396457799680);
INSERT INTO `mscode_sys_role_menu` VALUES (693699451529121792, 689659913718648832, 665198241801228288);
INSERT INTO `mscode_sys_role_menu` VALUES (693699451680116736, 689659913718648832, 665198079427137536);
INSERT INTO `mscode_sys_role_menu` VALUES (693699451784974336, 689659913718648832, 663862916743090176);
INSERT INTO `mscode_sys_role_menu` VALUES (693699451902414848, 689659913718648832, 663862096987344896);
INSERT INTO `mscode_sys_role_menu` VALUES (693699494290051072, 689660323321794560, 663854683370475520);
INSERT INTO `mscode_sys_role_menu` VALUES (693699495976161280, 689660323321794560, 687514791186976768);
INSERT INTO `mscode_sys_role_menu` VALUES (693699499822338048, 689660323321794560, 663854974853632000);
INSERT INTO `mscode_sys_role_menu` VALUES (693699501500059648, 689660323321794560, 693434079391436800);
INSERT INTO `mscode_sys_role_menu` VALUES (693699502213091328, 689660323321794560, 687563817764704256);
INSERT INTO `mscode_sys_role_menu` VALUES (693699503492354048, 689660323321794560, 665197875319721984);
INSERT INTO `mscode_sys_role_menu` VALUES (693699503723040768, 689660323321794560, 663863400497336320);
INSERT INTO `mscode_sys_role_menu` VALUES (693699504046002176, 689660323321794560, 663750580350930944);
INSERT INTO `mscode_sys_role_menu` VALUES (693699507208507392, 689660323321794560, 693698880994725888);
INSERT INTO `mscode_sys_role_menu` VALUES (693699507640520704, 689660323321794560, 663862460172128256);
INSERT INTO `mscode_sys_role_menu` VALUES (693699508332580864, 689660323321794560, 663863146888744960);
INSERT INTO `mscode_sys_role_menu` VALUES (693699508525518848, 689660323321794560, 663863700486541312);
INSERT INTO `mscode_sys_role_menu` VALUES (693699508785565696, 689660323321794560, 665198396457799680);
INSERT INTO `mscode_sys_role_menu` VALUES (693699509326630912, 689660323321794560, 665198241801228288);
INSERT INTO `mscode_sys_role_menu` VALUES (693699509788004352, 689660323321794560, 665198079427137536);
INSERT INTO `mscode_sys_role_menu` VALUES (693699514204606464, 689660323321794560, 663862916743090176);
INSERT INTO `mscode_sys_role_menu` VALUES (693699514594676736, 689660323321794560, 663862096987344896);
INSERT INTO `mscode_sys_role_menu` VALUES (708495380777979904, 666858336536678400, 663857014669496320);
INSERT INTO `mscode_sys_role_menu` VALUES (708495380949946368, 666858336536678400, 687514791186976768);
INSERT INTO `mscode_sys_role_menu` VALUES (708495381038026752, 666858336536678400, 693434079391436800);
INSERT INTO `mscode_sys_role_menu` VALUES (708495381130301440, 666858336536678400, 663855944106954752);
INSERT INTO `mscode_sys_role_menu` VALUES (708495381214187520, 666858336536678400, 663866117546496000);
INSERT INTO `mscode_sys_role_menu` VALUES (708495381310656512, 666858336536678400, 699081036910743552);
INSERT INTO `mscode_sys_role_menu` VALUES (708495381423902720, 666858336536678400, 665197875319721984);
INSERT INTO `mscode_sys_role_menu` VALUES (708495381507788800, 666858336536678400, 663856793264771072);
INSERT INTO `mscode_sys_role_menu` VALUES (708495381608452096, 666858336536678400, 663863400497336320);
INSERT INTO `mscode_sys_role_menu` VALUES (708495381709115392, 666858336536678400, 663750580350930944);
INSERT INTO `mscode_sys_role_menu` VALUES (708495381805584384, 666858336536678400, 663865201925738496);
INSERT INTO `mscode_sys_role_menu` VALUES (708495381897859072, 666858336536678400, 693698880994725888);
INSERT INTO `mscode_sys_role_menu` VALUES (708495381990133760, 666858336536678400, 663862460172128256);
INSERT INTO `mscode_sys_role_menu` VALUES (708495382086602752, 666858336536678400, 684185040317763584);
INSERT INTO `mscode_sys_role_menu` VALUES (708495382174683136, 666858336536678400, 663856139884482560);
INSERT INTO `mscode_sys_role_menu` VALUES (708495382266957824, 666858336536678400, 663867140080062464);
INSERT INTO `mscode_sys_role_menu` VALUES (708495382363426816, 666858336536678400, 665198396457799680);
INSERT INTO `mscode_sys_role_menu` VALUES (708495382459895808, 666858336536678400, 663857253375725568);
INSERT INTO `mscode_sys_role_menu` VALUES (708495382560559104, 666858336536678400, 663865503307452416);
INSERT INTO `mscode_sys_role_menu` VALUES (708495382665416704, 666858336536678400, 663864697992695808);
INSERT INTO `mscode_sys_role_menu` VALUES (708495382761885696, 666858336536678400, 665496626324230144);
INSERT INTO `mscode_sys_role_menu` VALUES (708495382849966080, 666858336536678400, 663865799052021760);
INSERT INTO `mscode_sys_role_menu` VALUES (708495382946435072, 666858336536678400, 665198079427137536);
INSERT INTO `mscode_sys_role_menu` VALUES (708495383042904064, 666858336536678400, 663864025209556992);
INSERT INTO `mscode_sys_role_menu` VALUES (708495383147761664, 666858336536678400, 663854683370475520);
INSERT INTO `mscode_sys_role_menu` VALUES (708495383240036352, 666858336536678400, 663854974853632000);
INSERT INTO `mscode_sys_role_menu` VALUES (708495383332311040, 666858336536678400, 663866797015355392);
INSERT INTO `mscode_sys_role_menu` VALUES (708495383424585728, 666858336536678400, 693762978637860864);
INSERT INTO `mscode_sys_role_menu` VALUES (708495383521054720, 666858336536678400, 693762431952277504);
INSERT INTO `mscode_sys_role_menu` VALUES (708495383625912320, 666858336536678400, 663867636220088320);
INSERT INTO `mscode_sys_role_menu` VALUES (708495383718187008, 666858336536678400, 690019670816116736);
INSERT INTO `mscode_sys_role_menu` VALUES (708495383856599040, 666858336536678400, 663856370193715200);
INSERT INTO `mscode_sys_role_menu` VALUES (708495383953068032, 666858336536678400, 687563817764704256);
INSERT INTO `mscode_sys_role_menu` VALUES (708495384066314240, 666858336536678400, 663867371509174272);
INSERT INTO `mscode_sys_role_menu` VALUES (708495384171171840, 666858336536678400, 663864915588993024);
INSERT INTO `mscode_sys_role_menu` VALUES (708495384267640832, 666858336536678400, 663855709905408000);
INSERT INTO `mscode_sys_role_menu` VALUES (708495384380887040, 666858336536678400, 663863146888744960);
INSERT INTO `mscode_sys_role_menu` VALUES (708495384468967424, 666858336536678400, 663863700486541312);
INSERT INTO `mscode_sys_role_menu` VALUES (708495384561242112, 666858336536678400, 663866457184456704);
INSERT INTO `mscode_sys_role_menu` VALUES (708495384661905408, 666858336536678400, 663864263244697600);
INSERT INTO `mscode_sys_role_menu` VALUES (708495384762568704, 666858336536678400, 665198241801228288);
INSERT INTO `mscode_sys_role_menu` VALUES (708495384859037696, 666858336536678400, 663855492212641792);
INSERT INTO `mscode_sys_role_menu` VALUES (708495384951312384, 666858336536678400, 663862916743090176);
INSERT INTO `mscode_sys_role_menu` VALUES (708495385043587072, 666858336536678400, 663856596409307136);
INSERT INTO `mscode_sys_role_menu` VALUES (708495385144250368, 666858336536678400, 663862096987344896);
INSERT INTO `mscode_sys_role_menu` VALUES (708495733544112128, 666858434746306560, 687514791186976768);
INSERT INTO `mscode_sys_role_menu` VALUES (708495733665746944, 666858434746306560, 693434079391436800);
INSERT INTO `mscode_sys_role_menu` VALUES (708495733795770368, 666858434746306560, 663866117546496000);
INSERT INTO `mscode_sys_role_menu` VALUES (708495733938376704, 666858434746306560, 665197875319721984);
INSERT INTO `mscode_sys_role_menu` VALUES (708495734039040000, 666858434746306560, 663863400497336320);
INSERT INTO `mscode_sys_role_menu` VALUES (708495734131314688, 666858434746306560, 663750580350930944);
INSERT INTO `mscode_sys_role_menu` VALUES (708495734261338112, 666858434746306560, 693698880994725888);
INSERT INTO `mscode_sys_role_menu` VALUES (708495734420721664, 666858434746306560, 663865201925738496);
INSERT INTO `mscode_sys_role_menu` VALUES (708495734529773568, 666858434746306560, 663862460172128256);
INSERT INTO `mscode_sys_role_menu` VALUES (708495734668185600, 666858434746306560, 663867140080062464);
INSERT INTO `mscode_sys_role_menu` VALUES (708495734785626112, 666858434746306560, 665198396457799680);
INSERT INTO `mscode_sys_role_menu` VALUES (708495734936621056, 666858434746306560, 663865503307452416);
INSERT INTO `mscode_sys_role_menu` VALUES (708495735041478656, 666858434746306560, 663864697992695808);
INSERT INTO `mscode_sys_role_menu` VALUES (708495735167307776, 666858434746306560, 663865799052021760);
INSERT INTO `mscode_sys_role_menu` VALUES (708495735284748288, 666858434746306560, 665198079427137536);
INSERT INTO `mscode_sys_role_menu` VALUES (708495735406383104, 666858434746306560, 663864025209556992);
INSERT INTO `mscode_sys_role_menu` VALUES (708495735532212224, 666858434746306560, 663854683370475520);
INSERT INTO `mscode_sys_role_menu` VALUES (708495735641264128, 666858434746306560, 663866797015355392);
INSERT INTO `mscode_sys_role_menu` VALUES (708495735771287552, 666858434746306560, 663854974853632000);
INSERT INTO `mscode_sys_role_menu` VALUES (708495735884533760, 666858434746306560, 693762978637860864);
INSERT INTO `mscode_sys_role_menu` VALUES (708495735981002752, 666858434746306560, 693762431952277504);
INSERT INTO `mscode_sys_role_menu` VALUES (708495736186523648, 666858434746306560, 663867636220088320);
INSERT INTO `mscode_sys_role_menu` VALUES (708495736287186944, 666858434746306560, 687563817764704256);
INSERT INTO `mscode_sys_role_menu` VALUES (708495736396238848, 666858434746306560, 663867371509174272);
INSERT INTO `mscode_sys_role_menu` VALUES (708495736505290752, 666858434746306560, 663864915588993024);
INSERT INTO `mscode_sys_role_menu` VALUES (708495736618536960, 666858434746306560, 663855709905408000);
INSERT INTO `mscode_sys_role_menu` VALUES (708495736715005952, 666858434746306560, 663863146888744960);
INSERT INTO `mscode_sys_role_menu` VALUES (708495736815669248, 666858434746306560, 663863700486541312);
INSERT INTO `mscode_sys_role_menu` VALUES (708495736916332544, 666858434746306560, 663866457184456704);
INSERT INTO `mscode_sys_role_menu` VALUES (708495737016995840, 666858434746306560, 663864263244697600);
INSERT INTO `mscode_sys_role_menu` VALUES (708495737117659136, 666858434746306560, 665198241801228288);
INSERT INTO `mscode_sys_role_menu` VALUES (708495737218322432, 666858434746306560, 663855492212641792);
INSERT INTO `mscode_sys_role_menu` VALUES (708495737331568640, 666858434746306560, 663862916743090176);
INSERT INTO `mscode_sys_role_menu` VALUES (708495737428037632, 666858434746306560, 663862096987344896);
INSERT INTO `mscode_sys_role_menu` VALUES (871705304789078016, 665499597804064768, 687514791186976768);
INSERT INTO `mscode_sys_role_menu` VALUES (871705305099456512, 665499597804064768, 665197381171990528);
INSERT INTO `mscode_sys_role_menu` VALUES (871705306181586944, 665499597804064768, 665197875319721984);
INSERT INTO `mscode_sys_role_menu` VALUES (871705306475188224, 665499597804064768, 663863400497336320);
INSERT INTO `mscode_sys_role_menu` VALUES (871705311789371392, 665499597804064768, 705593293912068096);
INSERT INTO `mscode_sys_role_menu` VALUES (871705311885840384, 665499597804064768, 693698880994725888);
INSERT INTO `mscode_sys_role_menu` VALUES (871705313693585408, 665499597804064768, 684185040317763584);
INSERT INTO `mscode_sys_role_menu` VALUES (871705316558295040, 665499597804064768, 663862460172128256);
INSERT INTO `mscode_sys_role_menu` VALUES (871705318970019840, 665499597804064768, 663867140080062464);
INSERT INTO `mscode_sys_role_menu` VALUES (871705321591459840, 665499597804064768, 663868238819938304);
INSERT INTO `mscode_sys_role_menu` VALUES (871705322249965568, 665499597804064768, 665564219978469376);
INSERT INTO `mscode_sys_role_menu` VALUES (871705322468069376, 665499597804064768, 663864025209556992);
INSERT INTO `mscode_sys_role_menu` VALUES (871705322568732672, 665499597804064768, 833249453736906752);
INSERT INTO `mscode_sys_role_menu` VALUES (871705322656813056, 665499597804064768, 663854974853632000);
INSERT INTO `mscode_sys_role_menu` VALUES (871705327295713280, 665499597804064768, 663866797015355392);
INSERT INTO `mscode_sys_role_menu` VALUES (871705327408959488, 665499597804064768, 663867371509174272);
INSERT INTO `mscode_sys_role_menu` VALUES (871705331368382464, 665499597804064768, 665199936786583552);
INSERT INTO `mscode_sys_role_menu` VALUES (871705331473240064, 665499597804064768, 663861613581225984);
INSERT INTO `mscode_sys_role_menu` VALUES (871705331557126144, 665499597804064768, 663864915588993024);
INSERT INTO `mscode_sys_role_menu` VALUES (871705331661983744, 665499597804064768, 665009153424936960);
INSERT INTO `mscode_sys_role_menu` VALUES (871705331754258432, 665499597804064768, 663868640676204544);
INSERT INTO `mscode_sys_role_menu` VALUES (871705331930419200, 665499597804064768, 665010520889675776);
INSERT INTO `mscode_sys_role_menu` VALUES (871705332035276800, 665499597804064768, 665199632426913792);
INSERT INTO `mscode_sys_role_menu` VALUES (871705332198854656, 665499597804064768, 663857014669496320);
INSERT INTO `mscode_sys_role_menu` VALUES (871705332337266688, 665499597804064768, 665003871265280000);
INSERT INTO `mscode_sys_role_menu` VALUES (871705332437929984, 665499597804064768, 665007146580496384);
INSERT INTO `mscode_sys_role_menu` VALUES (871705332542787584, 665499597804064768, 693434079391436800);
INSERT INTO `mscode_sys_role_menu` VALUES (871705332630867968, 665499597804064768, 663866117546496000);
INSERT INTO `mscode_sys_role_menu` VALUES (871705332723142656, 665499597804064768, 871704733705228288);
INSERT INTO `mscode_sys_role_menu` VALUES (871705332815417344, 665499597804064768, 665006303672193024);
INSERT INTO `mscode_sys_role_menu` VALUES (871705332907692032, 665499597804064768, 663856793264771072);
INSERT INTO `mscode_sys_role_menu` VALUES (871705333033521152, 665499597804064768, 705595098737856512);
INSERT INTO `mscode_sys_role_menu` VALUES (871705333117407232, 665499597804064768, 666473881804394496);
INSERT INTO `mscode_sys_role_menu` VALUES (871705333213876224, 665499597804064768, 663865201925738496);
INSERT INTO `mscode_sys_role_menu` VALUES (871705333339705344, 665499597804064768, 663856139884482560);
INSERT INTO `mscode_sys_role_menu` VALUES (871705333461340160, 665499597804064768, 665198396457799680);
INSERT INTO `mscode_sys_role_menu` VALUES (871705333784301568, 665499597804064768, 665010657795952640);
INSERT INTO `mscode_sys_role_menu` VALUES (871705333872381952, 665499597804064768, 663865503307452416);
INSERT INTO `mscode_sys_role_menu` VALUES (871705333973045248, 665499597804064768, 663865799052021760);
INSERT INTO `mscode_sys_role_menu` VALUES (871705334073708544, 665499597804064768, 665006482060136448);
INSERT INTO `mscode_sys_role_menu` VALUES (871705334291812352, 665499597804064768, 693762978637860864);
INSERT INTO `mscode_sys_role_menu` VALUES (871705334400864256, 665499597804064768, 687563817764704256);
INSERT INTO `mscode_sys_role_menu` VALUES (871705334484750336, 665499597804064768, 665009726589161472);
INSERT INTO `mscode_sys_role_menu` VALUES (871705334589607936, 665499597804064768, 663855709905408000);
INSERT INTO `mscode_sys_role_menu` VALUES (871705334690271232, 665499597804064768, 663863700486541312);
INSERT INTO `mscode_sys_role_menu` VALUES (871705334778351616, 665499597804064768, 665730162285268992);
INSERT INTO `mscode_sys_role_menu` VALUES (871705334979678208, 665499597804064768, 820207211942170624);
INSERT INTO `mscode_sys_role_menu` VALUES (871705335160033280, 665499597804064768, 665192197972742144);
INSERT INTO `mscode_sys_role_menu` VALUES (871705335264890880, 665499597804064768, 665564620060545024);
INSERT INTO `mscode_sys_role_menu` VALUES (871705335407497216, 665499597804064768, 699081036910743552);
INSERT INTO `mscode_sys_role_menu` VALUES (871705335508160512, 665499597804064768, 665006035807162368);
INSERT INTO `mscode_sys_role_menu` VALUES (871705335600435200, 665499597804064768, 665198983094128640);
INSERT INTO `mscode_sys_role_menu` VALUES (871705335692709888, 665499597804064768, 665004888883122176);
INSERT INTO `mscode_sys_role_menu` VALUES (871705335814344704, 665499597804064768, 666473278365683712);
INSERT INTO `mscode_sys_role_menu` VALUES (871705335944368128, 665499597804064768, 852017916051640320);
INSERT INTO `mscode_sys_role_menu` VALUES (871705336066002944, 665499597804064768, 871025442801766400);
INSERT INTO `mscode_sys_role_menu` VALUES (871705336175054848, 665499597804064768, 663857253375725568);
INSERT INTO `mscode_sys_role_menu` VALUES (871705336271523840, 665499597804064768, 663867981964955648);
INSERT INTO `mscode_sys_role_menu` VALUES (871705336393158656, 665499597804064768, 663864697992695808);
INSERT INTO `mscode_sys_role_menu` VALUES (871705336489627648, 665499597804064768, 665198079427137536);
INSERT INTO `mscode_sys_role_menu` VALUES (871705336611262464, 665499597804064768, 665197527700000768);
INSERT INTO `mscode_sys_role_menu` VALUES (871705336728702976, 665499597804064768, 692361087815176192);
INSERT INTO `mscode_sys_role_menu` VALUES (871705336837754880, 665499597804064768, 663854683370475520);
INSERT INTO `mscode_sys_role_menu` VALUES (871705336925835264, 665499597804064768, 665199423219224576);
INSERT INTO `mscode_sys_role_menu` VALUES (871705337081024512, 665499597804064768, 871025918976905216);
INSERT INTO `mscode_sys_role_menu` VALUES (871705337177493504, 665499597804064768, 690019670816116736);
INSERT INTO `mscode_sys_role_menu` VALUES (871705337269768192, 665499597804064768, 663856370193715200);
INSERT INTO `mscode_sys_role_menu` VALUES (871705337383014400, 665499597804064768, 665007802225709056);
INSERT INTO `mscode_sys_role_menu` VALUES (871705337844387840, 665499597804064768, 665005583355006976);
INSERT INTO `mscode_sys_role_menu` VALUES (871705338020548608, 665499597804064768, 665010791703302144);
INSERT INTO `mscode_sys_role_menu` VALUES (871705338188320768, 665499597804064768, 665198241801228288);
INSERT INTO `mscode_sys_role_menu` VALUES (871705338431590400, 665499597804064768, 663855492212641792);
INSERT INTO `mscode_sys_role_menu` VALUES (871705338565808128, 665499597804064768, 665192716074143744);
INSERT INTO `mscode_sys_role_menu` VALUES (871705338783911936, 665499597804064768, 852017515856318464);
INSERT INTO `mscode_sys_role_menu` VALUES (871705338922323968, 665499597804064768, 665007437736497152);
INSERT INTO `mscode_sys_role_menu` VALUES (871705339161399296, 665499597804064768, 663855944106954752);
INSERT INTO `mscode_sys_role_menu` VALUES (871705339677298688, 665499597804064768, 852017180513325056);
INSERT INTO `mscode_sys_role_menu` VALUES (871705339773767680, 665499597804064768, 663868873103560704);
INSERT INTO `mscode_sys_role_menu` VALUES (871705340071563264, 665499597804064768, 663750580350930944);
INSERT INTO `mscode_sys_role_menu` VALUES (871705340625211392, 665499597804064768, 665191788055023616);
INSERT INTO `mscode_sys_role_menu` VALUES (871705340767817728, 665499597804064768, 665199223704571904);
INSERT INTO `mscode_sys_role_menu` VALUES (871705342332293120, 665499597804064768, 665199774022422528);
INSERT INTO `mscode_sys_role_menu` VALUES (871705342755917824, 665499597804064768, 665006779776028672);
INSERT INTO `mscode_sys_role_menu` VALUES (871705342978215936, 665499597804064768, 663869085146599424);
INSERT INTO `mscode_sys_role_menu` VALUES (871705343309565952, 665499597804064768, 665496626324230144);
INSERT INTO `mscode_sys_role_menu` VALUES (871705343703830528, 665499597804064768, 765381334624817152);
INSERT INTO `mscode_sys_role_menu` VALUES (871705343796105216, 665499597804064768, 663861404352565248);
INSERT INTO `mscode_sys_role_menu` VALUES (871705343905157120, 665499597804064768, 665197219141832704);
INSERT INTO `mscode_sys_role_menu` VALUES (871705344232312832, 665499597804064768, 693762431952277504);
INSERT INTO `mscode_sys_role_menu` VALUES (871705344345559040, 665499597804064768, 663867636220088320);
INSERT INTO `mscode_sys_role_menu` VALUES (871705344488165376, 665499597804064768, 665734849524846592);
INSERT INTO `mscode_sys_role_menu` VALUES (871705344710463488, 665499597804064768, 663863146888744960);
INSERT INTO `mscode_sys_role_menu` VALUES (871705344853069824, 665499597804064768, 665004465291972608);
INSERT INTO `mscode_sys_role_menu` VALUES (871705345104728064, 665499597804064768, 663866457184456704);
INSERT INTO `mscode_sys_role_menu` VALUES (871705345297666048, 665499597804064768, 663864263244697600);
INSERT INTO `mscode_sys_role_menu` VALUES (871705345381552128, 665499597804064768, 663856596409307136);
INSERT INTO `mscode_sys_role_menu` VALUES (871705345473826816, 665499597804064768, 663862916743090176);
INSERT INTO `mscode_sys_role_menu` VALUES (871705345729679360, 665499597804064768, 663862096987344896);
INSERT INTO `mscode_sys_role_menu` VALUES (871705447525437440, 820240126587621376, 687514791186976768);
INSERT INTO `mscode_sys_role_menu` VALUES (871705447626100736, 820240126587621376, 665197381171990528);
INSERT INTO `mscode_sys_role_menu` VALUES (871705447709986816, 820240126587621376, 665564620060545024);
INSERT INTO `mscode_sys_role_menu` VALUES (871705447798067200, 820240126587621376, 699081036910743552);
INSERT INTO `mscode_sys_role_menu` VALUES (871705447886147584, 820240126587621376, 665006035807162368);
INSERT INTO `mscode_sys_role_menu` VALUES (871705447978422272, 820240126587621376, 665197875319721984);
INSERT INTO `mscode_sys_role_menu` VALUES (871705448074891264, 820240126587621376, 665198983094128640);
INSERT INTO `mscode_sys_role_menu` VALUES (871705448162971648, 820240126587621376, 663863400497336320);
INSERT INTO `mscode_sys_role_menu` VALUES (871705448246857728, 820240126587621376, 705593293912068096);
INSERT INTO `mscode_sys_role_menu` VALUES (871705448334938112, 820240126587621376, 665004888883122176);
INSERT INTO `mscode_sys_role_menu` VALUES (871705448435601408, 820240126587621376, 693698880994725888);
INSERT INTO `mscode_sys_role_menu` VALUES (871705448532070400, 820240126587621376, 684185040317763584);
INSERT INTO `mscode_sys_role_menu` VALUES (871705448645316608, 820240126587621376, 663862460172128256);
INSERT INTO `mscode_sys_role_menu` VALUES (871705448733396992, 820240126587621376, 663867140080062464);
INSERT INTO `mscode_sys_role_menu` VALUES (871705448842448896, 820240126587621376, 663868238819938304);
INSERT INTO `mscode_sys_role_menu` VALUES (871705448934723584, 820240126587621376, 663857253375725568);
INSERT INTO `mscode_sys_role_menu` VALUES (871705449026998272, 820240126587621376, 663867981964955648);
INSERT INTO `mscode_sys_role_menu` VALUES (871705449136050176, 820240126587621376, 665564219978469376);
INSERT INTO `mscode_sys_role_menu` VALUES (871705449228324864, 820240126587621376, 663864697992695808);
INSERT INTO `mscode_sys_role_menu` VALUES (871705449320599552, 820240126587621376, 665198079427137536);
INSERT INTO `mscode_sys_role_menu` VALUES (871705449412874240, 820240126587621376, 665197527700000768);
INSERT INTO `mscode_sys_role_menu` VALUES (871705449509343232, 820240126587621376, 663864025209556992);
INSERT INTO `mscode_sys_role_menu` VALUES (871705449614200832, 820240126587621376, 692361087815176192);
INSERT INTO `mscode_sys_role_menu` VALUES (871705449740029952, 820240126587621376, 663854683370475520);
INSERT INTO `mscode_sys_role_menu` VALUES (871705449836498944, 820240126587621376, 665199423219224576);
INSERT INTO `mscode_sys_role_menu` VALUES (871705449920385024, 820240126587621376, 663854974853632000);
INSERT INTO `mscode_sys_role_menu` VALUES (871705450012659712, 820240126587621376, 663866797015355392);
INSERT INTO `mscode_sys_role_menu` VALUES (871705450130100224, 820240126587621376, 690019670816116736);
INSERT INTO `mscode_sys_role_menu` VALUES (871705450239152128, 820240126587621376, 663856370193715200);
INSERT INTO `mscode_sys_role_menu` VALUES (871705450394341376, 820240126587621376, 663867371509174272);
INSERT INTO `mscode_sys_role_menu` VALUES (871705450486616064, 820240126587621376, 665199936786583552);
INSERT INTO `mscode_sys_role_menu` VALUES (871705450587279360, 820240126587621376, 663861613581225984);
INSERT INTO `mscode_sys_role_menu` VALUES (871705450679554048, 820240126587621376, 665007802225709056);
INSERT INTO `mscode_sys_role_menu` VALUES (871705450780217344, 820240126587621376, 663864915588993024);
INSERT INTO `mscode_sys_role_menu` VALUES (871705450880880640, 820240126587621376, 665009153424936960);
INSERT INTO `mscode_sys_role_menu` VALUES (871705450981543936, 820240126587621376, 663868640676204544);
INSERT INTO `mscode_sys_role_menu` VALUES (871705451078012928, 820240126587621376, 665005583355006976);
INSERT INTO `mscode_sys_role_menu` VALUES (871705451170287616, 820240126587621376, 665010791703302144);
INSERT INTO `mscode_sys_role_menu` VALUES (871705451291922432, 820240126587621376, 665010520889675776);
INSERT INTO `mscode_sys_role_menu` VALUES (871705451384197120, 820240126587621376, 665199632426913792);
INSERT INTO `mscode_sys_role_menu` VALUES (871705451489054720, 820240126587621376, 665198241801228288);
INSERT INTO `mscode_sys_role_menu` VALUES (871705451581329408, 820240126587621376, 663855492212641792);
INSERT INTO `mscode_sys_role_menu` VALUES (871705451673604096, 820240126587621376, 665192716074143744);
INSERT INTO `mscode_sys_role_menu` VALUES (871705451761684480, 820240126587621376, 852017515856318464);
INSERT INTO `mscode_sys_role_menu` VALUES (871705451858153472, 820240126587621376, 663857014669496320);
INSERT INTO `mscode_sys_role_menu` VALUES (871705451954622464, 820240126587621376, 665007437736497152);
INSERT INTO `mscode_sys_role_menu` VALUES (871705452072062976, 820240126587621376, 665003871265280000);
INSERT INTO `mscode_sys_role_menu` VALUES (871705452164337664, 820240126587621376, 665007146580496384);
INSERT INTO `mscode_sys_role_menu` VALUES (871705452273389568, 820240126587621376, 663855944106954752);
INSERT INTO `mscode_sys_role_menu` VALUES (871705452374052864, 820240126587621376, 693434079391436800);
INSERT INTO `mscode_sys_role_menu` VALUES (871705452466327552, 820240126587621376, 663866117546496000);
INSERT INTO `mscode_sys_role_menu` VALUES (871705452558602240, 820240126587621376, 871704733705228288);
INSERT INTO `mscode_sys_role_menu` VALUES (871705452659265536, 820240126587621376, 663868873103560704);
INSERT INTO `mscode_sys_role_menu` VALUES (871705452755734528, 820240126587621376, 665006303672193024);
INSERT INTO `mscode_sys_role_menu` VALUES (871705452843814912, 820240126587621376, 663856793264771072);
INSERT INTO `mscode_sys_role_menu` VALUES (871705453024169984, 820240126587621376, 705595098737856512);
INSERT INTO `mscode_sys_role_menu` VALUES (871705453133221888, 820240126587621376, 666473881804394496);
INSERT INTO `mscode_sys_role_menu` VALUES (871705453229690880, 820240126587621376, 663750580350930944);
INSERT INTO `mscode_sys_role_menu` VALUES (871705453321965568, 820240126587621376, 663865201925738496);
INSERT INTO `mscode_sys_role_menu` VALUES (871705453431017472, 820240126587621376, 663856139884482560);
INSERT INTO `mscode_sys_role_menu` VALUES (871705453565235200, 820240126587621376, 665191788055023616);
INSERT INTO `mscode_sys_role_menu` VALUES (871705453665898496, 820240126587621376, 665199223704571904);
INSERT INTO `mscode_sys_role_menu` VALUES (871705453766561792, 820240126587621376, 665199774022422528);
INSERT INTO `mscode_sys_role_menu` VALUES (871705453867225088, 820240126587621376, 665198396457799680);
INSERT INTO `mscode_sys_role_menu` VALUES (871705453951111168, 820240126587621376, 665010657795952640);
INSERT INTO `mscode_sys_role_menu` VALUES (871705454043385856, 820240126587621376, 663865503307452416);
INSERT INTO `mscode_sys_role_menu` VALUES (871705454135660544, 820240126587621376, 665006779776028672);
INSERT INTO `mscode_sys_role_menu` VALUES (871705454248906752, 820240126587621376, 663869085146599424);
INSERT INTO `mscode_sys_role_menu` VALUES (871705454336987136, 820240126587621376, 665496626324230144);
INSERT INTO `mscode_sys_role_menu` VALUES (871705454559285248, 820240126587621376, 663865799052021760);
INSERT INTO `mscode_sys_role_menu` VALUES (871705454722863104, 820240126587621376, 663861404352565248);
INSERT INTO `mscode_sys_role_menu` VALUES (871705454945161216, 820240126587621376, 665006482060136448);
INSERT INTO `mscode_sys_role_menu` VALUES (871705455033241600, 820240126587621376, 665197219141832704);
INSERT INTO `mscode_sys_role_menu` VALUES (871705455121321984, 820240126587621376, 693762978637860864);
INSERT INTO `mscode_sys_role_menu` VALUES (871705455209402368, 820240126587621376, 693762431952277504);
INSERT INTO `mscode_sys_role_menu` VALUES (871705455310065664, 820240126587621376, 663867636220088320);
INSERT INTO `mscode_sys_role_menu` VALUES (871705455465254912, 820240126587621376, 665734849524846592);
INSERT INTO `mscode_sys_role_menu` VALUES (871705455574306816, 820240126587621376, 687563817764704256);
INSERT INTO `mscode_sys_role_menu` VALUES (871705455666581504, 820240126587621376, 665009726589161472);
INSERT INTO `mscode_sys_role_menu` VALUES (871705455855325184, 820240126587621376, 663855709905408000);
INSERT INTO `mscode_sys_role_menu` VALUES (871705455968571392, 820240126587621376, 663863146888744960);
INSERT INTO `mscode_sys_role_menu` VALUES (871705456178286592, 820240126587621376, 663863700486541312);
INSERT INTO `mscode_sys_role_menu` VALUES (871705456278949888, 820240126587621376, 665004465291972608);
INSERT INTO `mscode_sys_role_menu` VALUES (871705456362835968, 820240126587621376, 665730162285268992);
INSERT INTO `mscode_sys_role_menu` VALUES (871705456463499264, 820240126587621376, 663866457184456704);
INSERT INTO `mscode_sys_role_menu` VALUES (871705456589328384, 820240126587621376, 663864263244697600);
INSERT INTO `mscode_sys_role_menu` VALUES (871705456706768896, 820240126587621376, 665192197972742144);
INSERT INTO `mscode_sys_role_menu` VALUES (871705456807432192, 820240126587621376, 663856596409307136);
INSERT INTO `mscode_sys_role_menu` VALUES (871705456903901184, 820240126587621376, 663862916743090176);
INSERT INTO `mscode_sys_role_menu` VALUES (871705456996175872, 820240126587621376, 663862096987344896);

-- ----------------------------
-- Table structure for mscode_sys_role_user
-- ----------------------------
DROP TABLE IF EXISTS `mscode_sys_role_user`;
CREATE TABLE `mscode_sys_role_user`  (
  `id` bigint(20) NOT NULL COMMENT '角色与用户关联主键ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色主键ID',
  `user_id` bigint(20) NOT NULL COMMENT '用户主键ID',
  `post_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '岗位编码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色与用户关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mscode_sys_role_user
-- ----------------------------
INSERT INTO `mscode_sys_role_user` VALUES (782909074710122496, 666858336536678400, 782909074190028800, 'CTS-RD-04');
INSERT INTO `mscode_sys_role_user` VALUES (782909074810785792, 685031431038488576, 782909074190028800, 'CTS-RD-04');
INSERT INTO `mscode_sys_role_user` VALUES (785353224579633152, 665499597804064768, 653917974152592329, 'CTS-RD-04');
INSERT INTO `mscode_sys_role_user` VALUES (785353224676102144, 666858336536678400, 653917974152592329, 'CTS-RD-04');
INSERT INTO `mscode_sys_role_user` VALUES (820241798818877440, 820240126587621376, 820241798336532480, 'CTS-RD-04');
INSERT INTO `mscode_sys_role_user` VALUES (820241798915346432, 666858336536678400, 820241798336532480, 'CTS-RD-04');
INSERT INTO `mscode_sys_role_user` VALUES (836851890691428352, 689659913718648832, 653917974152592329, 'CTS-RD-06');
INSERT INTO `mscode_sys_role_user` VALUES (836851934081503232, 666858434746306560, 653917974152592329, 'CTS-RD-05');
INSERT INTO `mscode_sys_role_user` VALUES (836852102705106944, 666858434746306560, 653918847061381120, 'CTS-RD-05');
INSERT INTO `mscode_sys_role_user` VALUES (836852191947313152, 666858336536678400, 653918847061381120, 'CTS-RD-04');
INSERT INTO `mscode_sys_role_user` VALUES (836852466229628928, 666858336536678400, 656413611073654784, 'CTS-RD-04');
INSERT INTO `mscode_sys_role_user` VALUES (836852522806595584, 689660323321794560, 656413611073654784, 'CTS-OFFICE-03');
INSERT INTO `mscode_sys_role_user` VALUES (836852638976233472, 666858336536678400, 656414601122336768, 'CTS-RD-04');
INSERT INTO `mscode_sys_role_user` VALUES (836852681217069056, 689659913718648832, 656414601122336768, 'CTS-OFFICE-01');
INSERT INTO `mscode_sys_role_user` VALUES (836852769058377728, 666858336536678400, 689718972811497472, 'CTS-RD-04');
INSERT INTO `mscode_sys_role_user` VALUES (836852845608620032, 689660323321794560, 689718972811497472, 'CTS-OFFICE-05');
INSERT INTO `mscode_sys_role_user` VALUES (836853213646213120, 666858336536678400, 689734818510327808, 'CTS-RD-04');
INSERT INTO `mscode_sys_role_user` VALUES (836853261641633792, 689659913718648832, 689734818510327808, 'CTS-PROJECT-01');
INSERT INTO `mscode_sys_role_user` VALUES (836853406915547136, 666858434746306560, 820241798336532480, 'CTS-RD-06');
INSERT INTO `mscode_sys_role_user` VALUES (836853456517386240, 689659913718648832, 820241798336532480, 'CTS-RD-01');
INSERT INTO `mscode_sys_role_user` VALUES (836854163672846336, 689660323321794560, 689732941378932736, 'CTS-OFFICE-02');

-- ----------------------------
-- Table structure for mscode_sys_tenant
-- ----------------------------
DROP TABLE IF EXISTS `mscode_sys_tenant`;
CREATE TABLE `mscode_sys_tenant`  (
  `id` bigint(20) NOT NULL COMMENT '租户主键ID',
  `tenant_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户编码',
  `tenant_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户名称',
  `tenant_contact` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系人',
  `tenant_email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系邮箱',
  `tenant_tel` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_tenant_code`(`tenant_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '租户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mscode_sys_tenant
-- ----------------------------
INSERT INTO `mscode_sys_tenant` VALUES (1, '10001', '国星科技', '管理员', '10000@qq.com', '123456789', '2019-09-03 09:54:20', '2021-04-18 17:02:17');
INSERT INTO `mscode_sys_tenant` VALUES (713569479355191296, '10002', 'A公司', '李工', '123456@qq.com', '15011111111', '2020-05-23 09:50:25', NULL);
INSERT INTO `mscode_sys_tenant` VALUES (713569744636530688, '100000003', 'B公司', '张部', '100000003@org.cn', '13988888888', '2020-05-23 09:51:28', NULL);

-- ----------------------------
-- Table structure for mscode_sys_user
-- ----------------------------
DROP TABLE IF EXISTS `mscode_sys_user`;
CREATE TABLE `mscode_sys_user`  (
  `id` bigint(20) NOT NULL COMMENT '用户主键ID',
  `username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '邮箱',
  `mobile` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '手机号',
  `prefix` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号国家代码',
  `nickname` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `province_regioncode` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '省份区域代码',
  `city_regioncode` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地市区域代码',
  `address` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '街道地址',
  `profile` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '个人简介',
  `avatar` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像',
  `org_id` bigint(20) NOT NULL COMMENT '所属机构ID',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '状态 0：禁用 1：正常',
  `notify_message` tinyint(4) NOT NULL DEFAULT 1 COMMENT '是否通知系统消息 0：关 1：开',
  `notify_todo` tinyint(4) NOT NULL DEFAULT 1 COMMENT '是否通知待办任务 0：关 1：开',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `tenant_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '10001' COMMENT '租户编码',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_user_username`(`username`) USING BTREE,
  UNIQUE INDEX `idx_user_email`(`email`) USING BTREE,
  UNIQUE INDEX `idx_user_mobile`(`mobile`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mscode_sys_user
-- ----------------------------
INSERT INTO `mscode_sys_user` VALUES (653917974152592329, 'admin', '$2a$10$0Gh0i/roNUhOHnEXTpn3WexRsc8RfhxAaumD.KdzUtmxesVwHH8q6', 'admin@mscodecloud.com', '12222222229', '86', '超级管理员', '0755-55556666', '440000', '440100', '花都区永福路1号', '我的简介', '/api/file/static/upload/20200224110421638.png', 666664834108411904, 1, 1, 1, '2019-06-13 11:40:14', '2021-01-27 21:38:34', '10001');
INSERT INTO `mscode_sys_user` VALUES (653918847061381120, 'test', '$2a$10$0Gh0i/roNUhOHnEXTpn3WexRsc8RfhxAaumD.KdzUtmxesVwHH8q6', 'test@mscodecloud.com', '15011112222', '86', '王雪红', NULL, '440000', '440100', NULL, NULL, '/api/file/static/upload/20200224110421638.png', 666858728934789120, 1, 1, 1, '2019-12-10 19:20:04', '2020-12-07 11:52:59', '10001');
INSERT INTO `mscode_sys_user` VALUES (656413611073654784, 'attendance', '$2a$10$0Gh0i/roNUhOHnEXTpn3WexRsc8RfhxAaumD.KdzUtmxesVwHH8q6', 'attendance@mscodecloud.com', '12111111113', '86', '人事专员', NULL, '440000', '440100', NULL, NULL, '/api/file/static/upload/20200224110421638.png', 666665278205513728, 1, 1, 1, '2019-12-17 16:33:22', '2020-03-18 15:17:28', '10001');
INSERT INTO `mscode_sys_user` VALUES (656414601122336768, 'manager', '$2a$10$0Gh0i/roNUhOHnEXTpn3WexRsc8RfhxAaumD.KdzUtmxesVwHH8q6', 'manager@mscodecloud.com', '13333333339', '86', '李总', '0755-55556666', '350000', '350100', '西湖区工专路 77 号', '我的简介', '/api/file/static/upload/20200224110421638.png', 666665278205513728, 1, 1, 1, '2019-12-17 16:37:18', '2020-03-18 14:14:03', '10001');
INSERT INTO `mscode_sys_user` VALUES (689718972811497472, 'personnel', '$2a$10$0Gh0i/roNUhOHnEXTpn3WexRsc8RfhxAaumD.KdzUtmxesVwHH8q6', 'personnel@mscodecloud.com', '15666666667', '86', '人事经理', '020-22222222', '440000', '440100', NULL, NULL, '/api/file/static/upload/20200224110421638.png', 666665278205513728, 1, 1, 1, '2020-03-18 14:16:57', '2020-03-18 15:17:31', '10001');
INSERT INTO `mscode_sys_user` VALUES (689732941378932736, 'hrd', '$2a$10$0Gh0i/roNUhOHnEXTpn3WexRsc8RfhxAaumD.KdzUtmxesVwHH8q6', 'hrd@mscodecloud.com', '19999999999', '86', '人事总监', '020-22222222', '440000', '440100', NULL, NULL, '/api/file/static/upload/20200224110421638.png', 666665278205513728, 1, 1, 1, '2020-03-18 15:12:28', '2020-03-18 15:17:34', '10001');
INSERT INTO `mscode_sys_user` VALUES (689734818510327808, 'rdd', '$2a$10$0Gh0i/roNUhOHnEXTpn3WexRsc8RfhxAaumD.KdzUtmxesVwHH8q6', 'rdd@mscodecloud.com', '15688888888', '86', '张部', '020-22222222', '440000', '440100', NULL, NULL, '/api/file/static/upload/20200224110421638.png', 666664834108411904, 1, 1, 1, '2020-03-18 15:19:55', '2020-12-07 11:52:45', '10001');
INSERT INTO `mscode_sys_user` VALUES (820241798336532480, 'reactadmin', '$2a$10$6yuZPLX0v.ts7fcNLJ15U.LQhyaqgrVT79D8ldEA9ChvxsicJKh8q', 'reactadmin@chinatechstar.com', '15011899123', '86', '管理员', '020-36968862', '440000', '440100', '广州市花都区永发大道永福路1号3楼307室', '广东国星科技有限公司是专业的软件基础平台与解决方案提供商，为企事业单位提供创新可靠的软件基础平台产品及技术服务。', '/api/file/static/upload/20200224110421638.png', 666664834108411904, 1, 1, 1, '2021-03-13 18:28:07', '2021-03-14 09:07:43', '10001');

-- ----------------------------
-- Table structure for mscode_sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `mscode_sys_user_post`;
CREATE TABLE `mscode_sys_user_post`  (
  `id` bigint(20) NOT NULL COMMENT '用户与岗位关联主键ID',
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位上下级名称',
  `post_description` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '岗位描述',
  `post_type` tinyint(4) NOT NULL DEFAULT 2 COMMENT '岗位类型 1:主岗位，2:副岗位',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '状态 0：隐藏 1：显示',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `tenant_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '10001' COMMENT '租户编码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mscode_sys_user_post
-- ----------------------------
INSERT INTO `mscode_sys_user_post` VALUES (836746106838110208, 820241798336532480, 'CTS-RD-04', '国星科技公司部门岗位·研发部·研发部开发岗', '这是研发部开发岗。', 1, 1, '2021-04-28 07:30:20', NULL, '10001');
INSERT INTO `mscode_sys_user_post` VALUES (836746197602848768, 689734818510327808, 'CTS-RD-04', '国星科技公司部门岗位·研发部·研发部开发岗', '这是研发部开发岗。', 1, 1, '2021-04-28 07:30:42', NULL, '10001');
INSERT INTO `mscode_sys_user_post` VALUES (836746318029705216, 689718972811497472, 'CTS-RD-04', '国星科技公司部门岗位·研发部·研发部开发岗', '这是研发部开发岗。', 1, 1, '2021-04-28 07:31:10', NULL, '10001');
INSERT INTO `mscode_sys_user_post` VALUES (836746376406028288, 656414601122336768, 'CTS-RD-04', '国星科技公司部门岗位·研发部·研发部开发岗', '这是研发部开发岗。', 1, 1, '2021-04-28 07:31:24', NULL, '10001');
INSERT INTO `mscode_sys_user_post` VALUES (836746421595459584, 656413611073654784, 'CTS-RD-04', '国星科技公司部门岗位·研发部·研发部开发岗', '这是研发部开发岗。', 1, 1, '2021-04-28 07:31:35', NULL, '10001');
INSERT INTO `mscode_sys_user_post` VALUES (836746465350438912, 653918847061381120, 'CTS-RD-04', '国星科技公司部门岗位·研发部·研发部开发岗', '这是研发部开发岗。', 1, 1, '2021-04-28 07:31:46', NULL, '10001');
INSERT INTO `mscode_sys_user_post` VALUES (836746509684232192, 653917974152592329, 'CTS-RD-04', '国星科技公司部门岗位·研发部·研发部开发岗', '这是研发部开发岗。', 1, 1, '2021-04-28 07:31:56', NULL, '10001');
INSERT INTO `mscode_sys_user_post` VALUES (836746707105927168, 820241798336532480, 'CTS-RD-06', '国星科技公司部门岗位·研发部·研发部架构设计岗', '这是研发部架构设计岗。', 2, 1, '2021-04-28 07:32:43', NULL, '10001');
INSERT INTO `mscode_sys_user_post` VALUES (836746843081068544, 653917974152592329, 'CTS-RD-06', '国星科技公司部门岗位·研发部·研发部架构设计岗', '这是研发部架构设计岗。', 2, 1, '2021-04-28 07:33:16', NULL, '10001');
INSERT INTO `mscode_sys_user_post` VALUES (836746927843758080, 653918847061381120, 'CTS-RD-05', '国星科技公司部门岗位·研发部·研发部测试岗', '这是研发部测试岗。', 2, 1, '2021-04-28 07:33:36', NULL, '10001');
INSERT INTO `mscode_sys_user_post` VALUES (836747094634450944, 653917974152592329, 'CTS-RD-05', '国星科技公司部门岗位·研发部·研发部测试岗', '这是研发部测试岗。', 2, 1, '2021-04-28 07:34:16', NULL, '10001');
INSERT INTO `mscode_sys_user_post` VALUES (836747292853063680, 656413611073654784, 'CTS-OFFICE-03', '国星科技公司部门岗位·办公室·办公室文秘岗', '这是办公室文秘岗。', 2, 1, '2021-04-28 07:35:03', NULL, '10001');
INSERT INTO `mscode_sys_user_post` VALUES (836747411690278912, 656414601122336768, 'CTS-OFFICE-01', '国星科技公司部门岗位·办公室·办公室主任', '这是办公室主任岗。', 2, 1, '2021-04-28 07:35:31', NULL, '10001');
INSERT INTO `mscode_sys_user_post` VALUES (836747517881667584, 689718972811497472, 'CTS-OFFICE-05', '国星科技公司部门岗位·办公室·办公室企业管理岗', '这是办公室企业管理岗。', 2, 1, '2021-04-28 07:35:56', NULL, '10001');
INSERT INTO `mscode_sys_user_post` VALUES (836747602292035584, 689732941378932736, 'CTS-OFFICE-02', '国星科技公司部门岗位·办公室·办公室副主任', '这是办公室副主任岗。', 1, 1, '2021-04-28 07:36:17', '2021-04-28 14:37:14', '10001');
INSERT INTO `mscode_sys_user_post` VALUES (836747679127490560, 689734818510327808, 'CTS-PROJECT-01', '国星科技公司部门岗位·工程部·工程部经理', '这是工程部经理岗。', 2, 1, '2021-04-28 07:36:35', NULL, '10001');
INSERT INTO `mscode_sys_user_post` VALUES (836747872665260032, 820241798336532480, 'CTS-RD-01', '国星科技公司部门岗位·研发部·研发部经理', '这是研发部经理岗。', 2, 1, '2021-04-28 07:37:21', NULL, '10001');

SET FOREIGN_KEY_CHECKS = 1;
