package com.chinatechstar.admin.client;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 提供给其他微服务调用的机构微服务接口层
 * 
 * @版权所有 广东国星科技有限公司，商业授权：www.mscodecloud.com
 */
@FeignClient(name = "mscode-admin-service", path = "/admin/sysorg", fallback = SysOrgServiceClientFallback.class)
public interface SysOrgServiceClient {

	/**
	 * 根据机构类型查询机构列表
	 * 
	 * @param orgType 机构类型
	 * @return
	 */
	@PostMapping(value = "/querySysOrgList")
	List<LinkedHashMap<String, Object>> querySysOrgList(@RequestParam(name = "orgType", required = true) String orgType);

}
