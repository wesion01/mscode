package com.chinatechstar.admin.client;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 提供给其他微服务调用的字典微服务接口层
 * 
 * @版权所有 广东国星科技有限公司，商业授权：www.mscodecloud.com
 */
@FeignClient(name = "mscode-admin-service", path = "/admin/sysdict", fallback = SysDictServiceClientFallback.class)
public interface SysDictServiceClient {

	/**
	 * 根据字典类型查询下拉框数据列表
	 * 
	 * @param dictType 字典类型
	 * @return
	 */
	@GetMapping(value = "/queryDictByDictType")
	List<LinkedHashMap<String, Object>> queryDictByDictType(@RequestParam(name = "dictType", required = true) String dictType);

}
