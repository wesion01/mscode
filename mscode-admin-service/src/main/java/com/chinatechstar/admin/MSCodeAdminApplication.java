package com.chinatechstar.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

/**
 * MSCode的系统管理启动类
 * 
 * @版权所有 广东国星科技有限公司，商业授权：www.mscodecloud.com
 */
@SpringBootApplication(scanBasePackages = "com.chinatechstar")
@EnableDiscoveryClient
@EnableWebSecurity
@EnableFeignClients(basePackages = { "com.chinatechstar" })
@EnableCircuitBreaker
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class MSCodeAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(MSCodeAdminApplication.class);
	}

}
