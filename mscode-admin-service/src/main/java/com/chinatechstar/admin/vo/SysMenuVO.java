package com.chinatechstar.admin.vo;

import java.io.Serializable;

import com.chinatechstar.component.commons.vo.CommonVO;

/**
 * 菜单信息的参数类
 * 
 * @版权所有 广东国星科技有限公司，商业授权：www.mscodecloud.com
 */
public class SysMenuVO extends CommonVO implements Serializable {

	private static final long serialVersionUID = 8283244799001002954L;
	private String menuName;// 菜单名称
	private String menuPath;// 菜单路由

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getMenuPath() {
		return menuPath;
	}

	public void setMenuPath(String menuPath) {
		this.menuPath = menuPath;
	}

}
