package com.chinatechstar.auth;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * MSCode的密码加密测试类
 * 
 * @版权所有 广东国星科技有限公司，商业授权：www.mscodecloud.com
 *
 */
@RunWith(SpringRunner.class)
public class BCryptPasswordEncoderTest {

	@Test
	public void contextLoads() throws Exception {
		assertThat(new BCryptPasswordEncoder().encode("123456")).isNotNull();
	}

}
