package com.chinatechstar.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

/**
 * MSCode的OAuth2安全认证授权启动类
 * 
 * @版权所有 广东国星科技有限公司，商业授权：www.mscodecloud.com
 */
@SpringBootApplication(scanBasePackages = "com.chinatechstar")
@EnableResourceServer
@EnableDiscoveryClient
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class MSCodeAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(MSCodeAuthApplication.class);
	}

}
