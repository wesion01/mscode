package com.chinatechstar.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * MSCode的入口网关启动类
 * 
 * @版权所有 广东国星科技有限公司，商业授权：www.mscodecloud.com
 */
@SpringBootApplication(scanBasePackages = "com.chinatechstar")
@EnableDiscoveryClient
public class MSCodeGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(MSCodeGatewayApplication.class);
	}

}
