package com.chinatechstar.notification.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.chinatechstar.component.commons.client.ClientResponse;

/**
 * 提供给其他微服务调用的邮件微服务接口层
 * 
 * @版权所有 广东国星科技有限公司，商业授权：www.mscodecloud.com
 */
@FeignClient(name = "mscode-notification-service", path = "/notification/email", fallback = EmailServiceClientFallback.class)
public interface EmailServiceClient {

	/**
	 * 发送邮件
	 * 
	 * @param toEmail 接收者的邮箱
	 * @param subject 邮件主题
	 * @param text    邮件内容
	 * @return
	 */
	@PostMapping(path = "/sendEmail")
	ClientResponse sendEmail(@RequestParam(name = "toEmail", required = true) String toEmail, @RequestParam(name = "subject", required = true) String subject,
			@RequestParam(name = "text", required = false) String text);

}
