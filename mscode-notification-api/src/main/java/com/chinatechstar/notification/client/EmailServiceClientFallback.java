package com.chinatechstar.notification.client;

import java.text.MessageFormat;

import org.springframework.stereotype.Component;

import com.chinatechstar.component.commons.client.ClientResponse;
import com.chinatechstar.component.commons.client.ResultCode;

/**
 * 提供给其他微服务调用的邮件微服务接口的熔断降级类
 * 
 * @版权所有 广东国星科技有限公司，商业授权：www.mscodecloud.com
 */
@Component
public class EmailServiceClientFallback implements EmailServiceClient {

	/**
	 * 发送邮件抛出异常的熔断降级
	 */
	@Override
	public ClientResponse sendEmail(String toEmail, String subject, String text) {
		String message = MessageFormat.format("发送邮件到邮箱{0}失败", toEmail);
		return new ClientResponse(ResultCode.FAILURE, message);
	}

}
