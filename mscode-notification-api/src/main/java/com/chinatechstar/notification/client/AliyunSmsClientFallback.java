package com.chinatechstar.notification.client;

import org.springframework.stereotype.Component;

/**
 * 提供给其他微服务调用的阿里云短信微服务接口的熔断降级类
 * 
 * @版权所有 广东国星科技有限公司，商业授权：www.mscodecloud.com
 */
@Component
public class AliyunSmsClientFallback implements AliyunSmsClient {

	/**
	 * 阿里云发送短信抛出异常的熔断降级
	 */
	@Override
	public boolean sendSms(String phoneNumber, String signName, String templateCode, String templateParam) {
		return false;
	}

	/**
	 * 获取随机数验证码抛出异常的熔断降级
	 */
	@Override
	public String getRandomCode(int digits) {
		return null;
	}

}
