package com.chinatechstar.notification.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 提供给其他微服务调用的阿里云短信微服务接口层
 * 
 * @版权所有 广东国星科技有限公司，商业授权：www.mscodecloud.com
 */
@FeignClient(name = "mscode-notification-service", path = "/notification/sms", fallback = AliyunSmsClientFallback.class)
public interface AliyunSmsClient {

	/**
	 * 阿里云发送短信
	 * 
	 * @param phoneNumber   手机号
	 * @param signName      阿里云短信签名
	 * @param templateCode  阿里云短信模板Code
	 * @param templateParam JSON模板参数字符串
	 * @return
	 */
	@PostMapping(path = "/sendSms")
	boolean sendSms(@RequestParam(name = "phoneNumber", required = true) String phoneNumber, @RequestParam(name = "signName", required = true) String signName,
			@RequestParam(name = "templateCode", required = true) String templateCode,
			@RequestParam(name = "templateParam", required = true) String templateParam);

	/**
	 * 获取随机数验证码
	 * 
	 * @param digits 几位随机数
	 * @return
	 */
	@GetMapping(path = "/getRandomCode")
	String getRandomCode(@RequestParam(name = "digits", required = true) int digits);

}
